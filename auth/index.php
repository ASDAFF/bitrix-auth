<?
//define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0) 
	LocalRedirect($_SESSION['BACK_URL_AUTH']);

if($USER->IsAuthorized()){
        LocalRedirect('/zayavki/');
}


$APPLICATION->SetTitle("Вход в систему");
?>




    
<?
$APPLICATION->IncludeComponent("bitrix:system.auth.form","auth",Array(
     "REGISTER_URL" => "register.php",
     "FORGOT_PASSWORD_URL" => "",
     "PROFILE_URL" => "profile.php",
     "SHOW_ERRORS" => "Y" 
     )
);
/*$APPLICATION->IncludeComponent("bitrix:main.register","auth_new",Array(
        "USER_PROPERTY_NAME" => "", 
        "SEF_MODE" => "Y", 
        "SHOW_FIELDS" => Array(), 
        "REQUIRED_FIELDS" => Array(), 
        "AUTH" => "Y", 
        "USE_BACKURL" => "Y", 
        "SUCCESS_PAGE" => "", 
        "SET_TITLE" => "N", 
        "USER_PROPERTY" => Array(), 
        "SEF_FOLDER" => "/", 
        "VARIABLE_ALIASES" => Array()
    )
);*/?> 



<h2 class="il-main-btn" id="il_sel_registr">Регистрация нового пользователя <i class="fa fa-chevron-right faVetal2" aria-hidden="true"></i> </h2>
                <div class="il-line-new-user il-window">
                    <form class="on_ajax register" action="/include/registor.php" method="POST">
                        <div class="row">
                            <div class="col-md-3">
                                <p>Тип пользователя</p>
                            </div>
                            <div class="col-md-9 radio">

                                <input  type="radio" name="RegistrationType" id="il_ur_li1" class="" value="8"/>
                                <label for="il_ur_li1"><p>Юридическое лицо</p></label>

                                <input type="radio" name="RegistrationType" id="il_ip_li2" class="" value="9"/>
                                <label for="il_ip_li2"><p>Индивидуальный предприниматель</p></label>

                                <input type="radio" name="RegistrationType" id="il_fp_li3" class="" value="10"/>
                                <label for="il_fp_li3"><p>Физичиское лицо</p></label>

                            </div>
                        </div>
                        <div class="row" id="il_li">
                            <div class="col-md-12">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="il-sub-title">Данные государственной регистрации</p>
                                        </div>
                                    </div>
                                    <div class="row" id="il_poisk">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">Поиск:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="divVet">
                                            <input type="text" class="form-control formControlVet" placeholder="Введите ИНН или название компании">
                                                <button>Поиск</button>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row" id="il_naimenovanie">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">Наименование:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="UF_COMPANY" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">ИНН:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="UF_INN" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row" id="il_kpp">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">КПП:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="UF_KPP" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row" id="il_ogpn">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">ОГРН:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="UF_OGN" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row" id="il_ogpnip">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">ОГРНИП:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" name="UF_OGRNIP" placeholder="" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="il-sub-title">Данные пользователя</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">Фамилия:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control requared" name="SURNAME" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">Имя:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control requared" name="NAME" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">Отчество:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control requared" name="BATKO" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">Логин (адрес электронной почты):</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control requared" name="EMAIL" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">Пароль:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="password" class="form-control requared" name="PASSWORD" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="il-lable-input">Подтверждение пароля:</p>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="password" class="form-control requared" name="CONFIRM_PASSWORD" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="alert"><i class="fa fa-exclamation-triangle"></i> Пароль должен содержать латинские буквы верхнего и нижнего регистров, а также цифры. Длина пароля должна составлять от 10 до 20 символов.</p>
                                        </div>
                                    </div>
									<div class="row">
										<div class="col-md-12">
											<div class="error-text"></div>
										</div>
									</div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="il-sub-title">Защита от спама</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="ill-login__btn button_login">Зарегистрироваться</button>
                                        </div>
                                    </div>

                            </div>
                        </div>
                    </form>
					<form class="on_ajax check_kod hide" action="/include/chech_kod.php" method="POST">
					<?/*<div class="row" id="il_li">*/?>
					
					
					<div class="row" id="il_poisk">
						<div class="col-md-3">
							<p class="il-lable-input">На ваш E-mail выслан код подтверждения</p>
						</div>
						<div class="col-md-9">
							<div class="divVet">
							<input type="text" class="form-control formControlVet requared" name="KOD" placeholder="">
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button class="ill-login__btn button_login">Активировать</button>
						</div>
					</div>
					
					
							<?/*<div class="element_anketa">
									На ваш E-mail выслан код подтверждения
									<input type="text"  placeholder='Код подтверждения' name="KOD" class='requared' required>
							</div>
							<div class="element_anketa">
									<div class="error-text"></div>
							</div>
							<div class="element_anketa">
									<button class='button_login'>Активировать</button>
							</div>
							
					</div>*/?>
					</form>
                </div>
<?/*
<form class="on_ajax register" action="/include/registor.php" method="POST">
								<div class="element_anketa">
								<label class="b-cabinet__radio-wrap">
                                        <input data-val="true" data-val-required="Требуется поле RegistrationType." id="RegistrationType" name="RegistrationType" type="radio" value="8" class="uridical">
										 Юридическое лицо
								</label>
								<label class="b-cabinet__radio-wrap">
										<input data-val="true" data-val-required="Требуется поле RegistrationType." id="RegistrationType" name="RegistrationType" type="radio" value="9" class="individual">
										 Индивидуальный предприниматель
								</label>
								<label class="b-cabinet__radio-wrap">
										<input data-val="true" data-val-required="Требуется поле RegistrationType." id="RegistrationType" name="RegistrationType" type="radio" class="fiz" value="10">
										 Физическое лицо
								</label>
                                </div>
								
								
								
								<div class="element_anketa">
                                        <input type="text" placeholder='Наименование' name="UF_COMPANY" class='companu_field not_fiz'>
                                </div>
								<div class="element_anketa">
                                        <input type="text" placeholder='ИНН' name="UF_INN" class='companu_field'>
                                </div>
								<div class="element_anketa">
                                        <input type="text" placeholder='КПП' name="UF_KPP" class='companu_field not_individual not_fiz'>
                                </div>
								<div class="element_anketa">
                                        <input type="text" placeholder='ОГРН' name="UF_OGN" class='companu_field not_fiz'>
                                </div>
								
								
								
								
                                <div class="element_anketa">
                                        <input type="text" placeholder='Фамилия' name="SURNAME" class='requared' required>
                                </div>
								 <div class="element_anketa">
                                        <input type="text" placeholder='Имя' name="NAME" class='requared' required>
                                </div>
								 <div class="element_anketa">
                                        <input type="text" placeholder='Отчество' name="BATKO" class='requared' required>
                                </div>
                                <div class="element_anketa">
                                        <input type="text" placeholder='E-mail' name="EMAIL" class='requared' required>
                                </div>
                                <div class="element_anketa">
                                        <input type="text" placeholder='Телефон' name="PERSONAL_MOBILE" class='requared' required>
                                </div>
                                <div class="element_anketa">
                                        <input type="password" placeholder='Пароль' name="PASSWORD" class='requared' required>
                                </div>
                                <div class="element_anketa">
                                        <input type="password" placeholder='Подтверждение пароля' name="CONFIRM_PASSWORD" class='requared' required>
                                </div>

                                <div class="element_anketa">
                                        <div class="error-text"></div>
                                </div>
                                <div class="element_anketa">
                                        <button class='button_login'>Зарегестрироваться</button>
                                </div>
                        </form>
                        

    */?>                    
                        



<?/*<p>Вы зарегистрированы и успешно авторизовались.</p>
 
<p><a href="<?=SITE_DIR?>">Вернуться на главную страницу</a></p>*/?>

<?/*                <div class="window">
                        <div class="element_anketa">
                                <p>Авторизация</p>
                        </div>
                        <div class="element_anketa">
                                <input type="text" placeholder='Логин'>
                        </div>
                        <div class="element_anketa">
                                <input type="text" placeholder='Пароль' class='requared_empty'>
                        </div>
                        <div class="element_anketa">
                                <div class="error-text">Авторизация</div>
                        </div>
                        <div class="element_anketa">
                                <button class='button_login'>Войти</button>
                        </div>
                        <div class="element_anketa">
                                <a href="#">Напомнить пароль</a> <a href="#">Зарегестрироваться</a>
                        </div>
                        
                </div>*/?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>