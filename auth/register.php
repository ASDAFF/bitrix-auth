<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");?>
                <div class="window">
                        <div class="element_anketa">
                                <p>Регистрация</p>
                        </div>
                        <form class="on_ajax register" action="/include/registor.php" method="POST">
                                <div class="element_anketa">
                                        <input type="text" placeholder='ФИО' name="NAME" class='requared' required>
                                </div>
                                <div class="element_anketa">
                                        <input type="text" placeholder='E-mail' name="EMAIL" class='requared' required>
                                </div>
                                <div class="element_anketa">
                                        <input type="text" placeholder='Телефон' name="PERSONAL_MOBILE" class='requared' required>
                                </div>
                                <div class="element_anketa">
                                        <input type="password" placeholder='Пароль' name="PASSWORD" class='requared' required>
                                </div>
                                <div class="element_anketa">
                                        <input type="password" placeholder='Подтверждение пароля' name="CONFIRM_PASSWORD" class='requared' required>
                                </div>

                                <div class="element_anketa">
                                        <div class="error-text"></div>
                                </div>
                                <div class="element_anketa">
                                        <button class='button_login'>Зарегестрироваться</button>
                                </div>
                        </form>
                        <form class="on_ajax check_kod hide" action="/include/chech_kod.php" method="POST">
                                <div class="element_anketa">
                                        На ваш E-mail выслан код подтверждения
                                        <input type="text"  placeholder='Код подтверждения' name="KOD" class='requared' required>
                                </div>
                                <div class="element_anketa">
                                        <div class="error-text"></div>
                                </div>
                                <div class="element_anketa">
                                        <button class='button_login'>Активировать</button>
                                </div>
                                

                        </form>

                        <div class="element_anketa">
                                <a href="/auth/?forgot_password=yes">Напомнить пароль</a> <a href="/auth/">Авторизоваться</a>
                        </div>
                        
                </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>