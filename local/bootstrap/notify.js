﻿$(document).ready(function () {
    $(window).on('click', function () {
        $('.notify').fadeOut(300, function () { $('.notify').remove(); });
    });
    handleAjaxMessages();
});
function handleAjaxMessages() {
    
    $(document).ajaxError(function (event, request) {
        try {
            var errorObj = JSON.parse(request.responseText);
            if (errorObj) {
                displayMessage(errorObj.Message, "error");
            } else {
                displayMessage(request.responseText, "error");
            }
        } catch(err) {
        }
    });
    $(document).ajaxSuccess(function(event, request) {
        checkAndHandleMessageFromHeader(request);
    });
}
function checkAndHandleMessageFromHeader(request) {
    var notify = request.getResponseHeader('X-Notify');
    if (notify) {
        displayMessage(notify, request.getResponseHeader('X-Notify-Type'));
    }
}
function displayMessage(notify, notifyType) {
    window.setTimeout(function() {
        if ($('.notify').length == 0) {
            $("body").append('<div class="notify"><div class="alert alert-' + notifyType + '">' + decodeURIComponent(notify).replace(/\+/g, ' ') + '</div></div>');
        }
    },5);
}
