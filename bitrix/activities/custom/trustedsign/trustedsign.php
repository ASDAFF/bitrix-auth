<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();


class CBPTrustedSign
    extends CBPCompositeActivity
    implements IBPEventActivity, IBPActivityExternalEventListener
{
    private $taskId = 0;
    private $subscriptionId = 0;

    private $isInEventActivityMode = false;

    private $arApproveResults = array();

    public function __construct($name)
    {
        parent::__construct($name);
        $this->arProperties = array(
            "Title" => "",
            "Users" => null,
            "ApproveType" => "all",
            "Percent" => 100,
            "OverdueDate" => null,
            "Name" => null,
            "Description" => null,
            "Parameters" => null,
            "ApproveMinPercent" => 50,
            "ApproveWaitForAll" => "N",
            "Comments" => "",
            "VotedCount" => 0,
            "TotalCount" => 0,
            "VotedPercent" => 0,
            "ApprovedPercent" => 0,
            "NotApprovedPercent" => 0,
            "ApprovedCount" => 0,
            "NotApprovedCount" => 0,
            "StatusMessage" => "",
            "SetStatusMessage" => "Y",
            "LastApprover" => null,
            "Approvers" => "",
            "Rejecters" => "",
            "TimeoutDuration" => 0,
            "TimeoutDurationType" => "s",
            "IsTimeout" => 0,
            "TaskButton1Message" => "",
            "TaskButton2Message" => "",
            "CommentLabelMessage" => "",
            # "ShowComment" => "Y",
            "ShowSign" => "PEP");
    }

    public function Execute()
    {
        if ($this->isInEventActivityMode)
            return CBPActivityExecutionStatus::Closed;

        $this->Subscribe($this);

        $this->isInEventActivityMode = false;
        return CBPActivityExecutionStatus::Executing;
    }

    public function Subscribe(IBPActivityExternalEventListener $eventHandler)
    {
        if ($eventHandler == null)
            throw new Exception("eventHandler");

        $this->isInEventActivityMode = true;

        $rootActivity = $this->GetRootActivity();
        $documentId = $rootActivity->GetDocumentId();

        $runtime = CBPRuntime::GetRuntime();
        $documentService = $runtime->GetService("DocumentService");

        $arUsersTmp = $this->Users;
        if (!is_array($arUsersTmp))
            $arUsersTmp = array($arUsersTmp);

        if ($this->ApproveType == "any")
            $this->WriteToTrackingService(str_replace("#VAL#", "{=user:" . implode("}, {=user:", $arUsersTmp) . "}", GetMessage("SIGN_ACT_TRACK1")));
        elseif ($this->ApproveType == "all")
            $this->WriteToTrackingService(str_replace("#VAL#", "{=user:" . implode("}, {=user:", $arUsersTmp) . "}", GetMessage("SIGN_ACT_TRACK2")));
        elseif ($this->ApproveType == "vote")
            $this->WriteToTrackingService(str_replace("#VAL#", "{=user:" . implode("}, {=user:", $arUsersTmp) . "}", GetMessage("SIGN_ACT_TRACK3")));

        $arUsers = CBPHelper::ExtractUsers($arUsersTmp, $documentId, false);

        $arParameters = $this->Parameters;
        if (!is_array($arParameters))
            $arParameters = array($arParameters);
        $arParameters["DOCUMENT_ID"] = $documentId;
        $arParameters["DOCUMENT_URL"] = $documentService->GetDocumentAdminPage($documentId);
        $arParameters["TaskButton1Message"] = $this->IsPropertyExists("TaskButton1Message") ? $this->TaskButton1Message : GetMessage("SIGN_ACT_BUTTON1");
        if (strlen($arParameters["TaskButton1Message"]) <= 0)
            $arParameters["TaskButton1Message"] = GetMessage("SIGN_ACT_BUTTON1");
        $arParameters["TaskButton2Message"] = $this->IsPropertyExists("TaskButton2Message") ? $this->TaskButton2Message : GetMessage("SIGN_ACT_BUTTON2");
        if (strlen($arParameters["TaskButton2Message"]) <= 0)
            $arParameters["TaskButton2Message"] = GetMessage("SIGN_ACT_BUTTON2");
        $arParameters["CommentLabelMessage"] = $this->IsPropertyExists("CommentLabelMessage") ? $this->CommentLabelMessage : GetMessage("SIGN_ACT_COMMENT");
        if (strlen($arParameters["CommentLabelMessage"]) <= 0)
            $arParameters["CommentLabelMessage"] = GetMessage("SIGN_ACT_COMMENT");
        # $arParameters["ShowComment"] = $this->IsPropertyExists("ShowComment") ? $this->ShowComment : "Y";
        $arParameters["ShowSign"] = $this->IsPropertyExists("ShowSign") ? $this->ShowSign : "PEP";

        # if ($arParameters["ShowComment"] != "Y" && $arParameters["ShowComment"] != "N")
        # $arParameters["ShowComment"] = "Y";

        $taskService = $this->workflow->GetService("TaskService");
        $this->taskId = $taskService->CreateTask(
            array(
                "USERS" => $arUsers,
                "WORKFLOW_ID" => $this->GetWorkflowInstanceId(),
                "ACTIVITY" => "TrustedSign",
                "ACTIVITY_NAME" => $this->name,
                "OVERDUE_DATE" => $this->OverdueDate,
                "NAME" => $this->Name,
                "DESCRIPTION" => $this->Description,
                "PARAMETERS" => $arParameters,));

        $this->TotalCount = count($arUsers);
        if (!$this->IsPropertyExists("SetStatusMessage") || $this->SetStatusMessage == "Y") {
            $totalCount = $this->TotalCount;
            $message = ($this->IsPropertyExists("StatusMessage") && strlen($this->StatusMessage) > 0) ? $this->StatusMessage : GetMessage("SIGN_ACT_INFO");
            $this->SetStatusTitle(str_replace(
                array("#PERC#", "#PERCENT#", "#REV#", "#VOTED#", "#TOT#", "#TOTAL#", "#APPROVERS#", "#REJECTERS#"),
                array(0, 0, 0, 0, $totalCount, $totalCount, GetMessage("SIGN_ACT_APPROVERS_NONE"), GetMessage("SIGN_ACT_APPROVERS_NONE")),
                $message));
        }

        $timeoutDuration = $this->CalculateTimeoutDuration();
        if ($timeoutDuration > 0) {
            $schedulerService = $this->workflow->GetService("SchedulerService");
            $this->subscriptionId = $schedulerService->SubscribeOnTime($this->workflow->GetInstanceId(), $this->name, time() + $timeoutDuration);
        }

        $this->workflow->AddEventHandler($this->name, $eventHandler);
    }

    private function ReplaceTemplate($str, $ar)
    {
        $str = str_replace("%", "%2", $str);
        foreach ($ar as $key => $val) {
            $val = str_replace("%", "%2", $val);
            $val = str_replace("#", "%1", $val);
            $str = str_replace("#" . $key . "#", $val, $str);
        }
        $str = str_replace("%1", "#", $str);
        $str = str_replace("%2", "%", $str);

        return $str;
    }

    public function Unsubscribe(IBPActivityExternalEventListener $eventHandler)
    {
        if ($eventHandler == null)
            throw new Exception("eventHandler");

        $taskService = $this->workflow->GetService("TaskService");
        $taskService->DeleteTask($this->taskId);

        $timeoutDuration = $this->CalculateTimeoutDuration();
        if ($timeoutDuration > 0) {
            $schedulerService = $this->workflow->GetService("SchedulerService");
            $schedulerService->UnSubscribeOnTime($this->subscriptionId);
        }

        $this->workflow->RemoveEventHandler($this->name, $eventHandler);

        $this->taskId = 0;
        $this->subscriptionId = 0;
    }

    public function HandleFault(Exception $exception)
    {
        if ($exception == null)
            throw new Exception("exception");

        $status = $this->Cancel();
        if ($status == CBPActivityExecutionStatus::Canceling)
            return CBPActivityExecutionStatus::Faulting;

        return $status;
    }

    public function Cancel()
    {
        if (!$this->isInEventActivityMode && $this->taskId > 0)
            $this->Unsubscribe($this);

        return CBPActivityExecutionStatus::Closed;
    }

    protected function ExecuteOnApprove()
    {
        if (count($this->arActivities) <= 0) {
            $this->workflow->CloseActivity($this);
            return;
        }

        $this->WriteToTrackingService(GetMessage("SIGN_ACT_APPROVE"));

        $activity = $this->arActivities[0];
        $activity->AddStatusChangeHandler(self::ClosedEvent, $this);
        $this->workflow->ExecuteActivity($activity);

    }

    protected function ExecuteOnNonApprove()
    {
        if (count($this->arActivities) <= 1) {
            $this->workflow->CloseActivity($this);
            return;
        }

        $this->WriteToTrackingService(GetMessage("SIGN_ACT_NONAPPROVE"));

        $activity = $this->arActivities[1];
        $activity->AddStatusChangeHandler(self::ClosedEvent, $this);
        $this->workflow->ExecuteActivity($activity);
    }

    public function OnExternalEvent($arEventParameters = array())
    {
        if ($this->executionStatus == CBPActivityExecutionStatus::Closed)
            return;

        $timeoutDuration = $this->CalculateTimeoutDuration();
        if ($timeoutDuration > 0) {
            if (array_key_exists("SchedulerService", $arEventParameters) && $arEventParameters["SchedulerService"] == "OnAgent") {
                $this->IsTimeout = 1;
                $this->Unsubscribe($this);
                $this->ExecuteOnNonApprove();
                return;
            }
        }

        if (!array_key_exists("USER_ID", $arEventParameters) || intval($arEventParameters["USER_ID"]) <= 0)
            return;
        if (!array_key_exists("APPROVE", $arEventParameters))
            return;

        $approve = ($arEventParameters["APPROVE"] ? true : false);

        $rootActivity = $this->GetRootActivity();
        $documentId = $rootActivity->GetDocumentId();

        $arUsersTmp = $this->Users;
        $arUsers = CBPHelper::ExtractUsers($arUsersTmp, $documentId, false);

        $arEventParameters["USER_ID"] = intval($arEventParameters["USER_ID"]);
        if (!in_array($arEventParameters["USER_ID"], $arUsers))
            return;

        if ($this->IsPropertyExists("LastApprover"))
            $this->LastApprover = "user_" . $arEventParameters["USER_ID"];

        $taskService = $this->workflow->GetService("TaskService");
        $taskService->MarkCompleted($this->taskId, $arEventParameters["USER_ID"]);

        $dbUser = CUser::GetById($arEventParameters["USER_ID"]);
        if ($arUser = $dbUser->Fetch())
            $this->Comments = $this->Comments .
                CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser) . " (" . $arUser["LOGIN"] . "): " . ($approve ? GetMessage("SIGN_LOG_Y") : GetMessage("SIGN_LOG_N")) . "\n" .
                (strlen($arEventParameters["COMMENT"]) > 0 ? GetMessage("SIGN_LOG_COMMENTS") . ": " . $arEventParameters["COMMENT"] : "") . "\n";

        if ($approve) {
            $this->WriteToTrackingService(
                str_replace(
                    array("#PERSON#", "#COMMENT#"),
                    array("{=user:user_" . $arEventParameters["USER_ID"] . "}", (strlen($arEventParameters["COMMENT"]) > 0 ? ": " . $arEventParameters["COMMENT"] : "")),
                    GetMessage("SIGN_ACT_APPROVE_TRACK")),
                $arEventParameters["USER_ID"]);
        } else {
            $this->WriteToTrackingService(
                str_replace(
                    array("#PERSON#", "#COMMENT#"),
                    array("{=user:user_" . $arEventParameters["USER_ID"] . "}", (strlen($arEventParameters["COMMENT"]) > 0 ? ": " . $arEventParameters["COMMENT"] : "")),
                    GetMessage("SIGN_ACT_NONAPPROVE_TRACK")),
                $arEventParameters["USER_ID"]);
        }

        $result = "Continue";

        $this->arApproveResults[$arEventParameters["USER_ID"]] = $approve;

        if ($approve)
            $this->ApprovedCount = $this->ApprovedCount + 1;
        else
            $this->NotApprovedCount = $this->NotApprovedCount + 1;

        $this->VotedCount = count($this->arApproveResults);
        $this->VotedPercent = intval($this->VotedCount / $this->TotalCount * 100);
        $this->ApprovedPercent = intval($this->ApprovedCount / $this->TotalCount * 100);
        $this->NotApprovedPercent = intval($this->NotApprovedCount / $this->TotalCount * 100);

        if ($this->ApproveType == "any") {
            $result = ($approve ? "Approve" : "NonApprove");
        } elseif ($this->ApproveType == "all" || ($this->ApproveType == "vote" && $this->ApproveMinPercent >= 100)) {
            if (!$approve) {
                $result = "NonApprove";
            } else {
                $allAproved = true;
                foreach ($arUsers as $userId) {
                    if (!isset($this->arApproveResults[$userId])) // && $this->arApproveResults[$userId]!==true
                        $allAproved = false;
                }

                if ($allAproved)
                    $result = "Approve";
            }
        } elseif ($this->ApproveType == "vote") {
            if ($this->ApproveWaitForAll == "Y") {
                if ($this->VotedPercent == 100) {
                    if ($this->ApprovedPercent > $this->ApproveMinPercent)
                        $result = "Approve";
                    else
                        $result = "NonApprove";
                }
            } else {
                if ($this->ApprovedPercent > $this->ApproveMinPercent)
                    $result = "Approve";
                elseif (($this->VotedCount - $this->ApprovedCount) / $this->TotalCount * 100 >= 100 - $this->ApproveMinPercent)
                    $result = "NonApprove";
            }
        }

        $approvers = "";
        $rejecters = "";
        if (!$this->IsPropertyExists("SetStatusMessage") || $this->SetStatusMessage == "Y") {
            $messageTemplate = ($this->IsPropertyExists("StatusMessage") && strlen($this->StatusMessage) > 0) ? $this->StatusMessage : GetMessage("SIGN_ACT_INFO");
            $votedPercent = $this->VotedPercent;
            $votedCount = $this->VotedCount;
            $totalCount = $this->TotalCount;

            if (strpos($messageTemplate, "#REJECTERS#") !== false)
                $rejecters = $this->GetApproversNames(false);
            if (strpos($messageTemplate, "#APPROVERS#") !== false)
                $approvers = $this->GetApproversNames(true);

            $approversTmp = $approvers;
            $rejectersTmp = $rejecters;
            if ($approversTmp == "")
                $approversTmp = GetMessage("SIGN_ACT_APPROVERS_NONE");
            if ($rejectersTmp == "")
                $rejectersTmp = GetMessage("SIGN_ACT_APPROVERS_NONE");

            $this->SetStatusTitle(str_replace(
                array("#PERC#", "#PERCENT#", "#REV#", "#VOTED#", "#TOT#", "#TOTAL#", "#APPROVERS#", "#REJECTERS#"),
                array($votedPercent, $votedPercent, $votedCount, $votedCount, $totalCount, $totalCount, $approversTmp, $rejectersTmp),
                $messageTemplate));
        }

        if ($result != "Continue") {
            $this->Unsubscribe($this);

            if ($rejecters == "")
                $this->Rejecters = $this->GetApproversNames(false);
            else
                $this->Rejecters = $rejecters;

            if ($approvers == "")
                $this->Approvers = $this->GetApproversNames(true);
            else
                $this->Approvers = $approvers;

            if ($result == "Approve")
                $this->ExecuteOnApprove();
            else
                $this->ExecuteOnNonApprove();

        }
    }

    private function GetApproversNames($b)
    {
        $result = "";

        foreach ($this->arApproveResults as $k => $v) {
            if ($b && $v || !$b && !$v) {
                $dbUsers = CUser::GetByID($k);
                if ($arUser = $dbUsers->Fetch()) {
                    if ($result != "")
                        $result .= ", ";
                    $result .= CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser) . " (" . $arUser["LOGIN"] . ")";
                }
            }
        }

        /*		$ar = array();
        foreach ($this->arApproveResults as $k => $v){
        if ($b && $v || !$b && !$v)
        $ar[] = $k;
        }

        if (count($ar) > 0){
        $dbUsers = CUser::GetList(($b = ""), ($o = ""), array("ID" => $ar));
        while ($arUser = $dbUsers->Fetch()){
        if ($result != "")
        $result .= ", ";
        $result .= $arUser["NAME"]." ".$arUser["LAST_NAME"]." (".$arUser["LOGIN"].")";
        }
        }*/

        return $result;
    }

    protected function OnEvent(CBPActivity $sender)
    {
        $sender->RemoveStatusChangeHandler(self::ClosedEvent, $this);
        $this->workflow->CloseActivity($this);
    }

    protected function ReInitialize()
    {
        parent::ReInitialize();

        $this->arApproveResults = array();
        $this->ApprovedCount = 0;
        $this->NotApprovedCount = 0;

        $this->VotedCount = 0;
        $this->VotedPercent = 0;
        $this->ApprovedPercent = 0;
        $this->NotApprovedPercent = 0;
        $this->Comments = '';
        $this->IsTimeout = 0;
        $this->Approvers = '';
        $this->Rejecters = '';
    }

    public static function ShowTaskForm($arTask, $userId, $userName = "")
    {
		function pathinfo_utf($path) {

			if (strpos($path, '/') !== false)
				$basename = end(explode('/', $path));
			elseif (strpos($path, '\\') !== false)
				$basename = end(explode('\\', $path));
			else
				return false;

			if (!$basename)
				return false;

			$dirname = substr($path, 0,
				strlen($path) - strlen($basename) - 1);

			if (strpos($basename, '.') !== false) {
				$extension = end(explode('.', $path));
				$filename = substr($basename, 0,
					strlen($basename) - strlen($extension) - 1);
			} else {
				$extension = '';
				$filename = $basename;
			}

			return array (
				'dirname' => $dirname,
				'basename' => $basename,
				'extension' => $extension,
				'filename' => $filename
			);
		}

//?????????, ??? ?????? ????????????. ???? ??? ?????????? ??????????
        if ($arTask[MODULE_ID] == "disk") {
            CModule::IncludeModule("disk");
            $fileModel = Bitrix\Disk\File::loadById($arTask[DOCUMENT_ID]);
            //  $fileArray = $fileModel->getFile();
            $lastVersId = $fileModel->getFileId();
            $file = CFile::GetFileArray($lastVersId);
            $extfile = strtolower(pathinfo($file[FILE_NAME], PATHINFO_EXTENSION));
            $filename = $file[FILE_NAME];
            if ($extfile == "sig") {
                $filenames = $file[FILE_NAME];
            } else {
                $filenames = $file[FILE_NAME] . ".sig";
            }
            $hredocus = $file[SRC];
            if ($hredocus) {
                $textpred = Array("TYPE" => "OK", "MESSAGE" => "");
            } else {
                $textpred = Array("TYPE" => "ERROR", "MESSAGE" => GetMessage("SIGN_NO_DOC_ACTIV"));
            }
        } elseif (($arTask[MODULE_ID] == "webdav")||($arTask[MODULE_ID] == "lists")) {

            if (CModule::IncludeModule('iblock')) {
                $docObject = CIBlockDocument::GetDocument($arTask[DOCUMENT_ID]);
				                $res = $docObject[PROPERTY_FILE];
                foreach ($res as $key => $value) {
                    $hredocus = $value;
                }
                if ($hredocus) {
                    $textpred = Array("TYPE" => "OK", "MESSAGE" => "");
                } else {
                    $textpred = Array("TYPE" => "ERROR", "MESSAGE" => GetMessage("SIGN_NO_DOC_ACTIV"));
                }

                // ???????? ? ????????? ?????????? ?????????
				$FileExplode = pathinfo_utf($hredocus);
                $extfile = strtolower($FileExplode[extension]);
                $filename = $FileExplode[filename].".".$extfile;
                if ($extfile == "sig") {
                    $filenames = $filename;
                } else {
                    $filenames = $filename . ".sig";
                }
            }
        } else {
            $hredocus = false;
            $textpred = Array("TYPE" => "ERROR", "MESSAGE" => GetMessage("SIGN_NO_MOD_ACTIV"));
        }
        $form = '';

        if (!array_key_exists("ShowComment", $arTask["PARAMETERS"]) || ($arTask["PARAMETERS"]["ShowComment"] != "N")) {
            $form .= ShowMessage($textpred) .
                '<tr><td valign="top" width="40%" align="right" class="bizproc-field-name">' . (strlen($arTask["PARAMETERS"]["CommentLabelMessage"]) > 0 ? $arTask["PARAMETERS"]["CommentLabelMessage"] : GetMessage("SIGN_ACT_COMMENT")) . ':</td>' .
                '<td valign="top" width="60%" class="bizproc-field-value">' .
                '<textarea rows="3" cols="50" id="task_comment" name="task_comment"></textarea>' .
                '<input type="hidden"  name="filename" value="' . $filenames . '">' .
                '<input type="hidden" id="fileName" value="' . $filename . '">' .
                '<input type="hidden" id="fileURL" value="' . $hredocus . '"/>' .
                '<input type="hidden" id="moduleID" value="' . $arTask[MODULE_ID] . '"/>' .
                '<input type="hidden" id="docID" value="' . $arTask[DOCUMENT_ID] . '"/>' .
                '<td valign="top" width="40%" align="right" style="display:none;" class="bizproc-field-name"></td>' .
                '<td valign="top" width="60%" class="bizproc-field-value">' .
                '<textarea id="pkcs7" rows="20" cols="50" style="display:none;width:100%" name="sign_content"></textarea>' .
                '</td></tr>';
        }

        $buttons =
            '<input type="button" id="approvebutton" ' . ($hredocus ? " " : "disabled") . '
name="approve" onclick="onCapicomClick();"   value="' . (strlen
            ($arTask["PARAMETERS"]["TaskButton1Message"]) >
            0 ? $arTask["PARAMETERS"]["TaskButton1Message"] : GetMessage("SIGN_ACT_BUTTON1")) . '"/>' .
            '<input type="button"
id="nonapprovebutton" onclick="confirmation();"
name="nonapprove"
value="' . (strlen($arTask["PARAMETERS"]["TaskButton2Message"]) > 0 ? $arTask["PARAMETERS"]["TaskButton2Message"] : GetMessage("SIGN_ACT_BUTTON2")) . '"/>';

        return array($form, $buttons);

    }

    public static function getTaskControls($arTask)
    {
        return array(
            'BUTTONS' => array(
                array(
                    'TYPE' => 'submit',
                    'TARGET_USER_STATUS' => CBPTaskUserStatus::Yes,
                    'NAME' => 'approve',
                    'VALUE' => 'Y',
                    'TEXT' => strlen($arTask["PARAMETERS"]["TaskButton1Message"]) > 0 ? $arTask["PARAMETERS"]["TaskButton1Message"] : GetMessage("BPAA_ACT_BUTTON1")
                ),
                array(
                    'TYPE' => 'submit',
                    'TARGET_USER_STATUS' => CBPTaskUserStatus::No,
                    'NAME' => 'nonapprove',
                    'VALUE' => 'Y',
                    'TEXT' => strlen($arTask["PARAMETERS"]["TaskButton2Message"]) > 0 ? $arTask["PARAMETERS"]["TaskButton2Message"] : GetMessage("BPAA_ACT_BUTTON2")
                )
            )
        );
    }

    public static function PostTaskForm($arTask, $userId, $arRequest, &$arErrors, $userName = "", $realUserId = null)
    {
        $arErrors = array();

        try {
            $userId = intval($userId);
            if ($userId <= 0)
                throw new CBPArgumentNullException("userId");

            $arEventParameters = array(
                "USER_ID" => $userId,
                "REAL_USER_ID" => $realUserId,
                "USER_NAME" => $userName,
                "COMMENT" => isset($arRequest["task_comment"]) ? $arRequest["task_comment"] : '',
            );

            if (isset($arRequest['approve']) && strlen($arRequest["approve"]) > 0
                || isset($arRequest['INLINE_USER_STATUS']) && $arRequest['INLINE_USER_STATUS'] == CBPTaskUserStatus::Yes
            )
                $arEventParameters["APPROVE"] = true;
            elseif (isset($arRequest['nonapprove']) && strlen($arRequest["nonapprove"]) > 0
                || isset($arRequest['INLINE_USER_STATUS']) && $arRequest['INLINE_USER_STATUS'] == CBPTaskUserStatus::No
            )
                $arEventParameters["APPROVE"] = false;
            else
                throw new CBPNotSupportedException(GetMessage("SIGN_ACT_NO_ACTION"));

            CBPRuntime::SendExternalEvent($arTask["WORKFLOW_ID"], $arTask["ACTIVITY_NAME"], $arEventParameters);

            return true;
        } catch (Exception $e) {
            $arErrors[] = array(
                "code" => $e->getCode(),
                "message" => $e->getMessage(),
                "file" => $e->getFile() . " [" . $e->getLine() . "]",
            );
        }

        return false;
    }
//    public static function PostTaskForm($arTask, $userId, $arRequest, & $arErrors, $userName = "")
//    {
//
//        $arErrors = array();
//
//        try {
//            $userId = intval($userId);
//            if ($userId <= 0)
//                throw new CBPArgumentNullException("userId");
//
//            $arEventParameters = array(
//                "USER_ID" => $userId,
//                "USER_NAME" => $userName,
//                "COMMENT" => $arRequest["task_comment"],
//                /*"SIGN_CONTENT" => $arRequest["sign_content"],*/);
//
//
//            if (strlen($arRequest["approve"]) > 0) {
//
//                $arEventParameters["APPROVE"] = true;
//            } elseif (strlen($arRequest["nonapprove"]) > 0)
//                $arEventParameters["APPROVE"] = false;
//            else
//                throw new CBPNotSupportedException(GetMessage("SIGN_ACT_NO_ACTION"));
//
//            CBPRuntime::SendExternalEvent($arTask["WORKFLOW_ID"], $arTask["ACTIVITY_NAME"], $arEventParameters);
//
//            return true;
//        } catch (Exception $e) {
//            $arErrors[] = array(
//                "code" => $e->getCode(),
//                "message" => $e->getMessage(),
//                "file" => $e->getFile() . " [" . $e->getLine() . "]",);
//        }
//
//        return false;
//    }

    public static function ValidateProperties($arTestProperties = array(), CBPWorkflowTemplateUser $user = null)
    {
        $arErrors = array();

        if (!array_key_exists("Users", $arTestProperties)) {
            $bUsersFieldEmpty = true;
        } else {
            if (!is_array($arTestProperties["Users"]))
                $arTestProperties["Users"] = array($arTestProperties["Users"]);

            $bUsersFieldEmpty = true;
            foreach ($arTestProperties["Users"] as $userId) {
                if (!is_array($userId) && (strlen(trim($userId)) > 0) || is_array($userId) && (count($userId) > 0)) {
                    $bUsersFieldEmpty = false;
                    break;
                }
            }
        }

        if ($bUsersFieldEmpty)
            $arErrors[] = array("code" => "NotExist", "parameter" => "Users", "message" => GetMessage("SIGN_ACT_PROP_EMPTY1"));

        if (!array_key_exists("ApproveType", $arTestProperties)) {
            $arErrors[] = array("code" => "NotExist", "parameter" => "ApproveType", "message" => GetMessage("SIGN_ACT_PROP_EMPTY2"));
        } else {
            if (!in_array($arTestProperties["ApproveType"], array("any", "all", "vote")))
                $arErrors[] = array("code" => "NotInRange", "parameter" => "ApproveType", "message" => GetMessage("SIGN_ACT_PROP_EMPTY3"));
        }

        if (!array_key_exists("Name", $arTestProperties) || strlen($arTestProperties["Name"]) <= 0) {
            $arErrors[] = array("code" => "NotExist", "parameter" => "Name", "message" => GetMessage("SIGN_ACT_PROP_EMPTY4"));
        }

        return array_merge($arErrors, parent::ValidateProperties($arTestProperties, $user));
    }

    private function CalculateTimeoutDuration()
    {
        $timeoutDuration = ($this->IsPropertyExists("TimeoutDuration") ? $this->TimeoutDuration : 0);

        $timeoutDurationType = ($this->IsPropertyExists("TimeoutDurationType") ? $this->TimeoutDurationType : "s");
        $timeoutDurationType = strtolower($timeoutDurationType);
        if (!in_array($timeoutDurationType, array("s", "d", "h", "m")))
            $timeoutDurationType = "s";

        $timeoutDuration = intval($timeoutDuration);
        switch ($timeoutDurationType) {
            case 'd':
                $timeoutDuration *= 3600 * 24;
                break;
            case 'h':
                $timeoutDuration *= 3600;
                break;
            case 'm':
                $timeoutDuration *= 60;
                break;
            default:
                break;
        }

        return $timeoutDuration;
    }

    public static function GetPropertiesDialog($documentType, $activityName, $arWorkflowTemplate, $arWorkflowParameters, $arWorkflowVariables, $arCurrentValues = null, $formName = "")
    {
        $runtime = CBPRuntime::GetRuntime();

        $arMap = array(
            "Users" => "approve_users",
            "ApproveType" => "approve_type",
            "ApproveMinPercent" => "approve_percent",
            "OverdueDate" => "approve_overdue_date",
            "Name" => "approve_name",
            "Description" => "approve_description",
            "Parameters" => "approve_parameters",
            "ApproveWaitForAll" => "approve_wait",
            "StatusMessage" => "status_message",
            "SetStatusMessage" => "set_status_message",
            "TimeoutDuration" => "timeout_duration",
            "TimeoutDurationType" => "timeout_duration_type",
            "TaskButton1Message" => "task_button1_message",
            "TaskButton2Message" => "task_button2_message",
            "CommentLabelMessage" => "comment_label_message",
            # "ShowComment" => "show_comment",
            "ShowSign" => "show_sign");

        if (!is_array($arWorkflowParameters))
            $arWorkflowParameters = array();
        if (!is_array($arWorkflowVariables))
            $arWorkflowVariables = array();

        if (!is_array($arCurrentValues)) {
            $arCurrentValues = Array();
            $arCurrentActivity =  &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
            if (is_array($arCurrentActivity["Properties"])) {
                foreach ($arMap as $k => $v) {
                    if (array_key_exists($k, $arCurrentActivity["Properties"])) {
                        if ($k == "Users") {
                            $arCurrentValues[$arMap[$k]] = CBPHelper::UsersArrayToString($arCurrentActivity["Properties"][$k], $arWorkflowTemplate, $documentType);
                        } elseif ($k == "TimeoutDuration") {
                            $arCurrentValues["timeout_duration"] = $arCurrentActivity["Properties"]["TimeoutDuration"];
                            if (!preg_match('#^{=[A-Za-z0-9_]+:[A-Za-z0-9_]+}$#i', $arCurrentValues["timeout_duration"])
                                && !array_key_exists("TimeoutDurationType", $arCurrentActivity["Properties"])
                            ) {
                                $arCurrentValues["timeout_duration"] = intval($arCurrentValues["timeout_duration"]);
                                $arCurrentValues["timeout_duration_type"] = "s";
                                if ($arCurrentValues["timeout_duration"] % (3600 * 24) == 0) {
                                    $arCurrentValues["timeout_duration"] = $arCurrentValues["timeout_duration"] / (3600 * 24);
                                    $arCurrentValues["timeout_duration_type"] = "d";
                                } elseif ($arCurrentValues["timeout_duration"] % 3600 == 0) {
                                    $arCurrentValues["timeout_duration"] = $arCurrentValues["timeout_duration"] / 3600;
                                    $arCurrentValues["timeout_duration_type"] = "h";
                                } elseif ($arCurrentValues["timeout_duration"] % 60 == 0) {
                                    $arCurrentValues["timeout_duration"] = $arCurrentValues["timeout_duration"] / 60;
                                    $arCurrentValues["timeout_duration_type"] = "m";
                                }
                            }
                        } else {
                            $arCurrentValues[$arMap[$k]] = $arCurrentActivity["Properties"][$k];
                        }
                    } else {
                        if (!is_array($arCurrentValues) || !array_key_exists($arMap[$k], $arCurrentValues))
                            $arCurrentValues[$arMap[$k]] = "";
                    }
                }
            } else {
                foreach ($arMap as $k => $v)
                    $arCurrentValues[$arMap[$k]] = "";
            }

            if (strlen($arCurrentValues["approve_wait"]) <= 0)
                $arCurrentValues["approve_wait"] = "N";

            if (strlen($arCurrentValues["approve_percent"]) <= 0)
                $arCurrentValues["approve_percent"] = "50";
        }

        if (strlen($arCurrentValues['status_message']) <= 0)
            $arCurrentValues['status_message'] = GetMessage("SIGN_ACT_INFO");
        if (strlen($arCurrentValues['task_button1_message']) <= 0)
            $arCurrentValues['task_button1_message'] = GetMessage("SIGN_ACT_BUTTON1");
        if (strlen($arCurrentValues['task_button2_message']) <= 0)
            $arCurrentValues['task_button2_message'] = GetMessage("SIGN_ACT_BUTTON2");
        if (strlen($arCurrentValues['comment_label_message']) <= 0)
            $arCurrentValues['comment_label_message'] = GetMessage("SIGN_ACT_COMMENT");
        if (strlen($arCurrentValues["timeout_duration_type"]) <= 0)
            $arCurrentValues["timeout_duration_type"] = "s";

        $documentService = $runtime->GetService("DocumentService");
        $arDocumentFields = $documentService->GetDocumentFields($documentType);

        return $runtime->ExecuteResourceFile(
            __FILE__,
            "properties_dialog.php",
            array(
                "arCurrentValues" => $arCurrentValues,
                "arDocumentFields" => $arDocumentFields,
                "formName" => $formName,));
    }

    public static function GetPropertiesDialogValues($documentType, $activityName, & $arWorkflowTemplate, & $arWorkflowParameters, & $arWorkflowVariables, $arCurrentValues, & $arErrors)
    {
        $arErrors = array();

        $runtime = CBPRuntime::GetRuntime();

        $arMap = array(
            "approve_users" => "Users",
            "approve_type" => "ApproveType",
            "approve_overdue_date" => "OverdueDate",
            "approve_percent" => "ApproveMinPercent",
            "approve_wait" => "ApproveWaitForAll",
            "approve_name" => "Name",
            "approve_description" => "Description",
            "approve_parameters" => "Parameters",
            "status_message" => "StatusMessage",
            "set_status_message" => "SetStatusMessage",
            "timeout_duration" => "TimeoutDuration",
            "timeout_duration_type" => "TimeoutDurationType",
            "task_button1_message" => "TaskButton1Message",
            "task_button2_message" => "TaskButton2Message",
            "comment_label_message" => "CommentLabelMessage",
            # "show_comment" => "ShowComment",
            "show_sign" => "ShowSign");

        $arProperties = array();
        foreach ($arMap as $key => $value) {
            if ($key == "approve_users")
                continue;

            if (strlen($arCurrentValues[$key . "_X"]) > 0)
                $arProperties[$value] = $arCurrentValues[$key . "_X"];
            else
                $arProperties[$value] = $arCurrentValues[$key];
        }

        $arProperties["Users"] = CBPHelper::UsersStringToArray($arCurrentValues["approve_users"], $documentType, $arErrors);
        if (count($arErrors) > 0)
            return false;

        $arErrors = self::ValidateProperties($arProperties, new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser));
        if (count($arErrors) > 0)
            return false;

        $arCurrentActivity =  &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
        $arCurrentActivity["Properties"] = $arProperties;

        return true;
    }
}

?>

<script>
    var popup = new BX.CDialog({
        'content': '<div class="bx-core-waitwindow" style="margin:0 auto; background-color:transparent; border:none; position:relative;">' + BX.message('TBSIGN_WAIT') + '</div>',
        'title': BX.message('TBSIGN_ATT'),
        'width': '400',
        'height': '80'
    });

    function ajax(clickId, pkcs7) {
        var module = document.getElementById("moduleID").value;
        $.ajax({
            type: "POST",
            async: false,
            url: '/bitrix/activities/custom/trustedsign/ajax.php', // ????????? URL ?
            data: {'id': clickId, 'pkcs7': pkcs7, 'module': module}, // ??????? id
            dataType: "text", // ??? ??????????? ??????
            success: function (data, textStatus) { // ?????? ???? ?????????? ?? ??????? success
                if (data.indexOf('____SECFILTER_ACCEPT_JS') + 1) {
                    popup.ClearButtons();
                    popup.SetContent(BX.message('TBSIGN_ERROR_PROACTIVE'));
                    popup.SetButtons([BX.CDialog.prototype.btnClose]);
                }
                if (data == 1) {
                    if ($("table").is(".popup-window")) {
                        $('.bp-popup-title').append('<div class="bp-button-accept">' + BX.message("TBSIGN_SUCCESS") + '</div>');
                        document.getElementById('approvebutton').setAttribute('type', 'submit');
                        document.getElementById('approvebutton').removeAttribute('onclick');
                        document.getElementById('approvebutton').click();

                        popup.Close();
                    }
                    else {
                        popup.ClearButtons();
                        popup.SetContent('<div id="wd_upload_ok_message" style="color:#009900;"><br>' + BX.message('TBSIGN_SUCCESS'));
                        +'<br></div>';
                        popup.SetButtons([
                            {
                                title: BX.message('TBSIGN_CANCEL'),
                                action: function () {
                                    document.getElementById('approvebutton').setAttribute('type', 'submit');
                                    document.getElementById('approvebutton').removeAttribute('onclick');
                                    document.getElementById('approvebutton').click();
                                    popup.Close();
                                }
                            }
                        ]);
                    }
                }
                if (data == 0) {
                    popup.ClearButtons();
                    popup.SetContent(BX.message('TBSIGN_ERROR_SAVE_FILE'));
                    popup.SetButtons([BX.CDialog.prototype.btnClose]);
                }
            },
            error: function (xhr, status, error) {
                // ???? ?? ?????? ?? ???????????, ??????? ???? ? ???????
                popup.ClearButtons();
                popup.SetContent(BX.message('TBSIGN_ERROR_AJAX') + status + ': ' + error);
                popup.SetButtons([BX.CDialog.prototype.btnClose]);
            }
        });
    }

    function onCapicomClick() {
        popup.Show();
        popup.ClearButtons();
        $('.bx-core-adm-icon-close').remove();
        var ID = document.getElementById("docID").value;
        var url = document.getElementById("fileURL").value; // get url string from DOM input
        var fileName = document.getElementById("fileName").value;
        var parts,
            ext = (parts = fileName.split("/").pop().split(".")).length > 1 ? parts.pop() : "";
        var signature = new TBSignedData(); // ?????? ?????? ???????

        if (ext == 'sig') {
            popup.SetContent('<div class="bx-core-waitwindow" style="margin:0 auto; background-color:transparent; border:none; position:relative;">' + BX.message('TBSIGN_COSIGN_POPUP1') + '</div>');
            var fileSign = signature.CosignFile(url); // ?????????? ????? ????????? ???????
            popup.SetContent('<div class="bx-core-waitwindow" style="margin:0 auto; background-color:transparent; border:none; position:relative;">' + BX.message('TBSIGN_COSIGN_POPUP2') + '</div>');
            for (var key in fileSign.error) {      // ????????? ??????
                if (key === '0') {                     // ???? 0, ?? ?????? ???, ?????????? ?????????? ?????????
                    ajax(ID, fileSign.signature);
                }
                else if (key === '5') {    // ??? 5, ?? ?? ?????? ??????? ??????, ??????? ???? ? c??????
                    popup.ClearButtons();
                    popup.SetContent(fileSign.error[key]);
                    popup.SetButtons([
                        {
                            title: BX.message('TBSIGN_CRIPTOARM'),
                            name: 'download',
                            id: 'downloadCryptoArm',
                            action: function () {
                                window.open('http://www.trusted.ru/products/cryptoarm/', '_blank');
                                popup.Close();
                            }
                        },
                        BX.CDialog.prototype.btnClose]);
                }
                else {
                    popup.ClearButtons();
                    popup.SetContent(fileSign.error[key]);
                    popup.SetButtons([BX.CDialog.prototype.btnClose]);
                }   // ? ????????? ??????? ?????? ?????????? ????????? ? ??????? fileSign.error[key]
            }
        } else {
            popup.SetContent('<div class="bx-core-waitwindow" style="margin:0 auto; background-color:transparent; border:none; position:relative;">' + BX.message('TBSIGN_SIGN_POPUP1') + '</div>');
            var fileSign = signature.SignFile(url); // ?????????? ????? ????????? ???????
            popup.SetContent('<div class="bx-core-waitwindow" style="margin:0 auto; background-color:transparent; border:none; position:relative;">' + BX.message('TBSIGN_SIGN_POPUP2') + '</div>');
            for (var key in fileSign.error) {      // ????????? ??????
                if (key === '0') {
                    ajax(ID, fileSign.signature);// ???? 0, ?? ?????? ???, ?????????? ?????????? ?????????
                }
                else if (key === '5') {    // ??? 5, ?? ?? ?????? ??????? ??????, ??????? ???? ? c??????
                    popup.ClearButtons();
                    popup.SetContent(fileSign.error[key]);
                    popup.SetButtons([
                        {
                            title: BX.message('TBSIGN_CRIPTOARM'),
                            name: 'download',
                            id: 'downloadCryptoArm',
                            action: function () {
                                window.open('http://www.trusted.ru/products/cryptoarm/', '_blank');
                                popup.Close();
                            }
                        },
                        BX.CDialog.prototype.btnClose]);
                }
                else {
                    popup.ClearButtons();
                    popup.SetContent(fileSign.error[key]);
                    popup.SetButtons([BX.CDialog.prototype.btnClose]);
                }   // ? ????????? ??????? ?????? ?????????? ????????? ? ??????? fileSign.error[key]
            }
        }

    }
    function confirmation() {
        if ($("table").is(".popup-window")) {
            $('.bp-popup-title').append('<div class="bp-button-decline">' + BX.message("SIGN_ACT_NONAPPROVE") + '</div>');
            document.getElementById('nonapprovebutton').setAttribute('type', 'submit');
            document.getElementById('nonapprovebutton').removeAttribute('onclick');
            document.getElementById('nonapprovebutton').click();
        } else {
            var popup = new BX.CDialog({
                'content': '<?=GetMessage("SIGN_NOAPPROVE_QUN")?>',
                'title': BX.message('TBSIGN_ATT'),
                'width': '400',
                'height': '80',
                'buttons': [
                    {
                        title: '<?=GetMessage("SIGN_YES")?>',
                        action: function () {
                            document.getElementById('nonapprovebutton').setAttribute('type', 'submit');
                            document.getElementById('nonapprovebutton').removeAttribute('onclick');
                            document.getElementById('nonapprovebutton').click();
                            popup.Close();
                        }
                    },
                    {
                        title: '<?=GetMessage("SIGN_ACT_APPROVERS_NONE")?>',
                        action: function () {
                            popup.Close();
                        }
                    }
                ]
            });
            popup.Show();
        }
    }
    BX.ready(function () {

        $('.bp-button.bp-button.bp-button-accept').attr('onclick', 'onCapicomClick();');
        $('.bp-button.bp-button.bp-button-accept').attr('id', 'approvebutton');
        document.getElementById('approvebutton').setAttribute('type', 'button');

        $('.bp-button.bp-button.bp-button-decline').attr('onclick', 'confirmation();');
        $('.bp-button.bp-button.bp-button-decline').attr('id', 'nonapprovebutton');
        document.getElementById('nonapprovebutton').setAttribute('type', 'button');

        if (document.getElementById("fileURL").value == "") {

            $('.bp-button.bp-button.bp-button-accept').attr('disabled', 'disabled');
            $('.bp-button.bp-button.bp-button-accept').removeClass('bp-button-accept');
        }
    });
</script>
