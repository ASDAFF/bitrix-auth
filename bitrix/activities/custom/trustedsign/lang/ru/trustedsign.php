<?
$MESS ['SIGN_ACT_BUTTON1'] = "Подписать документ";
$MESS ['SIGN_LOG_Y'] = "Подписан";
$MESS ['SIGN_ACT_COMMENT'] = "Комментарий";
$MESS ['SIGN_LOG_COMMENTS'] = "Комментарий";
$MESS ['SIGN_ACT_BUTTON2'] = "Отклонить документ";
$MESS ['SIGN_LOG_N'] = "Отклонен";
$MESS ['SIGN_ACT_NO_ACTION'] = "Не указано действие";
$MESS ['SIGN_ACT_INFO'] = "Проголосовало #PERCENT#% (#VOTED# из #TOTAL#)";
$MESS ['SIGN_ACT_APPROVE'] = "Документ принят";
$MESS ['SIGN_ACT_NONAPPROVE'] = "Документ отклонен";
$MESS ['SIGN_ACT_TRACK2'] = "Документ должны принять все пользователи из списка: #VAL#";
$MESS ['SIGN_ACT_TRACK1'] = "Документ должен принять любой пользователь из списка: #VAL#";
$MESS ['SIGN_ACT_TRACK3'] = "Документ будет принят голосованием пользователей из списка: #VAL#";
$MESS ['SIGN_ACT_PROP_EMPTY4'] = "Свойство 'Название' не указано.";
$MESS ['SIGN_ACT_PROP_EMPTY2'] = "Свойство 'Тип одобрения' не указано.";
$MESS ['SIGN_ACT_APPROVE_TRACK'] = "Пользователь #PERSON# подписал документ#COMMENT#";
$MESS ['SIGN_ACT_NONAPPROVE_TRACK'] = "Пользователь #PERSON# отклонил документ#COMMENT#";
$MESS ['SIGN_ACT_PROP_EMPTY1'] = "Свойство 'Пользователи' не указано.";
$MESS ['SIGN_ACT_PROP_EMPTY3'] = "Значение свойства 'Тип одобрения' не корректно.";
$MESS ['SIGN_ACT_APPROVERS_NONE'] = "Нет";
$MESS ['SIGN_YES'] = "Да";
$MESS ['SIGN_NOAPPROVE_QUN'] = "Вы действительно хотите отклонить документ?";
$MESS ['SIGN_NO_DOC_ACTIV'] = "Документ не найден. Подпись невозможна.";
$MESS ['SIGN_NO_MOD_ACTIV'] = "Внимание! Подпись невозможна. Подписать можно документ из модуля 'Диск' или 'Библиотека документов'";
?>