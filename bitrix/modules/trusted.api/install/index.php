<?
IncludeModuleLangFile(__FILE__);
if (class_exists("trusted_api"))
    return;

Class trusted_api extends CModule
{
    var $MODULE_ID = "trusted.api";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = "Y";


    function trusted_api()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        } else {
            $this->MODULE_VERSION = TASKFROMEMAIL_MODULE_VERSION;
            $this->MODULE_VERSION_DATE = TASKFROMEMAIL_MODULE_VERSION_DATE;
        }

        $this->MODULE_NAME = GetMessage("MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("MODULE_DESCRIPTION");

        $this->PARTNER_NAME = GetMessage("PARTNER_NAME");
        $this->PARTNER_URI = "http://digt.ru/";
    }

    function DoInstall()
    {
        //  global $APPLICATION;
        if (!IsModuleInstalled("trusted.api")) {
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();

        }
        return true;
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        return true;
    }


    function InstallDB()
    {
        RegisterModule("trusted.api");
        return true;
    }

    function UnInstallDB()
    {
        UnRegisterModule("trusted.api");
        return true;
    }


    function InstallEvents()
    {
        RegisterModuleDependences("main", "OnBeforeEndBufferContent", $this->MODULE_ID, "CTrustedapi", "AddScript", "100");
        return true;
    }

    function UnInstallEvents()
    {
        UnRegisterModuleDependences("main", "OnBeforeEndBufferContent", $this->MODULE_ID, "CTrustedapi", "AddScript");
        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/trusted.api/install/js", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/trusted.api/", true, true);
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx("/bitrix/js/trusted.api/");
        return true;
    }


} //end class
?>