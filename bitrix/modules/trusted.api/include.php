<?
IncludeModuleLangFile(__FILE__);

class CTrustedApi
{

    function AddScript()
    {
        CJSCore::RegisterExt("trusted_api", Array(
            "js" => "/bitrix/js/trusted.api/sign.js",
            "lang" => "/bitrix/modules/trusted.api/lang/ru/lang_js.php",
            "rel" => array('jquery')
        ));

        if (CModule::IncludeModule('trusted.api')) {
            if (!defined(ADMIN_SECTION) && ADMIN_SECTION !== true) {
                CJSCore::Init(array("trusted_api"));
            }
        }
    }
}

?>