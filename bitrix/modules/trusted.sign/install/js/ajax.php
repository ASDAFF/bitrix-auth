<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("disk");
CModule::IncludeModule("iblock");
use Bitrix\Disk\File;
use Bitrix\Disk\Document\FileData;
use Bitrix\Disk\Driver;
use Bitrix\Disk\Object;
use Bitrix\Disk\Folder;

$id = htmlspecialchars(strip_tags($_POST["id"]));
$ext = htmlspecialchars(strip_tags($_POST["ext"]));
$module = htmlspecialchars(strip_tags($_POST["module"]));

$path = $_SERVER["DOCUMENT_ROOT"];
if ($module == "webdav")
{
	if ($id > 0)
	{
		$docObject = CIBlockDocument::GetDocument($id);
		$res = $docObject[PROPERTY_FILE];
		foreach ($res as $key => $value)
		{
			$hredocus = $value;
		}
		if ($ext == 'sign')
		{ // ???? ???????? ?? ???????????, ??????? ?????? ?? ????
			echo $hredocus;
		} elseif ($ext == 'cosign')
		{
			echo $hredocus;
		} elseif ($ext == 'remove')
		{ // ??? ??? ???????????? ?????????. ????????? ???
			$FileExplode = pathinfo_utf($hredocus);
			$fName = $FileExplode[filename];
			$extFile = strtolower(pathinfo($fName, PATHINFO_EXTENSION));
			$fName = str_replace(" ", "_", pathinfo($fName, PATHINFO_FILENAME));
			$href = $path . $hredocus;

			switch (fileExists($href))
			{
				case 0:
					break;
				case 1:
					break;
				case 2:
					$href = mb_convert_encoding($href, "WINDOWS-1251", "UTF-8");
					break;
			}

			@mkdir($path . "/bitrix/tmp/trustedsign");
			$random = randomString(10); // ?????????? ??????
			$randomPath = $path . "/bitrix/tmp/trustedsign/" . $random; //????? ????
			@mkdir($randomPath);   // ?????? ????? ? ????, ????????? ????
			switch (verify($href, $extFile, $fName, $randomPath))
			{
				case 0:
					echo 0;
					break;
				case 1:
					echo "/bitrix/tmp/trustedsign/" . $random . "/" . $fName . "." . $extFile;
					break;
				default:
					echo 2;
					break;
			}
		} else
		{ // ???? ?? ?? ? ?? ??????, ?? ?????????? ?????
			echo 2;
		}
	}
} elseif ($module == "disk")
{
	if ($id > 0)
	{
		try
		{
			$fileModel = File::loadById($id);
			$fileArray = $fileModel->getFile();
			$lastVersId = $fileModel->getFileId();
			$file = CFile::GetFileArray($lastVersId);
			if ($ext == 'sign')
			{ // ???? ???????? ?? ???????????, ??????? ?????? ?? ????
				echo $file[SRC];
			} elseif ($ext == 'cosign')
			{
				echo $file[SRC];
			} elseif ($ext == 'remove')
			{ // ??? ??? ???????????? ?????????. ????????? ???
				$originalName = Bitrix\Disk\BaseObject::loadById($id)->getName();

				$fileName = pathinfo($originalName, PATHINFO_FILENAME);
				$extFile = pathinfo($fileName, PATHINFO_EXTENSION);
				$fName = pathinfo($fileName, PATHINFO_FILENAME);
$fName = str_replace(" ", "_", pathinfo($fName, PATHINFO_FILENAME));
				$href = $path . $file[SRC];

				switch (fileExists($href))
				{
					case 0:
						break;
					case 1:
						break;
					case 2:
						$href = mb_convert_encoding($href, "WINDOWS-1251", "UTF-8");
						break;
				}
				
				@mkdir($path . "/bitrix/tmp/trustedsign");
				$random = randomString(10); // ?????????? ??????
				$randomPath = $path . "/bitrix/tmp/trustedsign/" . $random; //????? ????
				@mkdir($randomPath);   // ?????? ????? ? ????, ????????? ????
				switch (verify($href, $extFile, $fName, $randomPath))
				{
					case 0:
						echo 0;
						break;
					case 1:
						echo "/bitrix/tmp/trustedsign/" . $random . "/" . $fName . "." . $extFile;
						break;
					default:
						echo 2;
						break;
				}

			} else
			{ // ???? ?? ?? ? ?? ??????, ?? ?????????? ?????
				echo 2;
			}
		} catch (Exception $e)
		{
			echo 2;
		}
	}
}
function verify($signedData, $extFile, $fName, $randomPath)
{
	$Openssl_path = mb_convert_encoding((COption::GetOptionString("trusted.sign", "Openssl_path") != null) ? COption::GetOptionString("trusted.sign", "Openssl_path") : "openssl", 'Windows-1251', 'UTF-8');
	$signedFile = $randomPath . "/" . $fName . "." . $extFile . ".der";
	$file = $randomPath . "/" . $fName . "." . $extFile;
	exec("\"$Openssl_path\" enc -a -d -in \"$signedData\" -out \"$signedFile\"");
	$output = exec("\"$Openssl_path\" cms -verify -in \"$signedFile\" -inform DER -out \"$file\" -no_signer_cert_verify -no_attr_verify 2>&1");

	if ($output == "Verification successful")
	{
		$result = 1;
	} elseif (stristr($output, 'signer certificate not found'))
	{
		$result = 0;
	} elseif (stristr($output, ":error:"))
	{
		$result = 2;
	}
	@unlink($signedFile);
	//  ????????? ?????? ????? ? ?????? ????? ? ??????? ??
	$path = $_SERVER["DOCUMENT_ROOT"];
	$dir = $path . "/bitrix/tmp/trustedsign/";
	$scandir = scandir($dir);
	for ($i = 0; $i < count($scandir); $i++)
	{ // ????????? ??? ?????
		if ($scandir[$i] != '.' && $scandir[$i] != '..')
		{ // ???? ?? . ? ?? .. , ?? ????????? ?????
			if ($d = @opendir($dir . $scandir[$i]))
			{ //$dir - ???? ? ??????????
				while (($file = readdir($d)) !== false)
				{ //????????? ??????????
					// ????????? ???????? ???????? ?????
					$ftime = filemtime($dir . $scandir[$i] . "/" . $file); // ??????? ????? ????????
					if (time() - $ftime > 3600)
					{ //3600 - 1 ???. ????? ? ???.
						@unlink($dir . $scandir[$i] . "/" . $file); //???? ?????? ?????????? ??????? ???????
					}
					@rmdir($dir . $scandir[$i]); // ???????? ?????, ???? ??? ??????
				}
				closedir($d);
			}
		}
	}
	return $result;
}

function pathinfo_utf($path)
{

	if (strpos($path, '/') !== false)
		$basename = end(explode('/', $path));
	elseif (strpos($path, '\\') !== false)
		$basename = end(explode('\\', $path));
	else
		return false;

	if (!$basename)
		return false;

	$dirname = substr($path, 0,
		strlen($path) - strlen($basename) - 1);

	if (strpos($basename, '.') !== false)
	{
		$extension = end(explode('.', $path));
		$filename = substr($basename, 0,
			strlen($basename) - strlen($extension) - 1);
	} else
	{
		$extension = '';
		$filename = $basename;
	}

	return array(
		'dirname' => $dirname,
		'basename' => $basename,
		'extension' => $extension,
		'filename' => $filename
	);
}

function fileExists($url)
{
	if (file_exists($url))
	{
		return 1;
	} elseif (file_exists(mb_convert_encoding($url, "WINDOWS-1251", "UTF-8")))
	{
		return 2;
	} else
	{
		return 0;
	}
}

function randomString($length)
{
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++)
	{
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

$id2 = htmlspecialchars(strip_tags($_POST["id2"]));
$clickModule = htmlspecialchars(strip_tags($_POST["clickModule"]));
if ($clickModule == "webdav")
{
	try
	{
// ???????? ???????? ? ?????????? ?????
		if ($id2 > 0)
		{
			$pkcs7 = htmlspecialchars(strip_tags($_POST["pkcs7"]));
			if (!empty($pkcs7))
			{
				$docObject = CIBlockDocument::GetDocument($id2);
				$extfile = strtolower(pathinfo($docObject[NAME], PATHINFO_EXTENSION));
				if ($extfile == "sig")
				{
					$filename = $docObject[NAME];
				} else
				{
					$filename = $docObject[NAME] . ".sig";
				}
				$idBlock = $docObject[IBLOCK_TYPE_ID];
				# ???????? ????????
				$arr_file = Array(
					"name" => $filename,
					"del" => "Y",
					"MODULE_ID" => $idBlock,
					"content" => $pkcs7);
				$fid = CFile::SaveFile($arr_file, "iblock");
				$arriFile[] = $fid;
				# ????????? ???? ????
				$PROP["FILE"] = $arriFile;
				CIBlockElement::SetPropertyValueCode($id2, "FILE", $PROP["FILE"]);
				$el = new CIBlockElement;
				$arLoadProductArray = Array(
					"NAME" => $filename);
				$el->Update($id2, $arLoadProductArray);
				echo 1;
			} else echo 0;
		}
	} catch (Exception $e)
	{
		echo 0;
	}
} elseif ($clickModule == "disk")
{
	try
	{
		$pkcs7 = htmlspecialchars(strip_tags($_POST["pkcs7"]));
		$fileModel = File::loadById($id2);
		$lastVersId = $fileModel->getFileId();
		$file = CFile::GetFileArray($lastVersId);
		$extfile = strtolower(pathinfo($file[FILE_NAME], PATHINFO_EXTENSION));
		if ($extfile == "sig")
		{
			$filename = $file[FILE_NAME];
		} else
		{
			$filename = $file[FILE_NAME] . ".sig";
		}
//  ???????? ????? ????
		$arr_file = Array(
			"name" => $filename,
			"del" => "Y",
			"MODULE_ID" => $file[MODULE_ID],
			"content" => $pkcs7);
		// ????????? ????? ???? ? ????? ?????????
		$fid = CFile::SaveFile($arr_file, "disk");

		// ???????? ????? ????
		$lastFile = CFile::GetFileArray($fid);
//?????? ?????? ???????? ? ????? ??????
//$fileModel->updateContent($lastFile, $USER->GetID());
		$fileModel->addVersion($lastFile, $USER->GetID());
		// ??????????? ? sig, ???? ??? ?? sig
		$nameFile = $fileModel->getName();
		$extfile = strtolower(pathinfo($nameFile, PATHINFO_EXTENSION));
		if ($extfile == "sig")
		{
			$filename = $nameFile;
		} else
		{
			$filename = $nameFile . ".sig";
		}
		$fileModel->rename($filename);
		echo 1;
	} catch (Exception $e)
	{
		echo 0;
	}
}
?>

