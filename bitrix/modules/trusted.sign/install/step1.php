<?
IncludeModuleLangFile(__FILE__);
if (!IsModuleInstalled("trusted.api")) {
    echo CAdminMessage::ShowMessage(array(
        "MESSAGE" => GetMessage("NO_TRAPI"),
        "DETAILS" => GetMessage("NO_TRAPI_DETAIL"),
        "HTML" => true,
        "TYPE" => "ERROR",

    ));
} else {
    echo CAdminMessage::ShowMessage(array(
        "MESSAGE" => GetMessage("TRAPI_OK_INSTALL"),
        "DETAILS" => GetMessage("PUSH_CONTINUE"),
        "HTML" => true,
        "TYPE" => "OK",

    ));
}

?>
<form action="<?= $APPLICATION->GetCurPage() ?>" name="trustedbitrix_form" id="trustedbitrix_form" class="form-webdav"
      method="POST">
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
    <input type="hidden" name="id" value="trusted.sign">
    <input type="hidden" name="install" value="Y">
    <input type="hidden" name="step" value="1">
    <input type="submit" name="wd_next" value="<?= GetMessage("CONTINUE_INSTALL_TRAPI") ?>"/>

</form>