<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("disk");
use Bitrix\Disk\File;
use Bitrix\Disk\Document\FileData;
use Bitrix\Disk\Driver;
use Bitrix\Disk\Object;
use Bitrix\Disk\Folder;

$path = $_SERVER["DOCUMENT_ROOT"];

$id = htmlspecialchars(strip_tags($_POST["id"]));
$module = htmlspecialchars(strip_tags($_POST["module"]));
if (($module == "webdav")||($module == "lists")) {
    try {
// �������� �������� � ���������� �����
        if ($id > 0) {
            $pkcs7 = htmlspecialchars(strip_tags($_POST["pkcs7"]));
            if (!empty($pkcs7)) {
                $docObject = CIBlockDocument::GetDocument($id);
                $extfile = pathinfo($docObject[NAME], PATHINFO_EXTENSION);
                if ($extfile == "sig") {
                    $filename = $docObject[NAME];
                } else {
                    $filename = $docObject[NAME] . ".sig";
                }
                $idBlock = $docObject[IBLOCK_TYPE_ID];
                # �������� ��������
                $arr_file = Array(
                    "name" => $filename,
                    "del" => "Y",
                    "MODULE_ID" => $idBlock,
                    "content" => $pkcs7);
                $fid = CFile::SaveFile($arr_file, "iblock");
                $arriFile[] = $fid;
                # ��������� ���� ����
                $PROP["FILE"] = $arriFile;
                CIBlockElement::SetPropertyValueCode($id, "FILE", $PROP["FILE"]);
                $el = new CIBlockElement;
                $arLoadProductArray = Array(
                    "NAME" => $filename);
                $el->Update($id, $arLoadProductArray);
                echo 1;
            } else echo 0;
        }
    } catch (Exception $e) {
        echo 0;
    }
} elseif ($module == "disk") {
    try {
        $pkcs7 = htmlspecialchars(strip_tags($_POST["pkcs7"]));
        $fileModel = File::loadById($id);
//    $fileArray = $fileModel->getFile();
//    $fileModel->addVersion($fileArray, $USER->GetID());
        $lastVersId = $fileModel->getFileId();
        $file = CFile::GetFileArray($lastVersId);
        $extfile = strtolower(pathinfo($file[FILE_NAME], PATHINFO_EXTENSION));
        if ($extfile == "sig") {
            $filename = $file[FILE_NAME];
        } else {
            $filename = $file[FILE_NAME] . ".sig";
        }
//  �������� ����� ����
        $arr_file = Array(
            "name" => $filename,
            "del" => "Y",
            "MODULE_ID" => $file[MODULE_ID],
            "content" => $pkcs7);
        // ��������� ����� ���� � ����� ���������
        $fid = CFile::SaveFile($arr_file, "disk");

        // �������� ����� ����
        $lastFile = CFile::GetFileArray($fid);
//������ ������ �������� � ����� ������
//$fileModel->updateContent($lastFile, $USER->GetID());
        $fileModel->addVersion($lastFile, $USER->GetID());
        // ����������� � sig, ���� ��� �� sig
        $nameFile = $fileModel->getName();
        $extfile = strtolower(pathinfo($nameFile, PATHINFO_EXTENSION));
        if ($extfile == "sig") {
            $filename = $nameFile;
        } else {
            $filename = $nameFile . ".sig";
        }
        $fileModel->rename($filename);
        echo 1;
    } catch (Exception $e) {
        echo 0;
    }
} else {
    echo 0;
}

?>

