<?
$MESS ['SIGN_DESCR_DESCR'] = "Подпись документа";
$MESS ['SIGN_DESCR_NAME'] = "Подпись документа";
$MESS ['SIGN_DESCR_VC'] = "Сколько проголосовало";
$MESS ['SIGN_DESCR_TC'] = "Сколько должно проголосовать";
$MESS ['SIGN_DESCR_VP'] = "Процент проголосовавших";
$MESS ['SIGN_DESCR_AP'] = "Процент утвердивших";
$MESS ['SIGN_DESCR_NAP'] = "Процент отклонивших";
$MESS ['SIGN_DESCR_AC'] = "Количество утвердивших";
$MESS ['SIGN_DESCR_NAC'] = "Количество отклонивших";
$MESS ['SIGN_DESCR_LA'] = "Последний голосовавший";
$MESS ['SIGN_DESCR_TA1'] = "Автоматическое отклонение";
$MESS ['SIGN_DESCR_CM'] = "Комментарий";
$MESS ['SIGN_DESCR_APPROVERS'] = "Утвердили пользователи";
$MESS ['SIGN_DESCR_REJECTERS'] = "Отклонили пользователи";
?>