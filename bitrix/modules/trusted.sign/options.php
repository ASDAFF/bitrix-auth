<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
$module_id = "trusted.sign";
if (!CModule::IncludeModule($module_id)) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
    CAdminMessage::ShowMessage("Module don`t loaded.");
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
}

IncludeModuleLangFile(__FILE__);

$arAllOptions = Array();
$arAllOptions1 = Array();
$arAllOptions2 = Array();
$arAllOptions3 = Array();
if (!IsModuleInstalled("trusted.api")) {
    echo CAdminMessage::ShowMessage(array(
        "MESSAGE" => GetMessage("NO_TRAPI"),
        "DETAILS" => GetMessage("NO_TRAPI_DETAIL"),
        "HTML" => true,
        "TYPE" => "ERROR",

    ));
}

function getOptions()
{
    global $arAllOptions, $arAllOptions1, $arAllOptions2, $arAllOptions3;
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . "trusted.sign" . "/default_option.php");
    $def = $trusted_sign_default_option;

    $arAllOptions1 = Array(

        array("Activate_sign_webdav",
            GetMessage("Activate_sign"),
            0,
            array("selectbox", $trusted_sign_default_sign)),
        array("Activate_cosign_webdav",
            GetMessage("Activate_cosign"),
            0,
            array("selectbox", $trusted_sign_default_sign)),
        array("Activate_remove_webdav",
            GetMessage("Activate_remove"),
            0,
            array("selectbox", $trusted_sign_default_sign)),

    );
    $arAllOptions2 = Array(
        array("Activate_sign_disk",
            GetMessage("Activate_sign"),
            $def["Activate_sign"],
            array("checkbox", false)),
        array("Activate_cosign_disk",
            GetMessage("Activate_cosign"),
            $def["Activate_cosign"],
            array("checkbox", false)),
        array("Activate_remove_disk",
            GetMessage("Activate_remove"),
            $def["Activate_remove"],
            array("checkbox", false)),
    );
    $arAllOptions3 = Array(
        array("Openssl_path",
            GetMessage("Openssl_path"),
            $def["Openssl_path"],
            array("text", 50)),
    );
    $arAllOptions = array_merge($arAllOptions1, $arAllOptions2, $arAllOptions3);

}


?>

<? //������ ��������: ����������� ����������, �������� ������, �������� ���� �������, ������������� ������
// ������� ����� ������� �������� ������������ �� ������
$TRUSTED_RIGHT = $APPLICATION->GetGroupRight($module_id);
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($TRUSTED_RIGHT == "D") $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
?>

<? if ($TRUSTED_RIGHT >= "R"): ?>
    <?
    getOptions();
    $aTabs = array(
        array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "ICON" => $module_id . "_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
        array("DIV" => "edit2", "TAB" => GetMessage("MAIN_TAB_RIGHTS"), "ICON" => $module_id . "_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS")),
    );

    $tabControl = new CAdminTabControl("tabControl", $aTabs);

    CModule::IncludeModule($module_id);

    if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && $TRUSTED_RIGHT >= "W" && check_bitrix_sessid()) {
        if (strlen($RestoreDefaults) > 0) {
            foreach ($arAllOptions as $option) {
                COption::RemoveOption($module_id, $option[0]);
                $name = $option[0];
            }

            foreach ($arProxyOptions as $option) {
                COption::RemoveOption($module_id, $option[0]);
            }

            $z = CGroup::GetList($v1 = 'id', $v2 = 'asc', array('ACTIVE' => 'Y', 'ADMIN' => 'N'));
            while ($zr = $z->Fetch())
                $APPLICATION->DelGroupRight($module_id, array($zr['ID']));
        } else {
            if ($tabControl_active_tab == 'edit1') {
            }
            foreach ($arAllOptions as $arOption) {
                $name = $arOption[0];
                $val = $_REQUEST[$name];
                if ($arOption[3][0] == "multiselectbox")
                    $val = @implode(",", $val);
                if ($arOption[3][0] == "checkbox" && $val != "Y")
                    $val = "N";
                COption::SetOptionString($module_id, $name, $val);

            }

        }
        ob_start();
        $Update = $Update . $Apply;
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");
        ob_end_clean();

        if (strlen($_REQUEST["back_url_settings"]) > 0) {
            if ((strlen($Apply) > 0) || (strlen($RestoreDefaults) > 0))
                LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
            else
                LocalRedirect($_REQUEST["back_url_settings"]);
        } else {
            LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&" . $tabControl->ActiveTabParam());
        }
    }

    ?>
    <?
    $tabControl->Begin();
    ?>

    <form method="POST"
          action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialchars($mid) ?>&amp;lang=<?= LANGUAGE_ID ?>"><?
        $tabControl->BeginNextTab();
        ?>
        <tr class="heading">
            <td colspan="2"><b><? echo GetMessage("trustedsign_options_sys") ?></b></td>
        </tr><?
        __AdmSettingsDrawList($module_id, $arAllOptions1);
        ?>
        <tr class="heading">
            <td colspan="2"><b><? echo GetMessage("trustedsign_options_disk") ?></b></td>
        </tr><?
        __AdmSettingsDrawList($module_id, $arAllOptions2);
        ?>
        <tr class="heading">
            <td colspan="2"><b><? echo GetMessage("trustedsign_options_openssl") ?></b></td>
        </tr><?
        __AdmSettingsDrawList($module_id, $arAllOptions3);
        $tabControl->BeginNextTab(); ?>
        <? //require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights2.php");?>
        <? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php"); ?>
        <? $tabControl->Buttons(); ?>
        <input <? if ($TRUSTED_RIGHT < "W") echo "disabled" ?> type="submit" name="Update"
                                                               value="<?= GetMessage("MAIN_SAVE") ?>"
                                                               title="<?= GetMessage("MAIN_OPT_SAVE_TITLE") ?>">
        <input <? if ($TRUSTED_RIGHT < "W") echo "disabled" ?> type="submit" name="Apply"
                                                               value="<?= GetMessage("MAIN_OPT_APPLY") ?>"
                                                               title="<?= GetMessage("MAIN_OPT_APPLY_TITLE") ?>">
        <? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
            <input <? if ($TRUSTED_RIGHT < "W") echo "disabled" ?> type="button" name="Cancel"
                                                                   value="<?= GetMessage("MAIN_OPT_CANCEL") ?>"
                                                                   title="<?= GetMessage("MAIN_OPT_CANCEL_TITLE") ?>"
                                                                   onclick="window.location='<? echo htmlspecialchars(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
            <input type="hidden" name="back_url_settings"
                   value="<?= htmlspecialchars($_REQUEST["back_url_settings"]) ?>">
        <? endif ?>
        <input <? if ($TRUSTED_RIGHT < "W") echo "disabled" ?> type="submit" name="RestoreDefaults"
                                                               title="<? echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
                                                               OnClick="return confirm('<? echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
                                                               value="<? echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
        <?= bitrix_sessid_post(); ?>
        <? $tabControl->End(); ?>
    </form>
<? endif;
echo BeginNote();
echo GetMessage("Activate_remove_info");
echo "<br>";
echo GetMessage("Openssl_path_info");
echo EndNote(); ?>
<? //require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>


