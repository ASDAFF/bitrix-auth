<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
global $USER;
if(!$USER->IsAuthorized()){
        LocalRedirect('/auth/');
}

?>
<html>
        <head>
                <?$APPLICATION->ShowHead();?>
                <title><?$APPLICATION->ShowTitle()?></title>
                <?require($_SERVER["DOCUMENT_ROOT"].'/include/head.php');?>
        </head>
        <body>
                <?

                
if(!empty($_POST)) {	
	//записываем новые значения для фильтра в пользов. поле текущего пользователя
	$separated;
	foreach ($_POST as $ky=>$vl) {
		$separated .= $ky.'='.$vl.',';
	}
	$user2 = new CUser;
	$fields2 = Array(
	  "UF_FILTERPOST" => $separated
	  );
	$user2->Update($USER->GetID(), $fields2);
	$strError .= $user2->LAST_ERROR;
	if($strError) {
		AddMessage2Log("header.php (UF_FILTERPOST): ".print_r($strError, true));
	}
}

//берем значения для фильтра из пользов. поля текущего пользователя 
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
$FilterArray = $arUser['UF_FILTERPOST'];

//создаем массив
$FilterArrayPost = array();
$FilterArrayPieces = explode(",", $FilterArray);
foreach($FilterArrayPieces as $val) {
    if(!empty($val)) {
	$FilterArrayPieces2 = explode("=", $val);
	$FilterArrayPost[$FilterArrayPieces2[0]] = $FilterArrayPieces2[1];
    }
}
//echo "<pre>"; print_r($FilterArrayPost); echo "</pre>";

foreach($FilterArrayPost as $k=>$item){
        $_SESSION[$k] = $item;
}

                ?>
                <?$APPLICATION->ShowPanel()?>    
                <div class="wrapper_header">
                        <form name="filter_head" method="POST">
                                        <div class="new-select-style-wpandyou">
                                                <select name="STATUS" style="padding-right:28px">
                                                        <option value=''>Все статусы</option>
                                                        <option value="15"<?if($_SESSION['STATUS'] == '15'){?> selected<?}?>>Черновик</option>
                                                        <option value="16"<?if($_SESSION['STATUS'] == '16'){?> selected<?}?>>Ожидает подписи</option>
														<option value="159"<?if($_SESSION['STATUS'] == '159'){?> selected<?}?>>Передано в банк</option>
                                                        <!-- <option value="33"<?if($_SESSION['STATUS'] == '33'){?> selected<?}?>>Предварительное одобрение</option> -->
                                                        <option value="18"<?if($_SESSION['STATUS'] == '18'){?> selected<?}?>>Ожидается оплата</option>
                                                        <option value="19"<?if($_SESSION['STATUS'] == '19'){?> selected<?}?>>БГ отправлена</option>
                                                        <option value="20"<?if($_SESSION['STATUS'] == '20'){?> selected<?}?>>Отказанные</option>
                                                       <!-- <option value="21"<?if($_SESSION['STATUS'] == '21'){?> selected<?}?>>Все активные</option>
                                                        <option value="22"<?if($_SESSION['STATUS'] == '22'){?> selected<?}?>>Все пассивные</option>-->
                                                </select>
                                        </div>
                                        <div class="new-select-style-wpandyou_second">
                                                <select name="COUNT">
                                                        <option value="10"<?if($_SESSION['COUNT'] == '10'){?> selected<?}?>>Выводить по 10</option>
                                                        <option value="25"<?if($_SESSION['COUNT'] == '25'){?> selected<?}?>>Выводить по 25</option>
                                                        <option value="50"<?if($_SESSION['COUNT'] == '50'){?> selected<?}?>>Выводить по 50</option>
                                                        <option value="all"<?if($_SESSION['COUNT'] == 'all'){?> selected<?}?>>Вывести Все</option>
                                                </select>
                                        </div>

                                        <input type="text" name='ID_ELEMENT' placeholder='код заявки' value='<?=$_SESSION['ID_ELEMENT']?>'>
                                        <input type="text" name='DATA_FROM' placeholder='дата заявки с' value='<?=$_SESSION['DATA_FROM']?>' class="datepicker" autocomplete="false">
                                        <input type="text" name='DATA_TO' placeholder='дата заявки по' value='<?=$_SESSION['DATA_TO']?>' class="datepicker" autocomplete="false">
                                        <input type="text" name='INN' placeholder='Клиент' value='<?=$_SESSION['INN']?>'>

                        </form>
                        <button>обновить</button>
                        <div class="horizont_line"></div> 
                        
                </div>
                
                <?require($_SERVER["DOCUMENT_ROOT"].'/include/header.php');?>



