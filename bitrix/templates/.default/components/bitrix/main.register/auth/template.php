<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>

<?if($USER->IsAuthorized()):?>
<?LocalRedirect('/personal/order/');?>
<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>

<?else:?>

<?if(!empty($arParams["~AUTH_RESULT"])){?>
        <?if($arParams['AUTH_RESULT']['TYPE'] == 'OK'){?>
                <div class='message-type-ok'>
        <?}else{?>
                <div class='message-type-error'>
        <?}?>
                        <?ShowMessage($arParams["~AUTH_RESULT"]);?>
        </div>
<?}?>

                        
<?
//preprint($arResult["ERRORS"]);
if (count($arResult["ERRORS"]) > 0):
	foreach ($arResult["ERRORS"] as $key => $error)
                
		if (intval($key) == 0 && $key !== 0) 
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
                echo '<div class="message-type-error">';
	ShowError(implode("<br />", $arResult["ERRORS"]));
        echo '</div>';

elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
?>
<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<?endif?>


<h1>�����������</h1>

<?if(!empty($arParams["~AUTH_RESULT"])){?>
        <?if($arParams['AUTH_RESULT']['TYPE'] == 'OK'){?>
                <div class='message-type-ok'>
        <?}else{?>
                <div class='message-type-error'>
        <?}?>
                        <?ShowMessage($arParams["~AUTH_RESULT"]);?>
        </div>
<?}?>

                        <h2>����� ��� ������������</h2>
<form class="registration" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
<?
if($arResult["BACKURL"] <> ''):
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <input type="hidden" name="REGISTER[LOGIN]"> 
<?
endif;
?>                        
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
	array(
		"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
		"CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
		"AUTH_URL" => $arResult["AUTH_URL"],
		"POST" => $arResult["POST"],
		"SHOW_TITLES" => $arResult["FOR_INTRANET"]?'N':'Y',
		"FOR_SPLIT" => $arResult["FOR_INTRANET"]?'Y':'N',
		"AUTH_LINE" => $arResult["FOR_INTRANET"]?'N':'Y',
	),
	$component,
	array("HIDE_ICONS"=>"Y")
);
?>

    <!--registration__wrap-->
        <div class="registration__wrap">

                <fieldset class="input__layout">
                        <label for="name">���*:</label>
                        <input class="required" type="text" name="REGISTER[NAME]" id="name" value="<?=$arResult["VALUES"]['NAME']?>"> 

                        <label for="last_name">�������*:</label>
                        <input class="required" type="text" name='REGISTER[LAST_NAME]' id="last_name" value="<?=$arResult["VALUES"]['LAST_NAME']?>">

                        <label for="paternal">��������:</label>
                        <input type="text" id="paternal" name='REGISTER[SECOND_NAME]' value="<?=$arResult["VALUES"]['SECOND_NAME']?>">
                </fieldset>

                <fieldset class="input__layout">
                        <label for="date">���� ��������:</label>
                        <input type="text" name="REGISTER[PERSONAL_BIRTHDAY]" class="datepicker" id="date" value="<?=$arResult["VALUES"]['PERSONAL_BIRTHDAY']?>">
                </fieldset>
                        <?
                        $date_need = time()-(2049840000+6*315360000);
                        $date_need_format = date('Y, m, d',$date_need);
                        $date_need_max_format = date('Y, m, d',time());
                        ?>
                        <script>
                                $(document).ready(function() {
                                        $("input[name='REGISTER[PERSONAL_BIRTHDAY]']").datepicker("option", "minDate", new Date(<?=$date_need_format?>));
                                        $("input[name='REGISTER[PERSONAL_BIRTHDAY]']").datepicker("option", "maxDate", new Date(<?=$date_need_max_format?>));
                                });
                        </script>
                

                <fieldset class="input__layout">
                        <label for="sex">���:</label>
                        <select name="REGISTER[PERSONAL_GENDER]" id="sex" style="opacity: 0;">
                                <option value="��������">��������</option>
                                <option value="M" <?if($arResult["VALUES"]['PERSONAL_GENDER']=='M'){?> checked<?}?>>�������</option>
                                <option value="F" <?if($arResult["VALUES"]['PERSONAL_GENDER']=='F'){?> checked<?}?>>�������</option>
                        </select>
                </fieldset>

                <fieldset class="input__layout">
                        <label for="phone">��������� �������*:</label>
                        <input class="required mobile_mask" type="text" name="REGISTER[PERSONAL_MOBILE]" id="phone" value="<?=$arResult["VALUES"]['PERSONAL_MOBILE']?>" placeholder="+7(___) ___ __ __">
                </fieldset>

                <fieldset class="input__layout">
                        <label for="email">E-mail*:</label>
                        <input class="required" type="email" name="REGISTER[EMAIL]" id="email" value="<?=$arResult["VALUES"]['EMAIL']?>">
                </fieldset>

                <fieldset class="input__layout">
                        <label for="password">������*:</label>
                        <input class="required" type="password" name="REGISTER[PASSWORD]" id="password">
                </fieldset>

                <fieldset class="input__layout">
                        <label for="password_confirmation">������������� ������*:</label>
                        <input class="required" type="password" name="REGISTER[CONFIRM_PASSWORD]" id="password_confirmation">
                </fieldset>
                <?
                CModule::IncludeModule("subscribe");
                $arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC"); 
                $arFilter = Array("ACTIVE"=>"Y", "LID"=>LANG); 
                $rsRubric = CRubric::GetList($arOrder, $arFilter); 
                $arRubrics = array(); 
                while($arRubric = $rsRubric->GetNext()){ 
                        $Rubric[] = $arRubric; 
                }
                ?>
                <div class="subscribe">
                        <input type="checkbox" id="subscribe" name="subscribe">
                        <label for="subscribe"><?=$Rubric[0]['NAME']?></label>
                </div>

                <div class="kapcha">
<?/* CAPTCHA */	?>
                        <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" alt="CAPTCHA" />
                                
                        <input type="text" name="captcha_word" maxlength="50" value="" class="required">
                                
                
<?/* !CAPTCHA */ ?>                    
                        
                        
                </div>

                <input type="submit" name="register_submit_button" class="submit-nice" value="������������������">

                <span>*����������� ��� ����������</span>

        </div>
    <!--/registration__wrap-->

</form>          

<?endif?>
