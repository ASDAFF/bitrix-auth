<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if(count($arResult["ITEMS"])>0){?>
        <div class="wrapper_tow_head">
                
                <table class='width_100'>
                        <tr>
                                <th>Код заявки</th>
                                <th>ИНН принципала</th>
                                <th>Дата заявки</th>
                                <th>Статус заявки</th>
                                <th>Изменение статуса</th>
                                <th>Актуально до</th>
                                <th>Размер обеспечения, руб</th>
                                <th>Срок БГ, дней</th>
                        </tr>
                        
                        <?foreach($arResult["ITEMS"] as $arItem):?>
                                <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                ?>
                                <?$arItem['DAY'] = round((strtotime($arItem['PROPERTIES']['D2']['VALUE'])-strtotime($arItem['PROPERTIES']['D1']['VALUE']))/(3600*24));?>
                                <tr>
                                        <td><a href='<?=$arItem['DETAIL_PAGE_URL']?>'><?=$arItem['NAME']?></a></td>
                                        <td><?=$arItem['PROPERTIES']['M']['VALUE']?></td>
                                        <td><?=$arItem['CODE']?></td>
                                        <td><?=$arItem['PROPERTIES']['STATUS']['VALUE']?></td>
                                        <td><?=$arItem['PROPERTIES']['DATE_STATUS']['VALUE']?></td>
                                        <td><?=$arItem['PROPERTIES']['C']['VALUE']?></td>
                                        <td><?=num($arItem['PROPERTIES']['B']['VALUE'])?></td>
                                        <td><?=$arItem['DAY']?></td>
                                </tr>
                        <?endforeach;?>
                </table>
                <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                        <br /><?=$arResult["NAV_STRING"]?>
                <?endif;?>
        </div>
<?}?>

