<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>


					
<!--						<li><a class="pag-first" href="#"></a></li>
						<li><a class="pag-num act" href="#">1</a></li>
						<li><a class="pag-num" href="#">2</a></li>
						<li><a class="pag-num" href="#">3</a></li>
						<li><a class="pag-num" href="#">4</a></li>
						<li><a class="pag-num" href="#">5</a></li>
						<li><a class="pag-last" href="#"></a></li>
						<li><a class="pag-pass" href="#">�������</a></li>
						<li><input type="text" value="15"></li>
					
				</div>-->
<form action="" method="GET">
<div class="paginator">
        <ul>
                <?if($arResult["bDescPageNumbering"] === true):?>
                        <?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
                                <?if($arResult["bSavePage"]):?>
                                        <li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=GetMessage("nav_begin")?></a></li>
                                        |
                                        <li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_prev")?></a></li>
                                        |
                                <?else:?>
                                        <?if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
                                                <li><a class="pag-first" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a></li>                                        
                                        <?else:?>
                                                <li><a class="pag-first" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"></a></li>                                                                                        
                                                
                                                
                                        <?endif?>
                                <?endif?>
                        <?else:?>

                        <?endif?>

                        <?while($arResult["nStartPage"] >= $arResult["nEndPage"]):?>
                                <?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

                                <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                                        <li><a class="pag-num act"><?=$NavRecordGroupPrint?></a></li>
                                <?elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
                                        <li><a class="pag-num" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$NavRecordGroupPrint?></a></li>
                                <?else:?>
                                        <li><a class="pag-num" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$NavRecordGroupPrint?></a></li>
                                <?endif?>

                                <?$arResult["nStartPage"]--?>
                        <?endwhile?>

                        

                        <?if ($arResult["NavPageNomer"] > 1):?>
                                <li><a class="pag-last" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a></li>
                        <?endif?>
                        <?if($arResult['NavPageCount']>=5){?>
                                
                                <li><a class="pag-pass blog" href="#">�������</a></li>
                                <li><input type="text" name="PAGEN_<?=$arResult["NavNum"]?>" rel="<?=$arResult["NavPageCount"]?>" value="<?=$arResult['NavPageCount']?>"></li>
                                
                        <?}?>
                                

                <?else:?>
                        <?if ($arResult["NavPageNomer"] > 1):?>

                                <?if($arResult["bSavePage"]):?>
                                        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><?=GetMessage("nav_begin")?></a>
                                        |
                                        <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_prev")?></a>
                                        |
                                <?else:?>
                                        <?if ($arResult["NavPageNomer"] > 2):?>
                                                <li><a class="pag-first" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a></li>
                                        <?else:?>
                                                <li><a class="pag-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"></a></li>
                                        <?endif?>
                                <?endif?>

                        <?endif?>

                        <?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

                                <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                                        <li><a class="pag-num act"><?=$arResult["nStartPage"]?></a></li>
                                <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
                                        <li><a class="pag-num" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a></li>
                                <?else:?>
                                        <li><a class="pag-num" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a></li>
                                <?endif?>
                                        
                                <?$arResult["nStartPage"]++?>
                        <?endwhile?>
                        

                        <?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
                                <li><a class="pag-last" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"></a></li>
<!--                                <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=GetMessage("nav_end")?></a>-->
                        <?else:?>
<!--                                <li><a class="pag-last"></a></li>-->

                        <?endif?>
                        <?if($arResult['NavPageCount']>=5){?>
                                
                                <li><a class="pag-pass" href="#">�������</a></li>
                                <li><input type="text" name="PAGEN_<?=$arResult["NavNum"]?>" value="<?=$arResult['NavPageCount']?>"></li>
                                
                        <?}?>
                <?endif?>



                <?if ($arResult["bShowAll"]):?>
                <noindex>
                        <?if ($arResult["NavShowAll"]):?>
                                |&nbsp;<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0" rel="nofollow"><?=GetMessage("nav_paged")?></a>
                        <?else:?>
                                |&nbsp;<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1" rel="nofollow"><?=GetMessage("nav_all")?></a>
                        <?endif?>
                </noindex>
                <?endif?>
        </ul>
</div>
        </form>