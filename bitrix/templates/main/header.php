<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
if(!$USER->IsAuthorized() && ($APPLICATION->GetCurDir() != "/zayavki/signature/")){
        LocalRedirect('/auth/');
}

?>
<html>
        <head>
                <?$APPLICATION->ShowHead();?>
                <title><?$APPLICATION->ShowTitle()?></title>
                <?require($_SERVER["DOCUMENT_ROOT"].'/include/head.php');?>
				<style>
.hide-layout {
  background: #000; /* фон */
  bottom: 0;  /* координата снизу */
  filter: progid:DXImageTransform.Microsoft.Alpha(opacity=50); /* прозрачность для IE */
  height: 100%; /* высота на всю страницу */
  opacity: 0.5;  /* прозрачность слоя */
  position: fixed; /* фиксируем элемент на странице*/
  top: 0; /* координата сверху */
  width: 100%; /* ширина на всю страницу */
  z-index: 998; /* z-index для перекрытия остальных элементов */
}

.hide-layout-finish {
  background: #000; /* фон */
  bottom: 0;  /* координата снизу */
  filter: progid:DXImageTransform.Microsoft.Alpha(opacity=50); /* прозрачность для IE */
  height: 100%; /* высота на всю страницу */
  opacity: 0.5;  /* прозрачность слоя */
  position: fixed; /* фиксируем элемент на странице*/
  top: 0; /* координата сверху */
  width: 100%; /* ширина на всю страницу */
  z-index: 998; /* z-index для перекрытия остальных элементов */
}

#popup {
  width: 80%;
  height: 80%;
  top: 300px;
  padding: 10px;
  position: fixed;
  z-index:10000;
  background: white;
  overflow: scroll;
}
#popup2 {
  width: 80%;
  height: 80%;
  top: 300px;
  padding: 10px;
  position: fixed;
  z-index:10000;
  background: white;
  overflow: scroll;
}
#popup_finish {
  width: 350px;
  height: 170px;
  top: 300px;
  padding: 10px;
  position: fixed;
  z-index:10000;
  background: white;
}


</style>
<script>
					$(function() {
	  $('#popup').hide();// скрыли фон и всплывающее окно
	  $('#hide-layout').hide();// скрыли фон и всплывающее окно
	  $('#hide-layout').css({opacity: .5}); // кроссбраузерная прозрачность
	  alignCenter($('#popup')); // центрировали окно
	  $(window).resize(function() {
		alignCenter($('#popup')); // центрирование при ресайзе окна
	  })
	  $('.click-me').click(function() {
		$('#hide-layout, #popup').fadeIn(300); // плавно открываем
	  })
	  $('.btn-close, #hide-layout').click(function() {
		$('#hide-layout, #popup').fadeOut(300); // плавно скрываем
	  })
	  $('#btn-yes, #btn-no').click(function() {
		alert('Выполнили какое-то действие, затем скрываем окно...'); // сделали что-то...
		$('#hide-layout, #popup').fadeOut(300); // скрыли
	  })
	  // функция центрирования
	  function alignCenter(elem) {
		elem.css({
		  left: ($(window).width() - elem.width()) / 2 + 'px', // получаем координату центра по ширине
		  top: ($(window).height() - elem.height()) / 3 + 'px' // получаем координату центра по высоте
		})
	  }
	})
	
	
	
	$(function() {
	  $('#hide-layout-finish').css({opacity: .5}); // кроссбраузерная прозрачность
	  alignCenter($('#popup_finish')); // центрировали окно
	  $(window).resize(function() {
		alignCenter($('#popup_finish')); // центрирование при ресайзе окна
	  })
	  $('.click-me').click(function() {
		$('#hide-layout-finish, #popup_finish').fadeIn(300); // плавно открываем
	  })
	  $('.btn-close, #hide-layout-finish').click(function() {
		$('#hide-layout-finish, #popup_finish').fadeOut(300); // плавно скрываем
	  })
	  $('#btn-yes, #btn-no').click(function() {
		alert('Выполнили какое-то действие, затем скрываем окно...'); // сделали что-то...
		$('#hide-layout-finish, #popup_finish').fadeOut(300); // скрыли
	  })
	  // функция центрирования
	  function alignCenter(elem) {
		elem.css({
		  left: ($(window).width() - elem.width()) / 2 + 'px', // получаем координату центра по ширине
		  top: ($(window).height() - elem.height()) / 3 + 'px' // получаем координату центра по высоте
		})
	  }
	})
	
	$(function() {
	  $('#popup2').hide();// скрыли фон и всплывающее окно
	  $('#hide-layout').hide();// скрыли фон и всплывающее окно
	  $('#hide-layout').css({opacity: .5}); // кроссбраузерная прозрачность
	  alignCenter($('#popup2')); // центрировали окно
	  $(window).resize(function() {
		alignCenter($('#popup2')); // центрирование при ресайзе окна
	  })
	  $('.click-me').click(function() {
		$('#hide-layout, #popup2').fadeIn(300); // плавно открываем
	  })
	  $('.btn-close, #hide-layout').click(function() {
		$('#hide-layout, #popup2').fadeOut(300); // плавно скрываем
	  })
	  $('#btn-yes, #btn-no').click(function() {
		alert('Выполнили какое-то действие, затем скрываем окно...'); // сделали что-то...
		$('#hide-layout, #popup2').fadeOut(300); // скрыли
	  })
	  // функция центрирования
	  function alignCenter(elem) {
		elem.css({
		  left: ($(window).width() - elem.width()) / 2 + 'px', // получаем координату центра по ширине
		  top: ($(window).height() - elem.height()) / 15 + 'px' // получаем координату центра по высоте
		})
	  }
	})
</script>
        </head>
        <body>
                <?$APPLICATION->ShowPanel()?>
                <?require($_SERVER["DOCUMENT_ROOT"].'/include/header.php');?>



