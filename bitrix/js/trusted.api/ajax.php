<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
function fileExists($url)
{
	if (file_exists($url))
	{
		return 1;
	} elseif (file_exists(mb_convert_encoding($url, "WINDOWS-1251", "UTF-8")))
	{
		return 2;
	} else
	{
		return 0;
	}
}

$sign = ($_POST["sign"]);
$sign = str_replace("\r", "", $sign);
$sign = str_replace("\n", "", $sign);
if (!empty($sign))
{
	$slide = ($_POST["slice"]);
	$path = $_SERVER["DOCUMENT_ROOT"] . $sign;
	switch (fileExists($path))
	{
		case 0:
			break;
		case 1:
			break;
		case 2:
			$path = mb_convert_encoding($path, "WINDOWS-1251", "UTF-8");
			break;
	}
	$handle = fopen($path, "rb");
	fseek($handle, 1048576 * $slide);
	$contents = fread($handle, 1048576);
	fclose($handle);
	echo base64_encode($contents);
}

$cosign = ($_POST["cosign"]);
$cosign = str_replace("\r", "", $cosign);
$cosign = str_replace("\n", "", $cosign);
if (!empty($cosign))
{
	$slideCo = ($_POST["sliceCo"]);
	$path = $_SERVER["DOCUMENT_ROOT"] . $cosign;
	switch (fileExists($path))
	{
		case 0:
			break;
		case 1:
			break;
		case 2:
			$path = mb_convert_encoding($path, "WINDOWS-1251", "UTF-8");
			break;
	}
	$handle = fopen($path, "rb");
	fseek($handle, 1048576 * $slideCo);
	$contents = fread($handle, 1048576);
	fclose($handle);
	echo $contents;
}
$fileSize = ($_POST["uri"]);
//if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//	$fileSize= mb_convert_encoding($fileSize, "WINDOWS-1251", "UTF-8");
//}
$fileSize = str_replace("\r", "", $fileSize);
$fileSize = str_replace("\n", "", $fileSize);
if (!empty($fileSize))
{
	$path = $_SERVER["DOCUMENT_ROOT"] . $fileSize;
	switch (fileExists($path))
	{
		case 0:
			echo "No File";
			break;
		case 1:
			echo filesize($path);
			break;
		case 2:
			echo filesize(mb_convert_encoding($path, "WINDOWS-1251", "UTF-8"));
			break;
	}
}

?>