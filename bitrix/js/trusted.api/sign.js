/**
 * Trusted API
 * ��� "�������� ����������"
 *            ################################################
 *            #  _______      ___    _______   ____________  #
 *            # |    _   \   |   |  |   ____\ |____    ____| #
 *            # |   | |   \  |   |  |  |   ___     |  |      #
 *            # |   | |    | |   |  |  |  |_  |    |  |      #
 *            # |   |_|   /  |   |  |  |___|  |    |  |      #
 *            # |________/   |___|  |_________/    |__|      #
 *            ################################################
 */

//BEGIN TBSignedData
/**
 *
 * @returns {TBSignedData}
 * @constructor
 */
TBSignedData = function () {
	if (!(this instanceof TBSignedData)) {
		return new TBSignedData();
	}
	if (!$("object").is("#pluginKripto"))
		$("body").prepend('<object style = "width: 0; height:0" id = "pluginKripto" type = "application/x-digtcapicom" > </object >');
};

/**
 *
 * @type {{0: *}} //������ ������ � �� ��������
 */
var err0 = {0: BX.message('NO_ERROR')},
err1     = {1: BX.message('NO_DATA')},
err2     = {2: BX.message('NO_CO_DATA')},
err3     = {3: BX.message('NO_HREF')},
err4     = {4: BX.message('NO_CO_HREF')},
err5     = {5: BX.message('NO_OBJECT')},
err6     = {6: BX.message('NO_DOC_TO_HREF')},
err7     = {7: BX.message('NO_PRIV_KEY')},
err8     = {8: BX.message('DOC_MODIF')},
err9     = {9: BX.message('NO_TRUST')},
err10    = {10: BX.message('NO_SUB')},
err11    = {11: BX.message('NOT_FORMED')},
err12    = {12: BX.message('LIM_LICENSE')},
err13    = {13: BX.message('LICENSE_POLICY')},
err14    = {14: BX.message('ER_ADD_SIGN')},
err15    = {15: BX.message('CANCELED_USER')},
err16    = {16: BX.message('NO_PARAM')},
err17    = {17: BX.message('NO_FILE_TO_HREF')},
err18    = {18: BX.message('NO_KONTEYNER')},
err19    = {19: BX.message('ERROR_ARGUM')},
err20    = {20: BX.message('NEDOPUS_CERT')},
err99    = {99: BX.message('OTHER_ERROR')};

// response
/**
 *
 * @type {Object}
 */
var response = new Object();
response['signature'] = null;
response['error'] = err0;

/**
 * ��������� ������� ������
 * @param data   ������ � ���� ������ ��� �������
 * @param certificate ���������� ���������� ��� �������. �������� �� ��������� false - ���� ������ �����������
 * @param bDetached ���������- true ��� �����������- false �������. �� ��������� false
 * @returns {Object} ���������� ������ response
 * @constructor
 */
TBSignedData.prototype.SignText = function (data, certificate, bDetached) {
	bDetached = bDetached || false;
	certificate = certificate || false;
	response['signature'] = null;
	var sd = CreateObject("CAPICOM.SignedData");
	if (!(sd === "NO_OBJECT")) {
		try {
			var signer = CreateObject("CAPICOM.Signer");
			if (!(signer === "NO_OBJECT")) {
				var cert = GetCert(certificate);
				signer.Certificate = cert;
				if (data) {
					response['error'] = err0;
					sign(true, data, signer, false, bDetached, false, sd);
				} else {
					response['signature'] = null;
					response['error'] = err1;
				}
				return response;
			}
			return response;
		} catch (e) {
			return response;
		}
	}
	return response;
};

/**
 * ��������� ������� ������
 * @param data ������ ��� ���������� ������� � ������� base64
 * @param certificate ���������� ���������� ��� �������. �������� �� ��������� false - ���� ������ �����������
 * @returns {Object} ���������� ������ response
 * @constructor
 */
TBSignedData.prototype.CosignText = function (data, certificate) {
	certificate = certificate || false;
	response['signature'] = null;
	var sd = CreateObject("CAPICOM.SignedData");
	if (!(sd === "NO_OBJECT")) {
		try {
			var signer = CreateObject("CAPICOM.Signer");
			if (!(signer === "NO_OBJECT")) {
				var cert = GetCert(certificate);
				signer.Certificate = cert;
				if (data) {
					response['error'] = err0;
					sign(true, data, signer, true, false, false, sd);
				} else {
					response['signature'] = null;
					response['error'] = err2;
				}
				return response;
			}
			return response;
		} catch (e) {
			return response;
		}
	}
	return response;
};

/**
 * ��������� ������� �����
 * @param url ������ �� ���� ��� �������, ���� bLocal= false, ��� ID �������� input
 * @param certificate ���������� ���������� ��� �������. �������� �� ��������� false - ���� ������ �����������
 * @param bDetached ���������- true ��� �����������- false �������. �� ��������� false
 * @param bLocal ���� �� ���� �� �������-false, ���� ��������� - true
 * @param callback ���������� ������ response ������ ���� bLocal = true
 * @returns {Object} ���������� ������ response ������ ���� bLocal = false
 * @constructor
 */
TBSignedData.prototype.SignFile = function (url, certificate, bDetached, bLocal, callback) {
	bDetached = bDetached || false;
	bLocal = bLocal || false;
	certificate = certificate || false;
	callback = callback || false;
	response['signature'] = null;
	var sd = CreateObject("CAPICOM.SignedData");
	if (!(sd === "NO_OBJECT")) {
		try {
			var signer = CreateObject("CAPICOM.Signer");
			if (!(signer === "NO_OBJECT")) {
				var cert = GetCert(certificate, bLocal, callback);
				signer.Certificate = cert;
				if (bLocal) {
					ReadLocalFile(false, url, signer, false, bDetached, bLocal, sd, callback);
				} else {
					if (url) {
						response['error'] = err0;
						if (url == '') {
							response['error'] = err6;
						} else {
							sign(false, url, signer, false, bDetached, bLocal, sd);
						}
					} else {
						response['signature'] = null;
						response['error'] = err3;
					}
					return response;
				}
			}
			return response;
		} catch (e) {
			return response;
		}
	}
	return response;
};

/**
 * ��������� ������� �����
 * @param url url ������ �� ���� ��� �������, ���� bLocal= false, ��� ID �������� input
 * @param certificate ���������� ���������� ��� �������. �������� �� ��������� false - ���� ������ �����������
 * @param bLocal ���� �� ���� �� �������-false, ���� ��������� - true
 * @param callback ���������� ������ response ������ ���� bLocal = true
 * @returns {Object} ���������� ������ response ������ ���� bLocal = false
 * @constructor
 */
TBSignedData.prototype.CosignFile = function (url, certificate, bLocal, callback) {
	certificate = certificate || false;
	bLocal = bLocal || false;
	callback = callback || false;
	response['signature'] = null;
	var sd = CreateObject("CAPICOM.SignedData");
	if (!(sd === "NO_OBJECT")) {
		try {
			var signer = CreateObject("CAPICOM.Signer");
			if (!(signer === "NO_OBJECT")) {
				var cert = GetCert(certificate, bLocal, callback);
				signer.Certificate = cert;
				if (bLocal) {
					ReadLocalFile(false, url, signer, true, false, bLocal, sd, callback);
				} else {
					if (url) {
						response['error'] = err0;
						if (url == '') {
							response['error'] = err6;
						} else {
							sign(false, url, signer, true, false, bLocal, sd);
						}
					} else {
						response['signature'] = null;
						response['error'] = err4;
					}
					return response;
				}
			}
			return response;
		} catch (e) {
			return response;
		}
	}
	return response;
};

//END TBSignedData

//BEGIN TBCertificate
/**
 *
 * @returns {TBCertificate}
 * @constructor
 */
TBCertificate = function () {
	if (!(this instanceof TBCertificate)) {
		return new TBCertificate();
	}
	if (!$("object").is("#pluginKripto"))
		$("body").prepend('<object style = "width: 0; height:0" id = "pluginKripto" type = "application/x-digtcapicom" > </object >');
};

/**
 *
 * @type {null} �������� �����������
 */
var Certificates = null;
TBCertificate.CertificatePolicies = null;
TBCertificate.KeyUsage = null;
TBCertificate.ExtendedKeyUsage = null;
TBCertificate.ValidFrom = null;
TBCertificate.ValidTo = null;
TBCertificate.SerialNumber = null;
TBCertificate.IssuerName = null;
TBCertificate.SubjectName = null;

/**
 *
 * @type {{0: *}} ������ ������ � �� ��������
 */
var errCert0 = {0: BX.message('NO_ERROR')},
errCert1     = {1: BX.message('NO_CERT')},
errCert2     = {2: BX.message('NO_OBJECT')},
errCert3     = {3: BX.message('NO_DATA_IN_CERT')},
errCert4     = {4: BX.message('NO_PARAM')},
errCert99    = {99: BX.message('OTHER_ERROR')};

/**
 *
 * @type {Object} ����� �� ����� Import
 */
var answer = new Object();
answer['error'] = errCert0;

/**
 * Import Certificate
 * @param certificate ����������  ��� ��������� ��� �������. ��������� � ���� ������ � ������� BASE64
 * @returns {*} ���������� ������ � ������� � ��������� ������
 * @constructor
 */
TBCertificate.prototype.Import = function (certificate) {
	if (certificate) {
		Certificates = certificate;
		InfoCert(Certificates);
		if (answer['error'] == errCert0) {
			this.CertificatePolicies = CertificatePolicies;
			this.KeyUsage = KeyUsage;
			this.ExtendedKeyUsage = ExtendedKeyUsage;
			this.ValidFrom = ValidFrom;
			this.ValidTo = ValidTo;
			this.SerialNumber = SerialNumber;
			this.IssuerName = IssuerName;
			this.SubjectName = SubjectName;
		}
	} else {
		answer['error'] = errCert1;
	}
	return answer['error'];
};

/**
 * Export Certificate
 * @returns {null}   ���������� ����������, ������� ��� �������� � ������ Import
 * @constructor
 */
TBCertificate.prototype.Export = function () {
	return Certificates;
};
// END TBCertificate

// CAPICOM constants
var CAPICOM_STORE_OPEN_READ_ONLY = 0;
var CAPICOM_CURRENT_USER_STORE = 2;
var CAPICOM_CERTIFICATE_FIND_SHA1_HASH = 0;
var CAPICOM_CERTIFICATE_FIND_EXTENDED_PROPERTY = 6;
var CAPICOM_CERTIFICATE_FIND_TIME_VALID = 9;
var CAPICOM_CERTIFICATE_FIND_KEY_USAGE = 12;
var CAPICOM_DIGITAL_SIGNATURE_KEY_USAGE = 0x00000080;
var CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME = 0;
var CAPICOM_INFO_SUBJECT_SIMPLE_NAME = 0;
var CAPICOM_ENCODE_BASE64 = 0;
var CAPICOM_E_CANCELLED = -2138568446;
var CERT_KEY_SPEC_PROP_ID = 6;

var CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED = 2;
var CAPICOM_MY_STORE = "My";

var CAPICOM_CERTIFICATE_FIND_SUBJECT_NAME = 1;
var CAPICOM_CERTIFICATE_FIND_TIME_VALID = 9;

var CAPICOM_VERIFY_SIGNATURE_ONLY = 0;
var CAPICOM_VERIFY_SIGNATURE_AND_CERTIFICATE = 1;

// Cades constants
var CADESCOM_CADES_X_LONG_TYPE_1 = 0x5d;
var CADESCOM_BASE64_TO_BINARY = 0x01;
var CADES_BES = 0x01;

var SignedData;
var CadesSignedData;
var SignerPerson;
var MyStore;
var pkcs7;
var trusted = {};
var SIGN_FIRST = true;
var SIGN_SECOND = false;

/**
 *  ������� ��������� ���������� � �����������
 * @param certificate ���������� � ������� base64
 * @constructor
 */
function InfoCert(certificate) {
	var plugin = document.getElementById("pluginKripto");
	plugin.createDate = function (year, month, day, hrs, min, sec, ms) {
		return new Date(Date.UTC(year, month, day, hrs, min, sec, ms));
	}
	var Cert = CreateObject("CAPICOM.Certificate");
	if (Cert === "NO_OBJECT") {
		answer['error'] = errCert2;
	} else {
		try {
			Cert.Import(certificate);

			//CertificatePolicies

			var CertificatePolicies = [];
			Cert.Extensions().forEach(function (e, i) {
				if (e.OID.Value == "2.5.29.32") {
					e.EncodedData.Decoder().forEach(function (value, index) {
						CertificatePolicies[index] = [];
						CertificatePolicies[index]['FriendlyName'] = value.OID.FriendlyName;
						CertificatePolicies[index]['Name'] = value.OID.Name;
						CertificatePolicies[index]['Value'] = value.OID.Value;
					});
				}
			});
			if (Object.keys(CertificatePolicies).length > 0) {
				this.CertificatePolicies = CertificatePolicies;

			} else {
				this.CertificatePolicies = BX.message('NO_DATA_IN_CERT');
			}

			// KeyUsage

			var KeyUsage = {
				"IsCritical": Cert.KeyUsage().IsCritical,
				"IsCRLSignEnabled": Cert.KeyUsage().IsCRLSignEnabled,
				"IsDataEnciphermentEnabled": Cert.KeyUsage().IsDataEnciphermentEnabled,
				"IsDecipherOnlyEnabled": Cert.KeyUsage().IsDecipherOnlyEnabled,
				"IsDigitalSignatureEnabled": Cert.KeyUsage().IsDigitalSignatureEnabled,
				"IsEncipherOnlyEnabled": Cert.KeyUsage().IsEncipherOnlyEnabled,
				"IsKeyAgreementEnabled": Cert.KeyUsage().IsKeyAgreementEnabled,
				"IsKeyCertSignEnabled": Cert.KeyUsage().IsKeyCertSignEnabled,
				"IsKeyEnciphermentEnabled": Cert.KeyUsage().IsKeyEnciphermentEnabled,
				"IsNonRepudiationEnabled": Cert.KeyUsage().IsNonRepudiationEnabled,
				"IsPresent": Cert.KeyUsage().IsPresent
			};
			if (Object.keys(KeyUsage).length > 0) {
				this.KeyUsage = KeyUsage;
			} else {
				this.KeyUsage = BX.message('NO_DATA_IN_CERT');
			}

			//ExtendedKeyUsage

			var ExtendedKeyUsage = [];
			Cert.ExtendedKeyUsage().EKUs.forEach(function (e, index) {
				ExtendedKeyUsage[index] = [];
				ExtendedKeyUsage[index]['NAME'] = e.Name;
				ExtendedKeyUsage[index]['OID'] = e.OID;
			});
			if (Object.keys(ExtendedKeyUsage).length > 0) {
				this.ExtendedKeyUsage = ExtendedKeyUsage;
			} else {
				this.ExtendedKeyUsage = BX.message('NO_DATA_IN_CERT');
			}

			//ValidFromDate

			var ValidFromDate = Cert.ValidFromDate;
			if (!ValidFromDate == "") {
				this.ValidFrom = ValidFromDate;
			} else {
				this.ValidFrom = errCert3;
			}

			//ValidToDate

			var ValidToDate = Cert.ValidToDate;
			if (!ValidToDate == "") {
				this.ValidTo = ValidToDate;
			} else {
				this.ValidTo = BX.message('NO_DATA_IN_CERT');
			}

			//SerialNumber

			var SerialNumber = Cert.SerialNumber;
			if (!SerialNumber == "") {
				this.SerialNumber = SerialNumber;
			} else {
				this.SerialNumber = BX.message('NO_DATA_IN_CERT');
			}

			//IssuerName

			var IssuerName = Cert.IssuerName;
			if (!IssuerName == "") {
				this.IssuerName = IssuerName;
			} else {
				this.IssuerName = errCert3;
			}

			//SubjectName

			var SubjectName = Cert.SubjectName;
			if (!SubjectName == "") {
				this.SubjectName = SubjectName;
			} else {
				this.SubjectName = BX.message('NO_DATA_IN_CERT');
			}

		} catch (e) {
			if (BX.browser.IsIE()) { // for ie
				var strEinIE = e.message;
				if (strEinIE.indexOf("Ukazan nedeystvitel'nyy parametr") + 1) {
					answer['error'] = errCert4;
				} else {
					errCert99[99] = e.message;
					answer['error'] = errCert99;
				}
			} else {
				var strEinOther = Cert.getLastException();
				if (strEinOther.indexOf("Ukazan nedeystvitel'nyy parametr") + 1) {
					answer['error'] = errCert4;
				}
				else {
					errCert99[99] = Cert.getLastException();
					answer['error'] = errCert99;
				}
			}
		}
	}
}

/**
 *  ������� �������� �������
 * @param name ��� �������
 * @returns {*} ���������� ������ ��� ��������� �� ������
 * @constructor
 */
function CreateObject(name) {
	if (BX.browser.IsIE()) {
		try {
			var obj = new ActiveXObject(name);
			return obj;
		} catch (e) {
			var err = "NO_OBJECT";
			response['error'] = err5;
			return err;
			throw err;
		}
	} else {
		var cadesobject = document.getElementById("pluginKripto");
		try {
			var obj = cadesobject.CreateObject(name);
			return obj;
		} catch (e) {
			var err = "NO_OBJECT";
			response['error'] = err5;
			return err;
			throw err;
		}
	}

}

function createAttr(Name, Value) {
	var oAttr = CreateObject("CAPICOM.Attribute");
	if (!(oAttr === "NO_OBJECT")) {
		oAttr.Name = Name;
		oAttr.Value = Value;
		return oAttr;
	} else return 0;

}

function createDate(year, month, day, hrs, min, sec, ms) {
	return new Date(Date.UTC(year, month, day, hrs, min, sec, ms));
}

function getVarDate(date) {
	if (BX.browser.IsIE()) {
		return date.getVarDate();
	} else {
		return date;
	}
}

/**
 *
 * @param certificate ���������� � ������� base64 ��� false
 * @param bLocal ��������� ��� ���
 * @param callback ������� �������� ��� bLocal = true
 * @returns {*}
 * @constructor
 */
function GetCert(certificate, bLocal, callback) {
	var plugin = document.getElementById("pluginKripto");
	plugin.createDate = function (year, month, day, hrs, min, sec, ms) {
		return new Date(Date.UTC(year, month, day, hrs, min, sec, ms));
	}
	if (!certificate) {
		var Store = CreateObject("CAPICOM.Store");
		if (Store === "NO_OBJECT") {
			response['error'] = err5;
		} else {
			try {
				Store.Open(CAPICOM_CURRENT_USER_STORE, "My", CAPICOM_STORE_OPEN_READ_ONLY);
				var certs = Store.Certificates;
				var SelectedCerts = certs.Select(BX.message('TEXT_CrARM'), "", false);
				var cert = SelectedCerts.Item(1);
				var bHasPrivKey = cert.HasPrivateKey();
				if (!bHasPrivKey) {
					response['signature'] = null;
					response['error'] = err7;
				} else
					return cert;
			}
			catch (e) {
				if (BX.browser.IsIE()) {
					var strEinIE = e.message;
					if (strEinIE.indexOf('Operatsiya otmenena pol') + 1) {
						response['error'] = err15;
					}
					else if (strEinIE.indexOf('The requested operation has been cancelled by the user') + 1) {
						response['error'] = err5;
					}
					else if (strEinIE.indexOf('0x80880902') + 1) {
						response['error'] = err15;
					}
					else {
						err99[99] = e.message;
						response['error'] = err99;
					}
				} else {
					var strEinOther = Store.getLastException();
					if (strEinOther.indexOf('Operatsiya otmenena pol') + 1) {
						response['error'] = err15;
					}
					else if (strEinOther.indexOf('The requested operation has been cancelled by the user') + 1) {
						response['error'] = err5;
					}
					else if (strEinOther.indexOf('0x80880902') + 1) {
						response['error'] = err15;
					}
					else {
						err99[99] = Store.getLastException();
						response['error'] = err99;
					}
				}
			}
		}
	}
	else { // ���� ������ ����������, ���� ���
		var Cert = CreateObject("CAPICOM.Certificate");
		if (Cert === "NO_OBJECT") {
			response['error'] = err5;
		} else {
			try {
				Cert.Import(certificate);
				var bHasPrivKey = Cert.HasPrivateKey();
				if (!bHasPrivKey) {
					var myStore = CreateObject("CAPICOM.Store");
					myStore.Open(CAPICOM_CURRENT_USER_STORE, "My", CAPICOM_STORE_OPEN_READ_ONLY);
					var FindCerts = myStore.Certificates.Find(0, Cert.Thumbprint);
					if (FindCerts.Count > 0) {
						var bolHasPrivKey = FindCerts[0].HasPrivateKey();
						if (bolHasPrivKey) return FindCerts[0];
					}
					else {
						response['signature'] = null;
						response['error'] = err7;
					}
				} else {
					return Cert;
				}

			}
			catch (e) {
				if (BX.browser.IsIE()) { // for ie
					var strEinIE = e.message;
					if (strEinIE.indexOf("Ukazan nedeystvitel'nyy parametr") + 1) {
						response['error'] = err16;
					}
					else {
						err99[99] = e.message;
						response['error'] = err99;
					}

				} else {
					var strEinOther = Cert.getLastException();
					if (strEinOther.indexOf("Ukazan nedeystvitel'nyy parametr") + 1) {
						response['error'] = err16;
					}
					else {
						err99[99] = Cert.getLastException();
						response['error'] = err99;
					}
				}
			}
		}
	}
	if (bLocal) callback(response);
}

/**
 *
 * @param bText true -���� ������������� ��������� ������, false- ���� ����
 * @param data ������ ��� �������
 * @param signer
 * @param cosign true- ���� ��������� �������, false- ���� ���������
 * @param bDetached true - ���� ������� ���������, false - ���� �����������
 * @param bLocal true - ���� ��������� ����, false - ���� �� �������
 * @param sd
 * @param callback ���� bLocal true, �� � callback ���������� ����� �� �������
 */
function sign(bText, data, signer, cosign, bDetached, bLocal, sd, callback) {
	try {
		// ������� ���������� �������
		var cosign2 = function () {
			sd.Verify(data, false, CAPICOM_VERIFY_SIGNATURE_ONLY);
			if (signer === undefined) {
				response['error'] = err5;
				return;
			}
			var date = new Date();
			var datas = getVarDate(date);
			var attrib = createAttr(CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME, datas)
			if ((attrib === '0') || (attrib === undefined)) {
				response['error'] = err5;
			} else {
				signer.AuthenticatedAttributes.Add(createAttr(CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME, getVarDate(new Date())));
				var signedData = sd.CoSign(signer, CAPICOM_ENCODE_BASE64);
				pkcs7 = "-----BEGIN PKCS7-----\n" + signedData + "-----END PKCS7-----\n";
				if (signedData == "") {
					response['signature'] = null;
					response['error'] = err11;
				} else {
					response['signature'] = pkcs7;
				}
			}
		}
		//������� �������
		var sign2 = function () {
			if (signer === undefined) {
				response['error'] = err5;
				return;
			}

			var date = new Date();
			var datas = getVarDate(date);
			var attrib = createAttr(CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME, datas)
			if ((attrib === '0') || (attrib === undefined)) {
				response['error'] = err5;
			} else {
				signer.AuthenticatedAttributes.Add(attrib);
				var signedData = sd.Sign(signer, bDetached, CAPICOM_ENCODE_BASE64);
				pkcs7 = "-----BEGIN PKCS7-----\n" + signedData + "-----END PKCS7-----\n";
				if (signedData == "") {
					response['signature'] = null;
					response['error'] = err11;
				} else {
					response['signature'] = pkcs7;
					response['error'] = err0;
				}
			}
		}
		// ���������� �������
		if (cosign) {
			// ��� �� ����������
			if (!bLocal) {
				if (bText) {     // ���������� � �������, ���� ��� ������ ������
					sd.Content = data;
					cosign2();
					// ���� ��� ����
				} else {
					// ���������, ���� �� ����� ����, ���� ����, �� �������� ��� ������
					var filsize = getSizeFile(data);
					if (filsize == "No File") {
						response['signature'] = null;
						response['error'] = err17;
					}
					else if (filsize == 0) {
						err99[99] = BX.message('EMPTY_FILE');
						response['error'] = err99;
					} else {
						// �������� ������� � ����� �� �������
						data = getFileByURL(data, SIGN_SECOND, filsize, sd);
						cosign2();
					}
				}

			}
			// ���� ���� ��������� ��� ���������� �������
			else if (bLocal) {
				cosign2();
				callback(response);
			}
		}
		// ��������� �������
		else if (!cosign) {
			// ���� ���� �� ���������
			if (!bLocal) {
				// ���� ��� ��������� ������, � �� ����
				if (bText) {     // ���������� � �������, ���� ��� ������ ������
					sd.Content = data;
					//sd.SetContent(data, 0, false);
					sign2();
					// ���� ��� ����
				} else {
					// ���������, ���� �� ����� ����, ���� ����, �� �������� ��� ������
					var filsize = getSizeFile(data);
					if (filsize == "No File") {
						response['signature'] = null;
						response['error'] = err17;
					}
					else if (filsize == 0) {
						err99[99] = BX.message('EMPTY_FILE');
						response['error'] = err99;
					}
					else {
						// �������� ������� � ����� �� �������
						getFileByURL(data, SIGN_FIRST, filsize, sd);
						sign2();
					}
				}
			}
			// ���� ��� ��������� ���� ��� ��������� �������
			else if (bLocal) {
				sign2();
				callback(response);
			}
		}
	}
	catch (e) {
		if (BX.browser.IsIE()) { // for ie
			console.error(e.message);
			var strEinIE = e.message;
			if (strEinIE.indexOf('Operatsiya s tekushchimi dannymi ne dopuskaetsya politikoy litsenzirovaniya') + 1) {
				response['error'] = err13;
			}
			else if (strEinIE.indexOf("Ispol'zovanie provaydera ogranicheno litsenziey") + 1) {
				response['error'] = err12;
			}
			else if (strEinIE.indexOf("Proizoshla oshibka pri dobavlenii podpisi") + 1) {
				response['error'] = err14;
			}
			else if (strEinIE.indexOf('otmenena pol') + 1) {
				response['error'] = err15;
			}
			else if (strEinIE.indexOf('SignedData ne proinitsializirovan dolzhnym obrazom') + 1) {
				response['error'] = err11;
			}
			else if (strEinIE.indexOf('Nevernyy format dannykh') + 1) {
				response['error'] = err2;
			}
			else if (strEinIE.indexOf('V kachestve parametra metodu peredan nevernyy argument') + 1) {
				response['error'] = err19;
			}
			else if (strEinIE.indexOf('The certificate for the signer of the message is invalid or not found') + 1) {
				response['error'] = err10;
			}
			else if (strEinIE.indexOf('The signer is not found in the signed message') + 1) {
				response['error'] = err5;
			}
			else if (strEinIE.indexOf('Object doesn\'t support property or method') + 1) {
				response['error'] = err5;
			}
			else if (strEinIE.indexOf('konteyner otsutstvuet') + 1) {
				response['error'] = err18;
			}
			else if (strEinIE.indexOf('Nedopustimyy sertifikat litsa, podpisavshego soobshcheniy ili sertifikat ne nayden') + 1) {
				response['error'] = err20;
			}
			else {
				err99[99] = e.message;
				response['error'] = err99;
			}
		} else { // for other
			console.error(sd.getLastException());
			var strEinOther = sd.getLastException();
			if (strEinOther.indexOf('Operatsiya s tekushchimi dannymi ne dopuskaetsya politikoy litsenzirovaniya') + 1) {
				response['error'] = err13;
			}
			else if (strEinOther.indexOf("Ispol'zovanie provaydera ogranicheno litsenziey") + 1) {
				response['error'] = err12;
			}
			else if (strEinOther.indexOf("Proizoshla oshibka pri dobavlenii podpisi") + 1) {
				response['error'] = err14;
			}
			else if (strEinOther.indexOf('otmenena pol') + 1) {
				response['error'] = err15;
			}
			else if (strEinOther.indexOf('SignedData ne proinitsializirovan dolzhnym obrazom') + 1) {
				response['error'] = err11;
			}
			else if (strEinOther.indexOf('Nevernyy format dannykh') + 1) {
				response['error'] = err2;
			}
			else if (strEinOther.indexOf('V kachestve parametra metodu peredan nevernyy argument') + 1) {
				response['error'] = err19;
			}
			else if (strEinOther.indexOf('The certificate for the signer of the message is invalid or not found') + 1) {
				response['error'] = err10;
			}
			else if (strEinOther.indexOf('The signer is not found in the signed message') + 1) {
				response['error'] = err5;
			}
			else if (strEinOther.indexOf('Object doesn\'t support property or method') + 1) {
				response['error'] = err5;
			}
			else if (strEinOther.indexOf('konteyner otsutstvuet') + 1) {
				response['error'] = err18;
			}
			else if (strEinOther.indexOf('Nedopustimyy sertifikat litsa, podpisavshego soobshcheniy ili sertifikat ne nayden') + 1) {
				response['error'] = err20;
			}
			else {
				err99[99] = sd.getLastException();
				response['error'] = err99;
			}
		}
		if (bLocal) callback(response);
	}
}

/**
 * ���������� ���������� ����� �� ��������� URL
 * @param url ������ �� ����
 * @param typeSign ��������� �������, ���� typeSign = true
 * @param filesize ������ �����
 * @param sd
 * @returns {string}  ���� ��������� �������, �� ������� ������������ � sd.SetContent �������. ���� ���������, �� � ������
 */
function getFileByURL(url, typeSign, filesize, sd) {
	var result = '';
	if (typeSign) {
		var slice = Math.ceil(filesize / 1048576);
		for (var i = 0; i < slice; i++) {
			$.ajax({
				type: "POST",
				async: false,
				url: '/bitrix/js/trusted.api/ajax.php', // ��������� URL �
				data: {"sign": url, "slice": i}, // ������� sign ��� cosign
				success: function (data, textStatus) { // ������ ���� ���������� �� ������� success
					if (textStatus == 'success') {
						if (data != "" || data != null) {
							sd.SetContent(data, 0, true);
						}
					}
				},
				error: function (xhr, status, error) {
					console.log(status, error);
				}
			});
		}
	} else if (!typeSign) {
		var slice = Math.ceil(filesize / 1048576);
		for (var i = 0; i < slice; i++) {
			$.ajax({
				type: "POST",
				async: false,
				url: '/bitrix/js/trusted.api/ajax.php', // ��������� URL �
				data: {"cosign": url, "sliceCo": i}, // ������� sign ��� cosign
				success: function (data, textStatus) { // ������ ���� ���������� �� ������� success
					if (textStatus == 'success') {
						if (data != "" || data != null) {
							//sd.SetContent(data, 0, true);
							result += data;
						}
					}
				},
				error: function (xhr, status, error) {
					console.log(status, error);
				}
			});
		}
	}
	return result;
}

/**
 * ���������� ������ ��������� � ������
 * @param url ������ �� ����
 * @returns {string} �������� ����� �����
 */
function getSizeFile(url) {
	var response = '';
	$.ajax({
		type: "POST",
		async: false,
		url: '/bitrix/js/trusted.api/ajax.php', // ��������� URL �
		data: "uri=" + url, // ������� sign ��� cosign
		success: function (data, textStatus) { // ������ ���� ���������� �� ������� success
			if (textStatus == 'success') {
				if (data != "" || data != null) {
					response = data;
				}
			}
		},
		error: function (xhr, status, error) {
			console.log(status, error);
		}
	});
	return response;
}

/**
 * ������ ��������� ����
 * @param bText ����� ��� ����
 * @param file id input, � ������� ��������� ����
 * @param signer
 * @param cosign ��������� ������� ��� ���������
 * @param bDetached ��������� ������� ��� �����������
 * @param bLocal ��������� ���� ��� � �������
 * @param sd
 * @param callback ������� �������� �������� ��� bLocal = true
 * @constructor
 */
function ReadLocalFile(bText, file, signer, cosign, bDetached, bLocal, sd, callback) {
	try {
		if (!$("input").is("#" + file)) {
			response['signature'] = null;
			err99[99] = BX.message('NO_ID');
			response['error'] = err99;
			callback(response);
			return;
		}
		var f = document.getElementById(file);
		if (f.files.length === 0) {
			response['signature'] = null;
			err99[99] = BX.message('NO_SELECT_FILE');
			response['error'] = err99;
			callback(response);
			return;
		}
		var fr = new FileReader();
		fr.onload = function () {
			if (fr.error) {
				response['signature'] = null;
				err99[99] = BX.message('ERROR_READ_FILE');
				response['error'] = err99;
				callback(response);
				return;
			}
			else {
				var slide = 1048576;
				var buffer = fr.result;
				var binary = '';
				var dan = '';
				var bytes = new Uint8Array(buffer);
				var len = bytes.byteLength;
				var slice = Math.ceil(len / slide);
				if (len < slide) slide = len;

				for (var a = 0; a < slice; a++) {
					for (var i = a * slide; i < (a * slide + slide); i++) {
						binary += String.fromCharCode(bytes[i]);
					}
					if (!cosign) {
						sd.SetContent(btoa(binary), 0, true);
					} else {
						dan += binary;
					}
				}
				sign(bText, dan, signer, cosign, bDetached, bLocal, sd, callback);
			}
		};
		fr.readAsArrayBuffer(f.files[0]);
	} catch (e) {
		response['signature'] = null;
		err99[99] = e.message;
		response['error'] = err99;
		callback(response);
	}
}
