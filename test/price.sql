-- phpMyAdmin SQL Dump
-- version 3.4.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 31 2012 г., 00:54
-- Версия сервера: 5.5.13
-- Версия PHP: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `price`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tovar`
--

CREATE TABLE IF NOT EXISTS `tovar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `tovar`
--

INSERT INTO `tovar` (`id`, `name`, `price`, `quantity`) VALUES
(1, 'AM2+ Athlon II X2 7850+ 2.8 Ghz (AD785ZWCJ2BGH) Tray', 400, 10),
(2, 'AMD A4 X2 3300 (2.5GHz, 1Mb, 65W, GPU 444MHz, FM1) box', 500, 5),
(3, 'AMD A6-3650 (2.6GHz, 4Mb, 100W, FM1, 32nm) box', 10000, 1),
(4, 'AMD A6-3670K (2,7GHz, FM1, HT 4000MHz, GPU 443MHz, 4Mb, 32nm, 100W) Box', 120, 20),
(5, 'AMD A8-3850 (2,9GHz, FM1, HT 4000MHz, GPU 600MHz, 4Mb, 32nm, 100W) Box', 450, 2),
(6, 'AMD A8-3870K (3,0GHz, FM1, HT 4000MHz, GPU 600MHz, 4Mb, 32nm, 100W) Box', 123, 10),
(7, 'AMD Athlon II X2 250 (3.0GHz, HT 4000MHz, 2Mb, 45nm, 65W, AM3) Box', 1235, 1),
(8, 'AMD Athlon II X3 445 (3,1GHz, HT 4000MHz,1536Kb, 95W, АM3) Tray', 236, 1),
(9, 'AMD Athlon II X3 445 (3,1GHz, HT 4000MHz,1536Kb, 95W, АM3) Tray', 1223, 50),
(10, 'AMD Athlon II X4 640 (3,0GHz, 2Mb, 45nm, 95W, AM3) Box', 12, 236);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
