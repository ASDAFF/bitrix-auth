<?
// ���������� ����� ��� ������ � excel
require_once('test/Classes/PHPExcel.php');
// ���������� ����� ��� ������ ������ � ������� excel
require_once('test/Classes/PHPExcel/Writer/Excel5.php');

// ������� ������ ������ PHPExcel
$xls = new PHPExcel();
// ������������� ������ ��������� �����
$xls->setActiveSheetIndex(0);
// �������� �������� ����
$sheet = $xls->getActiveSheet();
// ����������� ����
$sheet->setTitle('������� ���������');

// ��������� ����� � ������ A1
$sheet->setCellValue("A1", '������� ���������');
$sheet->getStyle('A1')->getFill()->setFillType(
    PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('EEEEEE');

// ���������� ������
$sheet->mergeCells('A1:H1');

// ������������ ������
$sheet->getStyle('A1')->getAlignment()->setHorizontal(
    PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

for ($i = 2; $i < 10; $i++) {
	for ($j = 2; $j < 10; $j++) {
        // ������� ������� ���������
        $sheet->setCellValueByColumnAndRow(
                                          $i - 2,
                                          $j,
                                          $i . "x" .$j . "=" . ($i*$j));
	    // ��������� ������������
	    $sheet->getStyleByColumnAndRow($i - 2, $j)->getAlignment()->
                setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}
}
?>