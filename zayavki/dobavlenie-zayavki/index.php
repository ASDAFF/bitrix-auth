<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавление заявки");
CModule::IncludeModule('iblock'); 
?>  
<style type="text/css">  
		.table input, .uneditable-input {
		  width: 206px;
		  max-width: 120px;
		}
		.table {width: 100%}
		.table .currency{
			margin:0!important;
		}
		
		.btn-sm, .btn-group-sm > .btn {
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
}

.btn-primary {
    color: #ffffff;
    background-color: #428bca;
    border-color: #357ebd;
	text-decoration: none;
}
.my_button{float: left; width: 117px !important; background-color: #e1e1e1 !important; color: black !important; font-size: 13px !important; border: 1px gray solid !important; height: 22px !important;}

.tab {padding: 15px 30px; background: #CFE5E5; color: #147070; text-decoration: none}
.activ_tab {background: #147070; color: white}

#license_window input{float: right}
#urlica_window input{float: right}
.element_license {border-bottom: 1px solid yellow}
.element_urlica {border-bottom: 1px solid yellow}

.table_fizlica {font-size: 12px}
.table_fizlica td {text-align: center; border: 1px solid #333;}
</style>


<? 
$elem = GetIBlockElement($_GET["id_zayvki"]); 
$elements   = CIBlockElement::GetList( array(), array("IBLOCK_ID" => "6") );
while($ar_res = $elements->Fetch()) 
       {
         if($ar_res['NAME'] == $elem["PROPERTIES"]['M']["VALUE"]) $id_el = $ar_res['ID'];
       }	
$e = CIBlockElement::GetByID($id_el)->Fetch();
echo $e["DETAIL_TEXT"];
?>
<script type="text/javascript">
$(document).ready(function(){

 $(".my_button").click(function() {	         
			 $(this).parent().children("input").click();
	 })

   function readCookie(cookieName) {
        var re = new RegExp('[; ]'+cookieName+'=([^\\s;]*)');
        var sMatch = (' '+document.cookie).match(re);
        if (cookieName && sMatch) return unescape(sMatch[1]);
    }	  
   function setCookie(name,value,days) {
				if (days) {
					var date = new Date();
					date.setTime(date.getTime() + (days*24*60*60*1000));
					var expires = "; expires=" + date.toGMTString();
				}
				else var expires = "";
				document.cookie = name + "=" + escape(value) + expires + "; path=/";
		} 
   var inn = readCookie("inn");		
   setCookie("inn", "0", 10);

   $('.inn_select').change(function(){
           document.location.href = "/zayavki/dobavlenie-zayavki/?id_zayvki="+ $(this).val();	  	
   })
   
   
   
	

   var a = "<?echo $e["DETAIL_TEXT"];?>"
       var arr = a.split('_');
		   
		   $('.table input').each(function(){	         
				$(this).attr('value', '');					   
		   });
		   var t = 1;
		   $('.table input').each(function(){	         
				$(this).attr('value', arr[t]);
				
				t++;		   
		   });
   
   
   if($(".inn").val()){
       $(".block").css("pointer-events", "all");
	   $(".block").css("cursor", "pointer");
	   $(".block").css("color", "red");
   }

   
   $(".inn").change(function() {//для новой
       $.ajax({	           
                url    : "/ajax/inn_exist.php",
                dataType  : "html",
                type    : "POST",
                data    : {inn: $(this).val()},
		        success: function(response) {
                      $('.inn_exist').html(response);
                      }
	   })
	   
	   $("input").attr("disabled", false);
	   $(".block").css("pointer-events", "all");
	   $(".block").css("cursor", "pointer");
	   $(".block").css("color", "red");
       setCookie("inn", $(this).val(), 10);	
   })
   
   $( ".notice" ).change(function() {
 
       setCookie("notice", $(this).val(), 10);
	   if($(".notice").val()){
			  $.ajax({
						url    : "/ajax/download.php",
						dataType  : "html",
						type    : "POST",
						//data    : {inn: inn, table: table},
						success: function(response) {
							  $('.download_div').html(response);
							  }
					});
      }else{
	     alert("Вы не ввели номер извещения/закупки!");
	  }
   })



	

	
		 $("input").change(function() {	         
			 $(this).parent().children("input[type='hidden']").val($(this).parent().children("input[type='file']").val());
			 var file_name = $(this).parent().children("input[type='file']").val();
			 var file_arr = file_name.split('\\');
			 $(this).parent().children("a").html(file_arr[2]);
			 $(this).parent().children("a").css("pointer-events", "none");
			 $(this).parent().children("a").css("text-decoration", "none");
			 $(this).parent().children("a").css("color", "gray");
			 $(this).parent().children("input[type='file']").attr("name", $(this).parent().children("input[type='hidden']").attr("name"));
			 $(this).parent().children("input[type='hidden']").attr("name", "1111");
     })
	 
	 $('.for_signature').click(function(){
            $.ajax({
						url    : "/ajax/for_signature.php",
						dataType  : "html",
						type    : "POST",
						//data    : {a_cb: $("a .CB").attr("href"), inn: $(".inn").val()}, 
						data    : {a_cb: $(".CB").val(), inn: $(".inn").val()},
						success: function(response) {
							  $('.inn_exist').html(response);
							  }
			});	  	
	 })
	
	
///////////////////////////////////////////////////////////	
 $(".add_table_fizlica").click(function() {//для новой
	    /*$.ajax({	           
				url    : "/ajax/add_table_fizlica.php",
				dataType  : "html",
				type    : "POST",
				data    : {inn: $(this).val()},
				success: function(response) {
					  $('.inn_exist').html(response);
					  }
	    })*/
		$(".table_fizlica_inputs input").each(function(i) { 
		    arr[i] = $(this).val();
		})
		$('.table_fizlica tr:last').after('<tr><td style="width: 200px">'+arr[0]+'</td><td>'+arr[1]+'</td><td>'+arr[2]+'</td><td>'+arr[3]+'</td><td>'+arr[4]+'</td></tr>');
		
   })
	
	
})

</script>

<span id="operation" data="false"></span>
<script type="text/javascript">
    function assent(n){
	   if(n === 0){ 
	        $(".assent_content").hide();
	   }
	   else{ 
	        $(".assent_content").show();
	   }
	}
	
	function add_license(){	
        count_license = $('.element_license').size();
        //alert(count_license)		
		if(count_license == 1) $('.element_license').append('<a href = "#" onclick = "remove_license(\'license1\')">Удалить</a>')
		count_license = count_license + 1;
		content_license ='<div id="license'+(count_license)+'" class="element_license"><div class="element_anketa">Вид деятельности<input type="text" name="prop[]"></div><div class="element_anketa">Номер лицензии<input type="text" name="prop[]"></div><div class="element_anketa">Дата выдачи лицензии<input type="text" name="prop[]"></div><div class="element_anketa">Кем выдана лицензия<input type="text" name="prop[]"></div><div class="element_anketa">Перечень видов лицензируемой деятельности (через запятую)<input type="text" class="princ" name="prop[]"></div><div class="element_anketa">Скан лицензии <input type="file" name="prop[]"></div><a href = "#" onclick = "add_license()">Добавить</a> <a href = "#" onclick = "remove_license(\'license'+(count_license)+'\')">Удалить</a></div>';
	       $('#license_window').append(content_license)		   
	}	
	function remove_license(n){		
	    count_license = $('.element_license').size();
		if(count_license != 1)$("#"+n).remove();
        else alert("Удаление не возможно, осталась одна форма лицензии!");		
	}
	
	function add_urlica(){	
        count_urlica = $('.element_urlica').size();
        //alert(count_license)		
		if(count_urlica == 1) $('.element_urlica').append('<a href = "#" onclick = "remove_license(\'urlica1\')">Удалить</a>')
		count_urlica = count_urlica + 1;
		content_urlica ='  <div id = "urlica'+(count_urlica)+'" class = "element_urlica"><div class="element_anketa">Наименование<input type="text" name="urlica[]"></div><div class="element_anketa">ИНН<input type="text" name="urlica[]"></div>	<div class="element_anketa">ОГРН<input type="text" name="urlica[]"></div><div class="element_anketa">Местонахождение<input type="text" name="urlica[]"></div><div class="element_anketa">Доля в процентах<input type="text" name="urlica[]">					</div><a href = "#" onclick = "add_urlica()">Добавить</a> <a href = "#" onclick = "remove_urlica(\'urlica'+(count_urlica)+'\')">Удалить</a></div>';
	       $('#urlica_window').append(content_urlica)		   
	}	
	function remove_urlica(n){		
	    count_urlica = $('.element_urlica').size();
		if(count_urlica != 1)$("#"+n).remove();
        else alert("Удаление не возможно, осталась одна форма юр лица!");		
	}
	
  
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    function comgos(a){
        console.log(a);
        if(a === 1){
            $("#notice").val("Номер извещения");
            $(".notice").prop( "disabled", true );
            $(".notice_box").css("display", "none");
        }else{
            $(".notice").prop( "disabled", false );
            $("#notice").val("");
            $(".notice_box").css("display", "block");
        }
    }
    
    function show_file(a){
        if(a === 1){
            $(".file").prop( "disabled", false );
            $(".file_box").css("display", "block");
        }else{
            $(".file").prop( "disabled", true );
            $(".file_box").css("display", "none");
        }
    }
    $( document ).ready(function() {
    	$('#GARANTE1').click(function(){
        		$('#add_new_bg').find('input[name=D1]').prop( "disabled", true );
        		$('#add_new_bg').find('input[name=D1]').css("display", "none");
        	});
    	$('#GARANTE2').click(function(){
    		$('#GARANTE1').prop('checked', false);
    			$('#add_new_bg').find('input[name=D1]').prop( "disabled", false );
    			$('#add_new_bg').find('input[name=D1]').css("display", "initial");
    	});
		
		
		$('.BG_tab').click(function(){
	        if($(".BG_window").css("display") == "block"){$(".BG_window").css("display", "none")}
			else{
			    $(".BG_window").css("display", "block");
				$(".BG_tab").addClass("activ_tab");
			    $(".applicant_window").css("display", "none");
			    $(".doc_window").css("display", "none");
				$(".applicant_tab").removeClass("activ_tab");
				$(".doc_tab").removeClass("activ_tab");
                
			}
    	});
		$('.applicant_tab').click(function(){
	        if($(".applicant_window").css("display") == "block"){$(".applicant_window").css("display", "none")}
			else{
			    $(".BG_window").css("display", "none");
			    $(".applicant_window").css("display", "block");
				$(".applicant_tab").addClass("activ_tab");
			    $(".doc_window").css("display", "none");
				$(".BG_tab").removeClass("activ_tab");
				$(".doc_tab").removeClass("activ_tab");
			}
    	});
		$('.doc_tab').click(function(){
		if($(".doc_window").css("display") == "block"){$(".doc_window").css("display", "none")}
		else{
			$(".BG_window").css("display", "none");
			$(".applicant_window").css("display", "none");
			$(".doc_tab").addClass("activ_tab");
			$(".doc_window").css("display", "block");
			$(".BG_tab").removeClass("activ_tab");
			$(".applicant_tab").removeClass("activ_tab");
		}
    	});
		
		
		
        $('#license').change(function(){
	        if($(this).val() == "Да") $('#license_window').css("display", "block");
		    else $('#license_window').css("display", "none");
	    })
		 $('#site').change(function(){
	        if($(this).val() == "Да") $('#site_window').css("display", "block");
		    else $('#site_window').css("display", "none");
	    })
		$('#urlica').change(function(){
	        if($(this).val() == "Да") $('#urlica_window').css("display", "block");
		    else $('#urlica_window').css("display", "none");
	    })
		
		
		
    });	  
</script>




<?
CModule::IncludeModule("iblock");
if(!isset($_GET["id_zayvki"])){
    $userID = $USER->GetID();	
    $rsUser = CUser::GetByID($userID);
    $arUser = $rsUser->Fetch();
	$current_user_id = $arUser[ID];
	
$elements   = CIBlockElement::GetList(Array("PROPERTY_L"=>"ASC", "ID" => "DESC"), array("IBLOCK_ID" => "3"));						
?>
</br></br></br></br>

<!--<h1>Сделайте выбор уже имеющегося клиента по инн или создайте новую заявку</h1>-->
<span>&nbsp; Выберите клиента: </span>
<select class="inn_select">
    <?
	echo "<option></option>";
	echo "<option value=0 style='color: blue'>Новый клиент</option>";
	while($ar_res = $elements->Fetch()) 
	{
	
	    if($ar_res["MODIFIED_BY"] == $arUser["ID"]){
		   $pr = GetIBlockElement($ar_res["ID"]);
		   if($pr["PROPERTIES"]['M']["VALUE"] && !(in_array($pr["PROPERTIES"]['M']["VALUE"], $inn1))){ 
		       $inn1[$i] = $pr["PROPERTIES"]['M']["VALUE"];	
		       echo "<option value=".$ar_res["ID"].">".$pr["PROPERTIES"]['L']["VALUE"]."(".$pr["PROPERTIES"]['M']["VALUE"].")</option>";
		   }	   
        }		   
	}   
	?>
</select>	
<?}else{
$el = GetIBlockElement($_GET["id_zayvki"]);
//print_r($el);
?>
	

<div class="wrapper">
                        <form method="POST" action='/include/add_BG.php' id="add_new_bg" class='on_ajax add_element' enctype="multipart/form-data">
<div>						
<p><a class="BG_tab tab activ_tab" href="#">Данные БГ</a><a class="applicant_tab tab" href="#">Данные заявителя правка</a><a class="doc_tab tab" href="#">Документы</a></p>
	<div class="BG_window" style="display: block; padding-top: 10px">
        <div class="element_anketa notice_box">
               <input type="text" id="notice" class="notice" name='A' placeholder='Номер извещения/закупки'>
        </div>
					   
			   </br>
			   <div class="download_div">								   
			   </div>
      
</div>
	
	
	
	
<div class="applicant_window" style="display: none; padding-top: 10px; width: 800px">
                                           
                              
                                     
                                        <div class="element_anketa">
                                               Наименование<input type="text" class="princ" name='L'value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">												
                                        </div>
										
										<div class="element_anketa">
                                               Полное фирменное наименование юридического лица<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Сокращенное фирменное наименование юридического лица<input type="text" class="princ" name="prop[]"value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Полное наименование на иностранном языке<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Сокращенное наименование на иностранном языке<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Организационно-правовая форма
											   <select style="float: right; width: 310px" name="prop[]">
                                                    <option>Общество с ограниченной ответственностью</option>
													<option>Акционерное общество/Закрытое акционерное общество</option>
													<option>Публичное акционерное общество/Открытое акционерное общество</option>
													<option>Иное</option>
                                               </select>											   
                                        </div>
										
										<div class="element_anketa">
                                               Наименование регистрирующего органа<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Дата государственной регистрации<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Дата выдачи свидетельства о регистрации<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>

                                        <div class="element_anketa">
                                               Место государственной регистрации (местонахождение)<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										
										
										
										<div class="element_anketa">
                                               ИНН<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               ОГРН<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               КПП<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               ОКПО<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               ОКАТО<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Основной код ОКВЭД(КДЕС Ред. 2)<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                                Тип собственности
                                                <select style="float: right; width: 310px" name="prop[]">
                                                    <option>Негосударственная организация</option>
													<option>Организация, находящаяся в федеральной собственности</option>
													<option>Организация, находящаяся в государственной (кроме федеральной) собственности</option>
                                               </select>												   
                                        </div>
										
										<div class="element_anketa">
                                                Тип организации
                                                <select style="float: right; width: 310px" name="prop[]">
                                                    <option>Коммерческая организация</option>
													<option>Некоммерческая организация</option>
													<option>Финансовая организация</option>
                                               </select>												   
                                        </div>
										
										
										
										
										
										
										<div class="element_anketa">
                                               Бик Банка<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Наименование Банка<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Номер р/с<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Наличие картотеки 
											   <select style="float: right; width: 310px" name="prop[]">
                                                    <option>Нет</option>	
													<option>Да</option>
																									
                                               </select>									
                                        </div>
										
										<div class="element_anketa">
                                               Оплаченый УК<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Объявленный УК<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										
										<div class="element_anketa">
                                               Численность работников согласно КНД 1110018<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										<br>
										<hr>
										<div class="element_anketa">
                                                <span style="color: green">Наличие лицензий</span>
                                  				<select id = "license" style="float: right; width: 310px" name="prop[]">
                                                    <option>Нет</option>	
													<option>Да</option>																							
                                               </select>							   
                                        </div>
										
											<div id = "license_window" style="display: none">
											    <div id = "license1" class = "element_license">
												<div class="element_anketa">
													   Вид деятельности<input type="text" name="license[]">											
												</div>																				
												<div class="element_anketa">
													   Номер лицензии<input type="text" name="license[]">											
												</div>
												
												<div class="element_anketa">
													   Дата выдачи лицензии<input type="text" name="license[]">											
												</div>
												
												<div class="element_anketa">
													   Кем выдана лицензия<input type="text" name="license[]">											
												</div>
												
												<div class="element_anketa">
													   Перечень видов лицензируемой деятельности (через запятую)<input type="text" class="princ" name="license[]">											
												</div>
												
												<div class="element_anketa">
													   Скан лицензии <input type="file" name="license[]">											
												</div>
												<a href = "#" onclick = "add_license()">Добавить</a>
											</div>
										</div>
										<hr>
										
										
										
										
										
										
										
										<div class="element_anketa">
                                               <span style="color: green">Имеется сайт компании</span>
                                               <select id = "site" style="float: right; width: 310px" name="prop[]">
                                                    <option>Нет</option>	
													<option>Да</option>
																									
                                               </select>											   
                                        </div>
										
										<div class="element_anketa" id = "site_window" style="display: none">
                                               Сайт компании<input type="text" class="princ" name="prop[]" placeholder = "http://" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">											
                                        </div>
										</br>
										<hr>
										<div class="element_anketa">
                                               <span style="color: green">Юридические лица – участники (акционеры) с долей не менее 1 % </span>
											   <select id = "urlica" style="float: right; width: 310px" name="prop[]">
                                                    <option>Нет</option>	
													<option>Да</option>
																									
                                               </select>	
                                        </div>
										
										<div id = "urlica_window" style="display: none">
										    <div id = "urlica1" class = "element_urlica">
											<div class="element_anketa">
												   Наименование<input type="text" name="urlica[]">											
											</div>
											<div class="element_anketa">
												   ИНН<input type="text" name="urlica[]">												
											</div>	
                                            <div class="element_anketa">
												   ОГРН<input type="text" name="urlica[]">												
											</div>
                                            <div class="element_anketa">
												   Местонахождение<input type="text" name="urlica[]">												
											</div>
                                            <div class="element_anketa">
												   Доля в процентах<input type="text" name="urlica[]">												
											</div>												
											<a href = "#" onclick = "add_urlica()">Добавить</a>
											</div>
										</div>
										<hr>
										<h4>Адрес</h4>
										<div class="element_anketa">
                                            Страна<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">                                           										 
                                        </div>	
										<div class="element_anketa">
                                            Регион<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Район<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Город<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Населенный пункт<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Улица, проспект, переулок и пр.<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Дом / строение / корпус<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Офис<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            ОКАТО<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Индекс<input type="text" class="princ" name="prop[]" value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" style="float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Помещение
                                            <select style="float: right; width: 310px" name="prop[]">
                                                    <option>В собственности</option>	
													<option>В оренде</option>																								
                                            </select>											
                                        </div>
										<hr>
										<h4>Бенефициары из США</h4>
										<div class="element_anketa">                                         
                                        Входит ли в состав контролирующих лиц (бенефициаров) компании, которым прямо или косвенно принадлежит более 10% доли в компании, одно из следующих лиц:</br>										
                                        1) физические лица, которые являются налоговыми резидентами США;	</br>									
                                        2) юридические лица, которые зарегистрированы/учреждены на территории США?                     
											<select style="float: right; width: 310px" name="prop[]">
                                                    <option>Нет</option>	
													<option>Да</option>																								
                                            </select>                                          										 
                                        </div>
										</br>
										<hr>
										<div class="element_anketa">
                                             Является ли организация финансовым институтом для целей FATCA?
											 <select style="float: right; width: 310px" name="prop[]">
                                                    <option>Да</option>		
													<option>Нет</option>	
																																			
                                            </select>                                            										 
                                        </div>
										<hr>
										<hr>




										
										<h4>Сведения о физических лицах-представителях организации</h4>
										<div class="table_div">
										<table class="table_fizlica">
										    <tr style="background: #e9e9e9">
											    <td style="width: 200px">ФИО</td><td>Единоличный исполнительный орган</td><td>Бенефициар (Доля в уставном капитале)</td><td>Участник (акционер) с долей не менее 1%
(Доля в уставном капитале)</td><td>Представитель
по доверенности</td>
											</tr>
											
										</table>
										</br>
										<div class="table_fizlica_inputs">
										<div class="element_anketa">
                                            ФИО <input type="text" style="width: 310px; float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Единоличный исполнительный орган <input type="text" style="width: 310px; float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Бенефициар (Доля в уставном капитале) <input type="text" style="width: 310px; float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Участник (акционер) с долей не менее 1%
(Доля в уставном капитале) <input type="text" style="width: 310px; float: right">                                           										 
                                        </div>
										<div class="element_anketa">
                                            Представитель
по доверенности <input type="text" style="width: 310px; float: right">                                           										 
                                        </div>
										</div>
										</br>
                                        <button type="button" class="add_table_fizlica" style="cursor: pointer">Добавить физ лицо</button>
                                        <hr>
										<hr>
										<h4>Отношения с кредитной организацией</h4>
										
										
										
										
										
                                        <!--<div class="element_anketa">
                                                <input type="text" class="<?if($_GET["id_zayvki"] == 0) echo "inn";?>" name='<?if($_GET["id_zayvki"] == 0) echo "M";?>' <?if($_GET["id_zayvki"] != 0) echo "disabled";?> placeholder='ИНН принципала' value="<?=$el["PROPERTIES"]['M']["VALUE"]?>"><span style="color:red" class="inn_exist"></span>
												<?if($_GET["id_zayvki"] != 0){?><input type="hidden" class="inn" value="<?=$el["PROPERTIES"]['M']["VALUE"]?>" name='M'><?}?>
                                        </div>
										<div class="element_anketa">
                                                <input type="text" class="ogrn" name='<?if($_GET["id_zayvki"] == 0) echo "OGRN_PRINC";?>' <?if($_GET["id_zayvki"] != 0) echo "disabled";?> placeholder='ОГРН принципала' value="<?=$el["PROPERTIES"]['OGRN_PRINC']["VALUE"]?>">
                                                <?if($_GET["id_zayvki"] != 0){?><input type="hidden" value="<?=$el["PROPERTIES"]['OGRN_PRINC']["VALUE"]?>" name='OGRN_PRINC'><?}?>
										</div>
                                        <div class="element_anketa">
                                                Шаблон заказчика
                                                <input class="required" type="radio" onclick="show_file(1);" id="avans41" name="N" value="11">
                                                <label for="avans41">Да</label>
                                                <input class="required" type="radio" onclick="show_file(2);" id="avans42" name="N" value="12">
                                                <label for="avans42">Нет</label>

                                        </div>
                                        <div class="element_anketa file_box" style="display: none;">
                                                Загрузите образец БГ
                                                <input type="file"class="file" name="FILE" disabled="">
                                        </div>                                       
                               

                                        <div class="element_anketa">
                                            Тип гарантии
                                            <input class="required" type="radio" onclick="comgos(1)" id="avans141" name="I" value="7">
                                            <label for="avans141">Коммерческая</label>
                                            <input class="required" type="radio" onclick="comgos(2)" checked="checked" id="avans142" name="I" value="8">
                                            <label for="avans142">Госзакупка</label>
                                        </div>
										
									
                                        
										
										
										
                                        <div class="element_anketa">
                                                <input class="main_element_form" type="text" name='B' placeholder='Сумма БГ'>
                                                <div class="new-select-style-wpandyou-anceta currency">
                                                        <select name="CURRENCY" class="main_element_form">
                                                                <option value="0" selected="">руб.</option>
                                                                <option value="24">$</option>
                                                                <option value="25">€</option>
                                                        </select>
                                                </div>
                                        </div>
                                  
                                        <div class="element_anketa">
                                        		Срок гарантии с момента выдачи 
                                        		<input class="required" type="radio" id="GARANTE1" checked="checked" name="GARANTE_VIDACHI" value="26">
                                                <label for="avans11">Да</label>
                                                <input class="required" type="radio" id="GARANTE2" name="GARANTE_VIDACHI" value="27">
                                                <label for="avans12">Нет</label>
                                        </div>
                                        <div class="element_anketa">
                                                Срок действия <input id="date_from" type="text" name="D1" placeholder='с'  style="display:none" disabled placeholder='' class="date_time datepicker" autocomplete="false"> <input placeholder='по' id="date_to" type="text" name='D2' placeholder='' class="date_time datepicker" autocomplete="false"> <span class="error text"></span>
                                        </div>
                                        <div class="element_anketa">
                                                Аванс
                                                <input class="required" type="radio" id="avans11" name="E" value="1">
                                                <label for="avans11">Да</label>
                                                <input class="required" type="radio" id="avans12" name="E" value="2">
                                                <label for="avans12">Нет</label>
                                        </div>
                                        
                                        <div class="element_anketa">
                                                <textarea placeholder='Комментарии' cols="53" rows="5" name='H'></textarea>
                                        </div>  -->   

                                   
</div>
	
	
	
<div class="doc_window" style="display: none; padding-top: 10px">
                                        <div class="uslugi">

<div class="horizont_line"></div>
<div class="element_anketa">
        Заявление на предоставление банковской гарантии, <a href="/zayavki/document_list/">образец</a>
        <div class="file_button">
                <input type="file" name='AE'>
        </div>
     

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Паспорта всех Учредителей, Генерального директора и Главного бухгалтера
        <div class="file_button">
                
				 <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='CB' class="CB"> 
				<?}else{?>
				   <?
				      $URL_CB = CFile::GetPath($el["PROPERTIES"]['CB']['VALUE']);
                    			  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a class = "CB" href="<?=$URL_CB?>">Посмотреть</a>
                   <input type="hidden" name='CB' value="<?=$URL_CB?>">
                <?}?>
        </div>     
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Проект контракта 
        <div class="file_button">
                <input type="file" name='AH'>
        </div>     
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Согласия юридического и физического лиц на запрос и предоставление информации в Бюро Кредитных Историй для всех учредителей и директора, <a href="/zayavki/document_list/">образец</a>
        <div class="file_button">             
				  <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='SOGLASIE_V_BURO'>
				<?}else{				
				    $URL_SOGLASIE_V_BURO = CFile::GetPath($el["PROPERTIES"]['SOGLASIE_V_BURO']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_SOGLASIE_V_BURO?>" download >Посмотреть</a>
				   <!--333333-->
                   <input type="hidden" name='SOGLASIE_V_BURO' value="<?=$URL_SOGLASIE_V_BURO?>">
                <?}?>
        </div>     
</div>


<div class="horizont_line"></div>
<div class="element_anketa">
        Баланс и отчет о финансовых результатах предприятия за последние 5 отчетных периодов. 
        <div class="file_button">
                <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='FIZ_BALANCE'>
				<?}else{				
				    $URL_FIZ_BALANCE = CFile::GetPath($el["PROPERTIES"]['FIZ_BALANCE']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_FIZ_BALANCE?>">Посмотреть</a>
                   <input type="hidden" name='FIZ_BALANCE' value="<?=$URL_FIZ_BALANCE?>">
                <?}?>				
        </div>     
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Финансовая отчетность за последние 5 отчетных периодов
        <div class="file_button">
               <a href="#"  class="block click-me" style="pointer-events: none; /* делаем элемент неактивным для взаимодействия */
cursor: default; /*  курсор в виде стрелки */
color: #888; float: left">Заполнить </a>&nbsp;
 <?if (file_exists($_SERVER['DOCUMENT_ROOT']."/archives/".$el["PROPERTIES"]['M']["VALUE"]."/fin.xlsm")) {?>
    <a href="<?$_SERVER['DOCUMENT_ROOT']?>/archives/<?=$el["PROPERTIES"]['M']["VALUE"]?>/fin.xlsm" download>Скачать</a>
 <?}?>
        </div>     
</div>

<div class="horizont_line"></div>
<div class="element_anketa">
        Выписка из ЕГРЮЛ (последняя полученная выписка из ЕГРЮЛ)*
        <div class="file_button">
                <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='EGRUL'>
				<?}else{				
				    $URL_EGRUL = CFile::GetPath($el["PROPERTIES"]['EGRUL']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_EGRUL?>">Посмотреть</a>
                   <input type="hidden" name='EGRUL' value="<?=$URL_EGRUL?>">
                <?}?>				
        </div>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Устав
        <div class="file_button">
                <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='ARTICLES'>
				<?}else{				
				    $URL_ARTICLES = CFile::GetPath($el["PROPERTIES"]['ARTICLES']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_ARTICLES?>">Посмотреть</a>
                   <input type="hidden" name='ARTICLES' value="<?=$URL_ARTICLES?>">
                <?}?>				
        </div>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Решение о назначении на должность единоличного исполнительного органа (директора, генерального директора, президента компании и пр.) *
        <div class="file_button">
                <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='RESH_O_NAZNACH'>
				<?}else{				
				   $URL_RESH_O_NAZNACH = CFile::GetPath($el["PROPERTIES"]['RESH_O_NAZNACH']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_RESH_O_NAZNACH?>">Посмотреть</a>
                   <input type="hidden" name='RESH_O_NAZNACH' value="<?=$URL_RESH_O_NAZNACH?>">
                <?}?>				
        </div>    
</div>




<div class="horizont_line"></div>
<div class="element_anketa">		
        Сделка банковской гарантии требует одобрения вышестоящих органов управления?
		<span><input type="radio" name = "ass" onclick="assent(0);" checked="checked"> Нет </span>
		<span><input type="radio" name = "ass" onclick="assent(1);"> Да  </span>
		<div class="assent_content" style="display: none">
			Протокол одобрения сделки *
			<div class="file_button">
					<?if($_GET["id_zayvki"] == 0){?>
					   <input type="file" name='POROTOCOL_OD_SDELKI'>
					<?}else{				
						$URL_POROTOCOL_OD_SDELKI = CFile::GetPath($el["PROPERTIES"]['POROTOCOL_OD_SDELKI']['VALUE']);                    				  
					   ?>
					   <button type="button" class="my_button">Выберите файл</button>
					   <input type="file" disabled style="display: none">
					   <a href="<?=$URL_POROTOCOL_OD_SDELKI?>">Посмотреть</a>
					   <input type="hidden" name='EGRUL' value="<?=$URL_POROTOCOL_OD_SDELKI?>">
					<?}?>				
			</div> 
      </div>			
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Бухгалтерская отчетность с отметкой ИФНС (за последний отчетный год)*
        <div class="file_button">
                <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='BUH_OT_IFNS'>
				<?}else{				
				   $URL_BUH_OT_IFNS = CFile::GetPath($el["PROPERTIES"]['BUH_OT_IFNS']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_BUH_OT_IFNS?>">Посмотреть</a>
                   <input type="hidden" name='BUH_OT_IFNS' value="<?=$URL_BUH_OT_IFNS?>">
                <?}?>				
        </div>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Справки ИФНС о наличии / отсутствии просроченной задолженности по налогам (сроком не позднее 30 календарных дней с даты выдачи) *
        <div class="file_button">
                <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='SPRAVKI_IFNS'>
				<?}else{				
				   $URL_SPRAVKI_IFNS = CFile::GetPath($el["PROPERTIES"]['SPRAVKI_IFNS']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_SPRAVKI_IFNS?>">Посмотреть</a>
                   <input type="hidden" name='SPRAVKI_IFNS' value="<?=$URL_SPRAVKI_IFNS?>">
                <?}?>				
        </div>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Справки из ИФНС об открытых р/счетах (необязательный документ в составе заявки)
        <div class="file_button">
                <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='SPRAVKI_IFNS_RAS_SH'>
				<?}else{				
				   $URL_SPRAVKI_IFNS_RAS_SH = CFile::GetPath($el["PROPERTIES"]['SPRAVKI_IFNS_RAS_SH']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_SPRAVKI_IFNS_RAS_SH?>">Посмотреть</a>
                   <input type="hidden" name='SPRAVKI_IFNS_RAS_SH' value="<?=$URL_SPRAVKI_IFNS_RAS_SH?>">
                <?}?>				
        </div>    
</div>

<div class="horizont_line"></div>
<div class="element_anketa">
        Документ, подтверждающий право собственности / аренды помещения
        <div class="file_button">
                <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='DOC_PODTV_PRAVO'>
				<?}else{				
				   $URL_DOC_PODTV_PRAVO = CFile::GetPath($el["PROPERTIES"]['DOC_PODTV_PRAVO']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_DOC_PODTV_PRAVO?>">Посмотреть</a>
                   <input type="hidden" name='DOC_PODTV_PRAVO' value="<?=$URL_DOC_PODTV_PRAVO?>">
                <?}?>				
        </div>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Отзывы контрагентов
        <div class="file_button">
                <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='OTZYVY_KONTR'>
				<?}else{				
				   $URL_OTZYVY_KONTR = CFile::GetPath($el["PROPERTIES"]['OTZYVY_KONTR']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_OTZYVY_KONTR?>">Посмотреть</a>
                   <input type="hidden" name='OTZYVY_KONTR' value="<?=$URL_OTZYVY_KONTR?>">
                <?}?>				
        </div>    
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Отзывы кредитных организаций, ранее обслуживавших Клиента, с информацией об оценке деловой репутации
        <div class="file_button">
                <?if($_GET["id_zayvki"] == 0){?>
                   <input type="file" name='OTZYVY_KRED'>
				<?}else{				
				   $URL_OTZYVY_KRED = CFile::GetPath($el["PROPERTIES"]['OTZYVY_KRED']['VALUE']);                    				  
				   ?>
				   <button type="button" class="my_button">Выберите файл</button>
				   <input type="file" disabled style="display: none">
				   <a href="<?=$URL_OTZYVY_KRED?>">Посмотреть</a>
                   <input type="hidden" name='OTZYVY_KRED' value="<?=$URL_OTZYVY_KRED?>">
                <?}?>				
        </div>    
</div>












<div class="horizont_line"></div>
<?if(isset($_GET["finish"])){?>
	<div class="element_anketa">
			Архив
			<div class="file_button">

												<td><a href="<?$_SERVER['DOCUMENT_ROOT']?>/archives/<?=$el["PROPERTIES"]['M']["VALUE"]?>/<?=$_GET["id_zayvki"]?>.zip" download>Скачать</a></td>								
			</div>     
	</div>

<div class="horizont_line"></div>
<?}?>
<?if(isset($_GET["finish"])){?>
<div class="element_anketa">
        Ссылка на страницу подписи
        <div class="file_button">
               <a href="https://pbg.nodomain.me/zayavki/signature/?id=<?=$el["ID"]?>&inn=<?=$el["PROPERTIES"]['M']["VALUE"]?>">Ссылка</a>

        </div>     
</div>
<div class="horizont_line"></div>
<?}?>
	</div>
</div>



                        
                             

                               
                            
								

<?/*
<div class="element_anketa">
        Баланс и отчет о финансовых результатах предприятия за  последний отчетный период 
        <div class="file_button">
                <input type="file" name='FIZ_BALANCE_LAST_PERIOD'>
        </div>     
</div>
<div class="horizont_line"></div>
*/?>
<!--<div class="element_anketa">
        Годовая декларация по налогу на прибыль предприятия за последний завершенный финансовый год с отметкой ФНС о принятии непосредственно на отчетности или с приложением документа, подтверждающего факт передачи отчетности в налоговой орган /если не применяется режим ЕНВД или УСН/
        <div class="file_button">
                <input type="file" name='FIZ_GOD_DEKL'>
        </div>     
</div>
<div class="horizont_line"></div>-->
<!--<div class="element_anketa">
        Налоговые декларации за последний календарный год, с отметкой ФНС о принятии непосредственно на отчетности или с приложением документа, подтверждающего факт передачи отчетности в налоговой орган /если компания уплачивает ЕНВД или применяет УСН/
        <div class="file_button">
                <input type="file" name='FIZ_NALOG_DEKL'>
        </div>     
</div>
<div class="horizont_line"></div>-->
<!--<div class="element_anketa">
        Информационное письмо, содержащее сведения: об открытых расчетных счетах  в других кредитных организациях; об оборотах по указанным счетам за последние 12 месяцев; об отсутствии/наличии картотеки №2; об отсутствии/наличии ссудной задолженности за последние 12 месяцев, <a href="/zayavki/document_list/">образец</a>
        <div class="file_button">
                <input type="file" name='FIZ_SCHET_INFO'>
        </div>     
</div>
<div class="horizont_line"></div>-->
<!--<div class="element_anketa">
        Карточка 51 счета за последний отчетный период  в разрезе операций.
        <div class="file_button">
                <input type="file" name='FIZ_51_CARD'>
        </div>     
</div>

<div class="horizont_line"></div>-->
<!--<div class="element_anketa">
        Отчет о дебиторской и кредиторской задолженности на последнюю отчетную дату по форме банка, образец стр. 7 
        <div class="file_button">
                <input type="file" name='FIZ_OTCHET_DEB'>
        </div>     
</div>



<div class="horizont_line"></div>-->
<!--<div class="element_anketa">
        Оборотно-сальдовые ведомости в разрезе операций за последний отчетный период следующих строк баланса: финансовые вложения (1170), запасы (1210), дебиторская задолженность (1230), финансовые вложения (1240),  долгосрочные займы (1410), краткосрочные займы (1510), кредиторская задолженность (1520)  и других строк баланса превышающих 10% от валюты баланса.
        <div class="file_button">
                <input type="file" name='FIZ_SALD'>
        </div>     
</div>


<div class="horizont_line"></div>-->
                                </div>
                                <div class="error-text">

                                </div>
                                <div class="element_anketa">
                                        <button name='STATUS' onclick="$('#operation').attr('data','save')" value='15'>Сохранить в черновик</button> <button onclick="$('#operation').attr('data','send')" name='STATUS' value='16' class="real">Подготовить к подписи</button>
										<!--<a href="#" class="for_signature">Подготовить к подписи</a>-->
                                </div>  
                        </form>
                </div>








<?if(isset($_GET["finish"])){?>
<div id="popup_finish">
</br></br></br>
<center style="color: green">
Заявка №<?=$_GET["id_zayvki"]?> сформирована. Ссылка на страницу подписи указана в конце заявки.
</center>
</div>
<?}?>
 
<div id="popup">    
<a class="btn btn-default btn-sm btn-close" href="#">Отменить</a>
<a class="btn btn-primary btn-sm save" href="#">Сохранить</a>
</br> </br>
<table class="table table-condensed" id="table" data-bind="with:models">
                    <thead>
                    <tr class="tableFloatingHeader" style="position: fixed; top: -2px; margin-left: 301px; margin-top: 55px; background-color: rgb(255, 255, 255); visibility: hidden; left: 0px; width: 1261px;">
                        <th style="font-size: 12px; width: 518px;">Значения измеряются в тысячах</th>
                        <th style="width: 47px;">Код</th>
                        <!-- ko foreach: Headers-->
                        <th style="font-size: 12px; class="input-th" data-bind="text: $data" style="width: 139px;">Год (2015)</th>
                        
                        <th style="font-size: 12px; class="input-th" data-bind="text: $data" style="width: 139px;">Квартал 1 (2016)</th>
                        
                        <th style="font-size: 12px; class="input-th" data-bind="text: $data" style="width: 139px;">Квартал 2 (2016)</th>
                        
                        <th style="font-size: 12px; class="input-th" data-bind="text: $data" style="width: 139px;">Квартал 3 (2016)</th>
                        
                        <th style="font-size: 12px; class="input-th" data-bind="text: $data" style="width: 139px;">Год (2016)</th>
                        <!-- /ko -->
                    </tr><tr class="tableFloatingHeaderOriginal">
                        <th style="font-size: 12px">Значения измеряются в тысячах </th>
                        <th style="font-size: 12px">Код</th>
                        <!-- ko foreach: Headers-->
                        <th style="font-size: 12px; class="input-th" data-bind="text: $data">Год (2015)</th>
                        
                        <th style="font-size: 12px; class="input-th" data-bind="text: $data">Квартал 1 (2016)</th>
                        
                        <th style="font-size: 12px; class="input-th" data-bind="text: $data">Квартал 2 (2016)</th>
                        
                        <th style="font-size: 12px; class="input-th" data-bind="text: $data">Квартал 3 (2016)</th>
                        
                        <th style="font-size: 12px; class="input-th" data-bind="text: $data">Год (2016)</th>
                        <!-- /ko -->
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: ItemsByCategory">

                    <tr>
                        <td colspan="7" style="text-align: center">
                            <h5 data-bind="text: Title" style="padding-top: 13px;">РАЗДЕЛ I. ВНЕОБОРОТНЫЕ АКТИВЫ</h5>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
                    <tr class="nemat">
                        <td style="font-size: 14px;">Нематериальные активы</td>
                        <td>
                            <span style="font-size: 14px; data-bind="text: Code">1110</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input   class="currency valid vneob_1" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_1" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency vneob_2" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_2" type="text">
                        </td>
                        
                        <td>
                            <input class="currency vneob_3" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_3" type="text">
                        </td>
                        
                        <td>
                            <input class="currency vneob_4" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_4" type="text">
                        </td>
                        
                        <td>
                            <input class="currency vneob_5" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_5" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="res">
                         <td style="font-size: 14px;">Результаты исследований и разработок</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1120</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_6" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_7" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_8" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_9" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_10" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="nem">
                         <td style="font-size: 14px;">Нематериальные поисковые активы</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1130</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_11" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_12" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_13" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_14" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_15" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="mat">
                         <td style="font-size: 14px;">Материальные поисковые активы</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1140</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_16" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_17" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_18" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_19" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_20" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="osn">
                         <td style="font-size: 14px;">Основные средства</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1150</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_21" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_22" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_23" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_24" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_25" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="doh">
                         <td style="font-size: 14px;">Доходные вложения в материальные ценности</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1160</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_26" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_27" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_28" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_29" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_30" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="fin">
                         <td style="font-size: 14px;">Финансовые вложения</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1170</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_31" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_32" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_33" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_34" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_35" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="otl">
                         <td style="font-size: 14px;">Отложенные налоговые активы</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1180</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_36" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_37" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_38" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_39" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_40" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="proch">
                         <td style="font-size: 14px;">Прочие внеоборотные активы</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1190</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_41" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_42" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_43" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_44" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_45" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog">
                         <td style="font-size: 14px;">ИТОГО по разделу I</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1100</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_46" readonly="readonly" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_47" readonly="readonly" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_48" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_49" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_50" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
                    
                    <tr>
                        
						<td colspan="7" style="text-align: center">
                            <h5 data-bind="text: Title" style="padding-top: 13px;">РАЗДЕЛ II. ОБОРОТНЫЕ АКТИВЫ</h5>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
                    <tr class="zap">
                         <td style="font-size: 14px;">Запасы</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1210</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_51" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_52" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_53" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_54" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_55" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="nalog">
                         <td style="font-size: 14px;">Налог на добавленную стоимость по приобретённым ценностям</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1220</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_56" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_57" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_58" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_59" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_60" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr  class="deb">
                         <td style="font-size: 14px;">Дебиторская задолженность</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1230</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_61" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_62" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_63" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_64" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_65" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="vtch">
                         <td style="font-size: 14px;">в т.ч. просроченная</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1231</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_66" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_67" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_68" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_69" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_70" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr  class="finvl">
                         <td style="font-size: 14px;">Финансовые вложения (за исключением денежных эквивалентов)</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1240</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_71" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_72" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_73" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_74" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_75" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="deneg">
                         <td style="font-size: 14px;">Денежные средства и денежные эквиваленты</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1250</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_76" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_77" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_78" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_79" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_80" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr  class="proch">
                         <td style="font-size: 14px;">Прочие оборотные активы</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1260</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_81" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_82" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_83" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_84" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_85" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr  class="itog2">
                         <td style="font-size: 14px;">ИТОГО по разделу II</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1200</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_86" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_87" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_88" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_89" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_90" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr  class="bal">
                         <td style="font-size: 14px;">БАЛАНС</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1600</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_91" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_92" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_93" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_94" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_95" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
                    
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <h5 data-bind="text: Title" style="padding-top: 13px;">РАЗДЕЛ III. КАПИТАЛ И РЕЗЕРВЫ</h4>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
                    <tr class="ustkap">
                         <td style="font-size: 14px;">Уставный капитал (складочный капитал, уставный фонд, вклады товарищей)</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1310</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_96" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_97" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_98" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_99" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_100" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="sobakz">
                         <td style="font-size: 14px;">Собственные акции, выкупленные у акционеров</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1320</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_101" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_102" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_103" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_104" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_105" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="pervne">
                         <td style="font-size: 14px;">Переоценка внеоборотных активов</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1340</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_106" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_107" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_108" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_109" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_110" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="dobkap">
                         <td style="font-size: 14px;">Добавочный капитал (без переоценки)</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1350</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_111" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_112" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_113" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_114" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_115" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="rezkap">
                         <td style="font-size: 14px;">Резервный капитал</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1360</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_116" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_117" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_118" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_119" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_120" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="nerpri">
                         <td style="font-size: 14px;">Нераспределенная прибыль (непокрытый убыток)</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1370</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_121" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_122" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_123" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_124" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_125" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog3">
                         <td style="font-size: 14px;">ИТОГО по разделу III</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1300</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_126" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_127" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_128" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_129" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_130" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
                    
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <h5 data-bind="text: Title" style="padding-top: 13px;">РАЗДЕЛ IV. ДОЛГОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА</h4>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
                    <tr class="zaesred">
                         <td style="font-size: 14px;">Заемные средства</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1410</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_131" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_132" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_133" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_134" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_135" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="otlnal4">
                         <td style="font-size: 14px;">Отложенные налоговые обязательства</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1420</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_136" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_137" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_138" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_139" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_140" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="ocenoby4">
                         <td style="font-size: 14px;">Оценочные обязательства</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1430</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_141" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_142" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_143" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_144" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_145" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prooby4">
                         <td style="font-size: 14px;">Прочие обязательства</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1450</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_146" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_147" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_148" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_149" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_150" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog4">
                         <td style="font-size: 14px;">ИТОГО по разделу IV</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1400</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_151" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_152" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_153" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_154" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_155" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
                    
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <h5 data-bind="text: Title" style="padding-top: 13px;">РАЗДЕЛ V. КРАТКОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА</h4>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
                    <tr class="zaesred5">
                         <td style="font-size: 14px;">Заемные средства</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1510</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_156" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_157" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_158" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_159" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_160" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="kredzad">
                         <td style="font-size: 14px;">Кредиторская задолженность</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1520</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_161" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_162" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_163" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_164" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_165" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="vtchpros5">
                         <td style="font-size: 14px;">в т.ч. Просроченная</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1521</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_166" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_167" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_168" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_169" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_170" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="dohbud">
                         <td style="font-size: 14px;">Доходы будущих периодов</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1530</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_171" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_172" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_173" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_174" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_175" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="oceoby">
                         <td style="font-size: 14px;">Оценочные обязательства</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1540</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_176" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_177" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_178" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_179" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_180" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prochoby5">
                         <td style="font-size: 14px;">Прочие обязательства</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1550</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_181" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_182" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_183" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_184" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_185" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog5">
                         <td style="font-size: 14px;">ИТОГО по разделу V</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1500</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_186" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_187" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_188" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_189" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_190" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="bal2">
                         <td style="font-size: 14px;">БАЛАНС</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">1700</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_191" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_192" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_193" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_194" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_195" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    <!-- /ko -->
    







	
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <h5 data-bind="text: Title" style="padding-top: 13px;">ОТЧЕТ О ФИНАНСОВЫХ РЕЗУЛЬТАТАХ</h4>
                        </td>
                    </tr>
                    <!-- ko foreach: Items-->
					
                    <tr class="vyr6">
                         <td style="font-size: 14px;">Выручка</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2110</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_196" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_197" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_198" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_199" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_200" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="sebprod6">
                         <td style="font-size: 14px;">Себестоимость продаж</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2120</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_201" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_202" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_203" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_204" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_205" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog61">
                         <td style="font-size: 14px;">Валовая прибыль (убыток)</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2100</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_206" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_207" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_208" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_209" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_210" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="komras6">
                         <td style="font-size: 14px;">Коммерческие расходы</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2210</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_211" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_212" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_213" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_214" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_215" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="uprras6">
                         <td style="font-size: 14px;">Управленческие расходы</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2220</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_216" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_217" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_218" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_219" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_220" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog62">
                         <td style="font-size: 14px;">Прибыль (убыток) от продаж</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2200</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_221" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_222" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_223" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_224" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_225" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="dohot6">
                         <td style="font-size: 14px;">Доходы от участия в других организациях</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2310</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_226" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_227" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_228" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_229" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_230" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prockpol6">
                         <td style="font-size: 14px;">Проценты к получению</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2320</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_231" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_232" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_233" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_234" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_235" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prockupl6">
                         <td style="font-size: 14px;">Проценты к уплате</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2330</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_236" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_237" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_238" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_239" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_240" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prochdoh6">
                         <td style="font-size: 14px;">Прочие доходы</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2340</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_241" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_242" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_243" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_244" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_245" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prochras6">
                         <td style="font-size: 14px;">Прочие расходы</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2350</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_246" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_247" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_248" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_249" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_250" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="itog63">
                         <td style="font-size: 14px;">Прибыль (убыток) до налогообложения</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2300</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_251" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_252" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_253" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_254" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_255" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="teknal6">
                         <td style="font-size: 14px;">Текущий налог на прибыль</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2410</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_256" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_257" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_258" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_259" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_260" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="izmotobl6">
                         <td style="font-size: 14px;">Изменение отложенных налоговых обязательств</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2430</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_261" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_262" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_263" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_264" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_265" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="izmotlart6">
                         <td style="font-size: 14px;">Изменение отложенных налоговых активов</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2450</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_266" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_267" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_268" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_269" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_270" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>
                    
                    <tr class="prochee6">
                         <td style="font-size: 14px;">Прочее</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2460</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_271" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_272" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_273" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_274" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_275" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>               
                    <tr class="itog64">
                         <td style="font-size: 14px;">Чистая прибыль (убыток)</td>
                        <td>
                            
                           <span style="font-size: 14px; data-bind="text: Code">2400</span>
                        </td>
                        <!-- ko foreach: Values-->
                        <td>
                            <input class="currency valid" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_276" readonly="readonly" data-original-title="" title="" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_277" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_278" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_279" readonly="readonly" type="text">
                        </td>
                        
                        <td>
                            <input class="currency" data-bind="numeric: Value, uniqueName: true, attr: { 'readonly' : $root.readonly || $data.readonly }" placeholder="0" data-val="true" data-val-regex="Значение должно быть числовым." data-val-regex-pattern="^[0-9 ]*$" name="ko_unique_280" readonly="readonly" type="text">
                        </td>
                        <!-- /ko -->
                    </tr>

                    </tbody>
                </table>
</iframe>  
</div> 
<div id="hide-layout" class="hide-layout"></div>
<?if(isset($_GET["finish"])){?>
<div id="hide-layout-finish" class="hide-layout-finish"></div>		
<?}}?>			
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>