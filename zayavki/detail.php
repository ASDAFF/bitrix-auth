<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявка");
?><?
if(in_array(6, CUser::GetUserGroup($USER->GetID()))){
    $arrFilter['!PROPERTY_STATUS'] = '15';
}else{
    $arrFilter['MODIFIED_BY'] = $USER->GetID();
}
$arrFilter['PROPERTY_STATUS'] = 'A';

//echo "<pre style=\"margin-top:115px\">"; print_r($_REQUEST); echo "</pre>";

if(!empty($_REQUEST["strIMessage"])) {
	LocalRedirect(SITE_DIR."zayavki/detail.php?id=".$_REQUEST["id"]."&edit=Y&CODE=".$_REQUEST["id"]."&UPDT=updated");
}

$APPLICATION->IncludeComponent(
	"custom:news.detail", 
	"zayavka", 
	array(
		"IBLOCK_TYPE" => "zayavki",
		"IBLOCK_ID" => "",
		"ELEMENT_ID" => $_REQUEST["id"],
		"ELEMENT_CODE" => "",
		"CHECK_DATES" => "N",
		"FIELD_CODE" => array(
			0 => "DATE_CREATE",
			1 => "undefined",
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "AE",
			2 => "",
		),
		"IBLOCK_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "N",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "N",
		"META_DESCRIPTION" => "-",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_ELEMENT_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"USE_PERMISSIONS" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>