<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои заявки");
global $arrFilter, $USER;

if(empty($_SESSION['COUNT'])){
        $_SESSION['COUNT'] = 50;
}
if(in_array(6, CUser::GetUserGroup($USER->GetID()))){
        $arrFilter['!PROPERTY_STATUS'] = '15';
        $shabl = 'all_zayavki_bank';
}else{
        $arrFilter['CREATED_BY'] = $USER->GetID();
        $shabl = 'all_zayavki';
}

if($USER->GetID() == 1){
        unset($arrFilter['CREATED_BY']);
}
if(!empty($_SESSION['STATUS'])){
        $arrFilter['PROPERTY_STATUS'] = $_SESSION['STATUS'];
}

if(!empty($_SESSION['ID_ELEMENT'])){
        $arrFilter['ID'] = $_SESSION['ID_ELEMENT'];
}

if(!empty($_SESSION['INN'])){
        $arrFilter['PROPERTY_M'] = $_SESSION['INN'];
}
if(!empty($_SESSION['DATA_FROM'])){
        $arrFilter['>=DATE_CREATE'] = $_SESSION['DATA_FROM'];
}

if(!empty($_SESSION['DATA_FROM'])){
        $arrFilter['<=DATE_CREATE'] = $_SESSION['DATA_TO'];
}
if(empty($_SESSION['SORT'])){
        $_SESSION['SORT'] = 'ACTIVE_FROM';
}
//preprint($arrFilter);
//echo $shabl;
//echo "<pre style=\"margin-top:150px\">"; print_r($_SESSION); echo "</pre>";
?>
<script language="javascript" src="cades.js"></script>
<script>    
	$(document).ready(function() {
				var canAsync = !!cadesplugin.CreateObjectAsync;
				if(canAsync)
				{
					cadesplugin.async_spawn(function *(args) {
						var oSignedData = yield cadesplugin.CreateObjectAsync("CAdESCOM.CadesSignedData");
						var dataToSign = "dataToSign";
						yield oSignedData.propset_Content(dataToSign);
					});
				}else
				{
					var oSignedData = cadesplugin.CreateObject("CAdESCOM.CadesSignedData");
					var dataToSign = "dataToSign";
					oSignedData.Content = dataToSign;
				}
		});
		
		
</script>


<input type="hidden" name="STATUS" value="<?=$_SESSION['STATUS']?>">
<input type="hidden" name="ID_ELEMENT" value="<?=$_SESSION['ID_ELEMENT']?>">
<input type="hidden" name="INN" value="<?=$_SESSION['INN']?>">
<input type="hidden" name="DATA_FROM" value="<?=$_SESSION['DATA_FROM']?>">
<input type="hidden" name="DATA_TO" value="<?=$_SESSION['DATA_TO']?>">
<input type="hidden" name="SORT" value="<?=$_SESSION['SORT']?>">
<input type="hidden" name="OR" value="<?=$_SESSION['OR']?>">
      
        <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	$shabl, 
	array(
		"IBLOCK_TYPE" => "zayavki",
		"IBLOCK_ID" => $_REQUEST["ID"],
		"NEWS_COUNT" => $_SESSION["COUNT"],
		"SORT_BY1" => $_SESSION["SORT"],
		"SORT_ORDER1" => $_SESSION["OR"],
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(
			0 => "DATE_CREATE",
			1 => "CREATED_BY",
			2 => "CREATED_USER_NAME",
			3 => "MODIFIED_BY",
			4 => "USER_NAME",
			5 => "undefined",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "undefined",
			2 => "",
			3 => "PARTNER_PR_20"
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
    
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>