<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Подпись документа");
?>
    <script src="q.min.js"></script>

<?$file = file($_SERVER['DOCUMENT_ROOT']."/archives/".$_GET["inn"]."/".$_GET["id"].".zip");
$str = implode('', $file); 
//print_r($str);
echo $base64_file = base64_encode($str);
echo "</br>";
?>

    <script type="text/javascript">
        var System;
        (function (System) {
            ;
        })(System || (System = {}));

        /// <reference path="system/ipromise.ts" />

        var Cryptography;
        (function (Cryptography) {
            var Error;
            (function (Error) {
                var ErrorResolver = (function () {
                    function ErrorResolver() {
                    }
                    ;
                    ErrorResolver.prototype.resolve = function (ex) {
                        var original = ex;
                        var result = {
                            message: ''
                        };
                        if (typeof (ex) === 'string')
                            ex = { message: ex };
                        else if (typeof (ex) === 'object') {
                            if (!ex.number) {
                                var errGroup = /\((0[xX][0-9a-fA-F]+)\)/gmi.exec(ex.message);
                                ex.number = errGroup ? errGroup[1] : null;
                                if (ex.number)
                                    ex.number = parseInt(ex.number, 16);
                            }
                        }
                        ;
                        switch (ex.number) {
                            case 0x80070005:
                                result.message = 'Отказано в доступе';
                                result.number = 0x80070005;
                                break;
                            case 0x8009000A:
                            case -2146893814:
                                result.message = 'Указан неправильный тип. Удалите запомненые пароли и попробуйте еще раз.';
                                result.number = 0x8009000A;
                                break;
                            case 0x8009200B:
                            case -2146885621:
                                //По сути, просто нет закрытого ключа
                                result.message = 'Не удается найти сертификат и закрытый ключ для расшифровки.';
                                result.number = 0x8009200B;
                                break;
                            case 0x800B0109:
                                result.message = 'Цепочка сертификатов обработана, но завершилась на корневом сертификате, который не является доверенным на Вашем рабочем месте. Установите корневой сертификат Удостоверяющего центра';
                                result.number = 0x800B0109;
                                break;
                            case 0x800B010A:
                                result.message = 'Невозможно построить цепочку сертификата, которым Вы осуществляете подпись. Установите корневой сертификат Удостоверяющего центра';
                                result.number = 0x800B010A;
                                break;
                            case 0x8010006E:
                            case -2146434962:
                                result.message = 'Отменено пользователем';
                                result.number = 0x8010006E;
                                break;
                            case 0x80092004:
                            case -2146885628:
                                result.message = 'Не удалось найти закрытый ключ для данного сертификата';
                                result.number = 0x80092004;
                                break;
                            case 0x8009001F:
                                result.message = 'Неправильный параметр набора ключей. Возможно отсутствует привязка закрытого ключа к сертификату';
                                result.number = 0x8009001F;
                                break;
                            case 0x800b0101:
                            case -2146762495:
                                result.message = 'Время создания сертификата не совпадает со временем на Вашем рабочем месте. Скачайте сертификат и установите его вручную.';
                                result.number = 0x800b0101;
                                break;
                            case 0x8010006b:
                            case -2146434965:
                                result.message = 'Нет доступа к карте или к носителю. Введен неправильный пароль (PIN-код)';
                                result.number = 0x8010006b;
                                break;
                            case 0x80090023:
                            case -2146893789:
                                result.message = 'Токен безопасности не имеет доступного места для хранения дополнительного контейнера.';
                                result.number = 0x80090023;
                                break;
                            case 0x80090020:
                            case -2146893792:
                                result.message = 'Внутренняя ошибка криптопровайдера';
                                result.number = 0x80090020;
                                break;
                            case 0x80040154:
                                result.message = 'На рабочем месте не установлен  xenroll.dll';
                                result.number = 0x80040154;
                                break;
                            case 0x80090016:
                                result.message = 'Не удалось найти закрытый ключ или нет доступа к закрытому ключу';
                                result.number = 0x80090016;
                            default:
                                result.message = ex.message;
                                result.number = ex.number;
                        }
                        ;
                        result.original = original;
                        return result;
                    };
                    ;
                    return ErrorResolver;
                })();
                Error.ErrorResolver = ErrorResolver;
                ;
            })(Error = Cryptography.Error || (Cryptography.Error = {}));
        })(Cryptography || (Cryptography = {}));
        ;

        var Cryptography;
        (function (Cryptography) {
            var Error;
            (function (Error) {
                ;
                ;
            })(Error = Cryptography.Error || (Cryptography.Error = {}));
        })(Cryptography || (Cryptography = {}));
        ;

        var Cryptography;
        (function (Cryptography) {
            var CryptoPro;
            (function (CryptoPro) {
                (function (CadespluginObject) {
                    CadespluginObject[CadespluginObject["About"] = 'CAdESCOM.About'] = "About";
                    CadespluginObject[CadespluginObject["Store"] = 'CAdESCOM.store'] = "Store";
                    CadespluginObject[CadespluginObject["RequestPKCS10"] = 'X509Enrollment.CX509CertificateRequestPkcs10'] = "RequestPKCS10";
                    CadespluginObject[CadespluginObject["Enrollment"] = 'X509Enrollment.CX509Enrollment'] = "Enrollment";
                    CadespluginObject[CadespluginObject["EnrollmentPrivateKey"] = 'X509Enrollment.CX509PrivateKey'] = "EnrollmentPrivateKey";
                    CadespluginObject[CadespluginObject["EnrollmentEKU"] = 'X509Enrollment.CX509ExtensionKeyUsage'] = "EnrollmentEKU";
                    CadespluginObject[CadespluginObject["EnrollmentSubject"] = 'X509Enrollment.CX500DistinguishedName'] = "EnrollmentSubject";
                    CadespluginObject[CadespluginObject["EnrollmentObjects"] = 'X509Enrollment.CObjectIds'] = "EnrollmentObjects";
                    CadespluginObject[CadespluginObject["EnrollmentObject"] = 'X509Enrollment.CObjectId'] = "EnrollmentObject";
                    CadespluginObject[CadespluginObject["EnrollmentEnhansed"] = 'X509Enrollment.CX509ExtensionEnhancedKeyUsage'] = "EnrollmentEnhansed";
                    CadespluginObject[CadespluginObject["EnrollTemplate"] = 'X509Enrollment.CX509ExtensionTemplateName'] = "EnrollTemplate";
                })(CryptoPro.CadespluginObject || (CryptoPro.CadespluginObject = {}));
                var CadespluginObject = CryptoPro.CadespluginObject;
                var Cadesplugin = (function () {
                    function Cadesplugin(q) {
                        this.q = q;
                        this.name = 'cadesplugin';
                        this.hiddenStyle = 'width:0;height:0;visiblity=hidden';
                        this.ErrorResolver = new Cryptography.Error.ErrorResolver();
                        this.IsLoaded = false;
                        if (!q)
                            throw { message: 'Argument "q" is empty' };
                        this.Navigator = new System.Browser.Utils.Navigator(window.navigator);
                        this.PluginSupport = this.checSupportedPluginType(this.Navigator);
                        this.IsAsync = this.PluginSupport.isAsync;
                        this.initCadespluginObject();
                    }
                    ;
                    Cadesplugin.prototype.Load = function (milliseconds) {
                        if (milliseconds === void 0) { milliseconds = 0; }
                        var def = this.q.defer();
                        var self = this;
                        if (!this.IsLoaded) {
                            if (self.PluginSupport.nativeBridge) {
                                try {
                                    this.iosBridge = new CryptoPro.IOSnativeBridge();
                                    this.IsLoaded = true;
                                    def.resolve(self);
                                }
                                catch (ex) {
                                    def.reject(ex);
                                }
                                ;
                            }
                            else if (self.PluginSupport.ppapi) {
                                try {
                                    if (self.Navigator.Name == 'Chrome' || self.Navigator.Name == 'Opera' || self.Navigator.Name == 'YaBrowser') {
                                        if (self.Navigator.Name == 'Chrome')
                                            self.load_chrome_extension();
                                        else if (self.Navigator.Name == 'Opera')
                                            self.load_opera_extension();
                                        else if (self.Navigator.Name == 'YaBrowser')
                                            self.load_yandex_extension();
                                        window.postMessage("cadesplugin_echo_request", "*");
                                        window.addEventListener("message", function (event) {
                                            self.listener = this;
                                            if (event.data != "cadesplugin_loaded")
                                                return;
                                            else {
                                                //давай дадим немного времени на раздумья этому медленному браузеру
                                                setTimeout(function () {
                                                    self.cpcsp_chrome_nmcades = window.cpcsp_chrome_nmcades;
                                                    if (self.cpcsp_chrome_nmcades) {
                                                        self.cpcsp_chrome_nmcades.CreatePluginObject().then(function (objInstance) {
                                                            self.pluginObject = objInstance;
                                                            self.IsLoaded = true;
                                                            def.resolve(self);
                                                        }, function (ex) { def.reject(ex); });
                                                    }
                                                    else {
                                                        if (self.listener)
                                                            window.removeEventListener('message', self.listener);
                                                        self.pluginObject = null;
                                                        self.IsLoaded = false;
                                                        def.reject({ message: 'Cadesplugin not loaded' });
                                                    }
                                                }, milliseconds);
                                            }
                                            ;
                                        }, false);
                                    }
                                    else
                                        def.reject({ message: 'Not supported browser' });
                                }
                                catch (ex) {
                                    def.reject(ex);
                                }
                                ;
                            }
                            else {
                                try {
                                    self.load_npapi_plugin();
                                    self.IsLoaded = true;
                                    def.resolve(self);
                                }
                                catch (ex) {
                                    def.reject(ex);
                                }
                                ;
                            }
                            ;
                        }
                        else {
                            def.resolve(self);
                        }
                        ;
                        return def.promise;
                    };
                    ;
                    Cadesplugin.prototype.OnMessage = function (event) {
                    };
                    Cadesplugin.prototype.CreateObject = function (name) {
                        var self = this;
                        var def = self.q.defer();
                        if (this.PluginSupport.nativeBridge) {
                            try {
                                var a = self.call_ru_cryptopro_npcades_10_native_bridge("CreateObject", [name]);
                                def.resolve(a);
                            }
                            catch (ex) {
                                def.reject(ex);
                            }
                        }
                        else if (this.Navigator.Name == "MSIE" && this.PluginSupport.npapi) {
                            try {
                                def.resolve(new ActiveXObject(name));
                            }
                            catch (ex) {
                                def.reject(ex);
                            }
                            ;
                        }
                        else if (this.PluginSupport.ppapi) {
                            if (this.Navigator.Name == 'Chrome' || this.Navigator.Name == 'Opera' || this.Navigator.Name == 'YaBrowser') {
                                try {
                                    if (self.cpcsp_chrome_nmcades) {
                                        if (self.pluginObject) {
                                            self.pluginObject.CreateObjectAsync(name).then(function (obj) { return def.resolve(obj); }, function (ex) { return def.reject(ex); });
                                        }
                                        else
                                            def.reject({ message: 'Не удалось создать объект cpcsp_chrome_nmcades' });
                                    }
                                    else
                                        def.reject({ message: 'Объект cpcsp_chrome_nmcades отсутсвует. Не удалось обратиться к криптографии' });
                                }
                                catch (e) {
                                    def.reject(e);
                                }
                                ;
                            }
                            else
                                def.reject({ message: 'Not supported browser' });
                        }
                        else if (this.PluginSupport.npapi) {
                            try {
                                var a = self.pluginObject.CreateObject(name);
                                def.resolve(a);
                            }
                            catch (ex) {
                                def.reject(ex);
                            }
                        }
                        return def.promise;
                    };
                    ;
                    Cadesplugin.prototype.About = function () {
                        var self = this;
                        return Cryptography.CryptoPro.CadespluginAbout.Create(self);
                    };
                    ;
                    /*Поддерживаемый функционал*/
                    Cadesplugin.prototype.Capabilities = function (milliseconds) {
                        if (milliseconds === void 0) { milliseconds = 500; }
                        var self = this;
                        var def = self.q.defer();
                        var result = {
                            certificateCreating: false,
                            certificateInstalling: false,
                            activex: typeof (window.ActiveXObject) === 'undefined' ? false : true
                        };
                        self.Load(milliseconds).then(function (x) {
                            self.GetPluginState(milliseconds).then(function (state) {
                                result.state = state;
                                if (state.isInstall && !state.isLocked) {
                                    self.About().then(function (about) {
                                        result.plugin = about.PluginVersion;
                                        var p1 = about.CSPVersion().then(function (csp) {
                                            result.csp = csp;
                                            result.certificateInstalling = !(csp.MajorVersion == 3 && csp.MinorVersion == 0) || (csp.MajorVersion < 3);
                                            result.certificateInstalling = true;
                                        }, def.resolve(result));
                                        p1['finally'](function (x) { return def.resolve(result); });
                                    }, def.resolve(result));
                                }
                                else
                                    def.resolve(result);
                            }, def.resolve(result));
                        }, def.resolve(result));
                        return def.promise;
                    };
                    Cadesplugin.prototype.GetName = function () {
                        return this.name;
                    };
                    Cadesplugin.prototype.GetPluginState = function (milliseconds) {
                        if (milliseconds === void 0) { milliseconds = 1000; }
                        var self = this;
                        var result;
                        result = { isInstall: false, isLocked: false };
                        var def = self.q.defer();
                        if (self.PluginSupport.ppapi) {
                            if (self.Navigator.Name == 'Chrome' || self.Navigator.Name == 'Opera' || self.Navigator.Name == 'YaBrowser') {
                                var xmlHttp = new XMLHttpRequest();
                                var testUrl;
                                if (self.Navigator.Name == 'Chrome')
                                    testUrl = 'chrome-extension://iifchhfnnmpdbibifmljnfjhpififfog/nmcades_plugin_api.js';
                                else if (self.Navigator.Name == 'Opera')
                                    testUrl = 'chrome-extension://epebfcehmdedogndhlcacafjaacknbcm/nmcades_plugin_api.js';
                                else if (self.Navigator.Name == 'YaBrowser')
                                    testUrl = 'chrome-extension://epebfcehmdedogndhlcacafjaacknbcm/nmcades_plugin_api.js';
                                xmlHttp.open('HEAD', testUrl, true);
                                xmlHttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                                xmlHttp.timeout = milliseconds;
                                xmlHttp.onreadystatechange = function () {
                                    if (xmlHttp.readyState != 4)
                                        return;
                                    if (xmlHttp.status == 200) {
                                        result.isInstall = true;
                                        result.isLocked = false;
                                        def.resolve(result);
                                    }
                                    else {
                                        def.resolve(result);
                                    }
                                };
                                xmlHttp.ontimeout = function () {
                                    def.resolve(result);
                                };
                                xmlHttp.send();
                            }
                            else
                                def.reject({ message: 'Don\' supported browser' });
                        }
                        else if (self.isIE() || self.isIOS()) {
                            self.About().then(function () {
                                result.isInstall = true;
                                result.isLocked = false;
                                def.resolve(result);
                            }, function (ex) {
                                def.resolve(result);
                            });
                        }
                        else if (self.PluginSupport.npapi) {
                            var body = document.querySelector("body");
                            ;
                            var elem = document.createElement('object');
                            elem.setAttribute("id", "cadesplugin_object_test");
                            elem.setAttribute("type", "application/x-cades");
                            elem.setAttribute("style", self.hiddenStyle);
                            if (body)
                                body.appendChild(elem);
                            var a = document.querySelector("#cadesplugin_object_test");
                            if (a) {
                                try {
                                    a.CreateObject(CadespluginObject.About);
                                    result.isInstall = true;
                                    result.isLocked = false;
                                }
                                catch (ex) {
                                    var mimetype = navigator.mimeTypes["application/x-cades"];
                                    if (mimetype) {
                                        var plugin = mimetype.enabledPlugin;
                                        if (plugin) {
                                            result.isInstall = true;
                                            result.isLocked = true;
                                        }
                                    }
                                }
                                if (a.parent)
                                    a.parent.removeChild(a);
                            }
                            ;
                            def.resolve(result);
                        }
                        else
                            def.reject({ message: 'Unknown browser supported' });
                        return def.promise;
                    };
                    Cadesplugin.prototype.Reset = function () {
                        var self = this;
                        if (self.listener)
                            window.removeEventListener('message', self.listener);
                    };
                    ;
                    Cadesplugin.prototype.GetQService = function () {
                        return this.q;
                    };
                    ;
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    Cadesplugin.prototype.checSupportedPluginType = function (navigator) {
                        var result = {
                            isAsync: false
                        };
                        switch (navigator.Name) {
                            case 'Opera':
                                if (navigator.Version.Major > 34) {
                                    result.isAsync = true;
                                    result.ppapi = true;
                                }
                                else
                                    result.npapi = true;
                                break;
                            case 'YaBrowser':
                                if (navigator.Version.Major > 15) {
                                    result.isAsync = true;
                                    result.ppapi = true;
                                }
                                else
                                    result.npapi = true;
                                break;
                            case 'Chrome':
                                if (navigator.Version.Major > 44) {
                                    result.isAsync = true;
                                    result.ppapi = true;
                                }
                                else {
                                    result.npapi = true;
                                }
                                break;
                            case 'Safari':
                                result.nativeBridge = true;
                                break;
                            default:
                                result.npapi = true;
                                break;
                        }
                        return result;
                    };
                    Cadesplugin.prototype.isIE = function () {
                        var retVal = (("Microsoft Internet Explorer" == navigator.appName) ||
                            navigator.userAgent.match(/Trident\/./i)); // IE 11
                        return retVal;
                    };
                    Cadesplugin.prototype.isIOS = function () {
                        var retVal = (navigator.userAgent.match(/ipod/i) ||
                            navigator.userAgent.match(/ipad/i) ||
                            navigator.userAgent.match(/iphone/i));
                        return retVal;
                    };
                    Cadesplugin.prototype.isChromiumBased = function () {
                        var retVal = (navigator.userAgent.match(/chrome/i) ||
                            navigator.userAgent.match(/opera/i));
                        return retVal;
                    };
                    Cadesplugin.prototype.isMSEdge = function () {
                        return navigator.userAgent.match(/Edge\/./i);
                    };
                    ;
                    Cadesplugin.prototype.call_ru_cryptopro_npcades_10_native_bridge = function (functionName, array) {
                        var tmpobj;
                        var ex;
                        this.iosBridge.call(functionName, array, function (e, response) {
                            ex = e;
                            var str = 'tmpobj=' + response;
                            eval(str);
                            if (typeof (tmpobj) == "string") {
                                tmpobj = tmpobj.replace(/\\\n/gm, "\n");
                                tmpobj = tmpobj.replace(/\\\r/gm, "\r");
                            }
                        });
                        if (ex)
                            throw ex;
                        return tmpobj;
                    };
                    ;
                    Cadesplugin.prototype.load_chrome_extension = function () {
                        this.load_ppapi_extension('chrome-extension://iifchhfnnmpdbibifmljnfjhpififfog/nmcades_plugin_api.js');
                    };
                    ;
                    Cadesplugin.prototype.load_opera_extension = function () {
                        this.load_ppapi_extension('chrome-extension://epebfcehmdedogndhlcacafjaacknbcm/nmcades_plugin_api.js');
                    };
                    ;
                    Cadesplugin.prototype.load_yandex_extension = function () {
                        this.load_ppapi_extension('chrome-extension://epebfcehmdedogndhlcacafjaacknbcm/nmcades_plugin_api.js');
                    };
                    ;
                    Cadesplugin.prototype.load_ppapi_extension = function (src) {
                        var fileref = document.createElement('script');
                        fileref.setAttribute("type", "text/javascript");
                        fileref.setAttribute("src", src);
                        document.getElementsByTagName("head")[0].appendChild(fileref);
                    };
                    Cadesplugin.prototype.load_firefox_extension = function () {
                        throw { message: 'Not implemented' };
                    };
                    ;
                    Cadesplugin.prototype.load_npapi_plugin = function () {
                        var body = document.querySelector("body");
                        ;
                        var elem = document.createElement('object');
                        elem.setAttribute("id", "cadesplugin_object");
                        elem.setAttribute("type", "application/x-cades");
                        elem.setAttribute("style", this.hiddenStyle);
                        if (body)
                            body.appendChild(elem);
                        this.pluginObject = document.getElementById("cadesplugin_object");
                    };
                    ;
                    /**
                     * Никому не нужная хрень
                     */
                    Cadesplugin.prototype.initCadespluginObject = function () {
                        var cadesplugin = {
                            JSModuleVersion: '2.0.2',
                            CAPICOM_LOCAL_MACHINE_STORE: 1,
                            CAPICOM_CURRENT_USER_STORE: 2,
                            CAPICOM_MY_STORE: "My",
                            CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED: 2,
                            CADESCOM_XML_SIGNATURE_TYPE_ENVELOPED: 0,
                            CADESCOM_XML_SIGNATURE_TYPE_ENVELOPING: 1,
                            CADESCOM_XML_SIGNATURE_TYPE_TEMPLATE: 2,
                            XmlDsigGost3410UrlObsolete: "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411",
                            XmlDsigGost3411UrlObsolete: "http://www.w3.org/2001/04/xmldsig-more#gostr3411",
                            XmlDsigGost3410Url: "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34102001-gostr3411",
                            XmlDsigGost3411Url: "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr3411",
                            CADESCOM_CADES_DEFAULT: 0,
                            CADESCOM_CADES_BES: 1,
                            CADESCOM_CADES_T: 0x5,
                            CADESCOM_CADES_X_LONG_TYPE_1: 0x5d,
                            CADESCOM_ENCODE_BASE64: 0,
                            CADESCOM_ENCODE_BINARY: 1,
                            CADESCOM_ENCODE_ANY: -1,
                            CAPICOM_CERTIFICATE_INCLUDE_CHAIN_EXCEPT_ROOT: 0,
                            CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN: 1,
                            CAPICOM_CERTIFICATE_INCLUDE_END_ENTITY_ONLY: 2,
                            CAPICOM_CERT_INFO_SUBJECT_SIMPLE_NAME: 0,
                            CAPICOM_CERT_INFO_ISSUER_SIMPLE_NAME: 1,
                            CAPICOM_CERTIFICATE_FIND_SHA1_HASH: 0,
                            CAPICOM_CERTIFICATE_FIND_ISSUER_NAME: 2,
                            CAPICOM_CERTIFICATE_FIND_ROOT_NAME: 3,
                            CAPICOM_CERTIFICATE_FIND_TEMPLATE_NAME: 4,
                            CAPICOM_CERTIFICATE_FIND_EXTENSION: 5,
                            CAPICOM_CERTIFICATE_FIND_EXTENDED_PROPERTY: 6,
                            CAPICOM_CERTIFICATE_FIND_APPLICATION_POLICY: 7,
                            CAPICOM_CERTIFICATE_FIND_CERTIFICATE_POLICY: 8,
                            CAPICOM_CERTIFICATE_FIND_TIME_VALID: 9,
                            CAPICOM_CERTIFICATE_FIND_TIME_NOT_YET_VALID: 10,
                            CAPICOM_CERTIFICATE_FIND_TIME_EXPIRED: 11,
                            CAPICOM_CERTIFICATE_FIND_KEY_USAGE: 12,
                            CAPICOM_DIGITAL_SIGNATURE_KEY_USAGE: 128,
                            CAPICOM_PROPID_ENHKEY_USAGE: 9,
                            CAPICOM_OID_OTHER: 0,
                            CAPICOM_OID_KEY_USAGE_EXTENSION: 10,
                            CAPICOM_EKU_CLIENT_AUTH: 2,
                            CAPICOM_EKU_SMARTCARD_LOGON: 5,
                            CAPICOM_EKU_OTHER: 0,
                            CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME: 0,
                            CADESCOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME: 1,
                            CADESCOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_DESCRIPTION: 2,
                            CADESCOM_ATTRIBUTE_OTHER: -1,
                            CADESCOM_STRING_TO_UCS2LE: 0,
                            CADESCOM_BASE64_TO_BINARY: 1,
                            CADESCOM_DISPLAY_DATA_NONE: 0,
                            CADESCOM_DISPLAY_DATA_CONTENT: 1,
                            CADESCOM_DISPLAY_DATA_ATTRIBUTE: 2,
                            CADESCOM_ENCRYPTION_ALGORITHM_RC2: 0,
                            CADESCOM_ENCRYPTION_ALGORITHM_RC4: 1,
                            CADESCOM_ENCRYPTION_ALGORITHM_DES: 2,
                            CADESCOM_ENCRYPTION_ALGORITHM_3DES: 3,
                            CADESCOM_ENCRYPTION_ALGORITHM_AES: 4,
                            CADESCOM_ENCRYPTION_ALGORITHM_GOST_28147_89: 25,
                            CADESCOM_HASH_ALGORITHM_SHA1: 0,
                            CADESCOM_HASH_ALGORITHM_MD2: 1,
                            CADESCOM_HASH_ALGORITHM_MD4: 2,
                            CADESCOM_HASH_ALGORITHM_MD5: 3,
                            CADESCOM_HASH_ALGORITHM_SHA_256: 4,
                            CADESCOM_HASH_ALGORITHM_SHA_384: 5,
                            CADESCOM_HASH_ALGORITHM_SHA_512: 6,
                            CADESCOM_HASH_ALGORITHM_CP_GOST_3411: 100,
                            CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_256: 101,
                            CADESCOM_HASH_ALGORITHM_CP_GOST_3411_2012_512: 102,
                            LOG_LEVEL_DEBUG: 4,
                            LOG_LEVEL_INFO: 2,
                            LOG_LEVEL_ERROR: 1,
                        };
                        window.cadesplugin = cadesplugin;
                    };
                    return Cadesplugin;
                })();
                CryptoPro.Cadesplugin = Cadesplugin;
            })(CryptoPro = Cryptography.CryptoPro || (Cryptography.CryptoPro = {}));
        })(Cryptography || (Cryptography = {}));

        var Cryptography;
        (function (Cryptography) {
            var CryptoPro;
            (function (CryptoPro) {
                ;
                var CadespluginAbout = (function () {
                    function CadespluginAbout(plugin, native) {
                        this.plugin = plugin;
                        this._native = native;
                    }
                    ;
                    /**
                     * Возвращает лицензию CSP
                     */
                    CadespluginAbout.prototype.CSPLicense = function () {
                        var self = this;
                        var cspGuid = '{407E5BA7-6406-40BF-A4DC-3654B8F584C1}'; //4.0
                        var cspLicense = 'CPCSPLicense.CPCSPLicense';
                        var def = self.plugin.GetQService().defer();
                        if (self.plugin.IsAsync) {
                            self.plugin.CreateObject(cspLicense).then(function (x) {
                                x.GetExpireDate(cspGuid).then(function (date) {
                                    def.resolve(date);
                                }).catch(function (ex) {
                                    def.reject(ex);
                                });
                            }).catch(function (ex) {
                                def.reject(ex);
                            });
                        }
                        else {
                            try {
                                self.plugin.CreateObject(cspLicense).then(function (x) {
                                    var date = x.GetExpireDate(cspGuid);
                                    def.resolve(date);
                                });
                            }
                            catch (ex) {
                                def.reject(ex);
                            }
                        }
                        return def.promise;
                    };
                    ;
                    /*Возвращает версию криптопровайдера в виде набора цифр*/
                    CadespluginAbout.prototype.CSPVersion = function (providerName, providerType) {
                        if (providerName === void 0) { providerName = ''; }
                        if (providerType === void 0) { providerType = 75; }
                        var self = this;
                        var def = self.plugin.GetQService().defer();
                        if (self.plugin.IsAsync) {
                            self._native.CSPVersion(providerName, providerType)
                                .then(function (version) {
                                    var Version = {};
                                    var promises = [];
                                    promises.push(version.MajorVersion.then(function (z) { return Version.MajorVersion = z; }));
                                    promises.push(version.MinorVersion.then(function (z) { return Version.MinorVersion = z; }));
                                    promises.push(version.BuildVersion.then(function (z) { return Version.BuildVersion = z; }));
                                    self.plugin.GetQService().all(promises)['finally'](function (z) {
                                        Version.toStringDefault = Version.MajorVersion + '.' + Version.MinorVersion + '.' + Version.BuildVersion;
                                        def.resolve(Version);
                                    });
                                }, def.reject);
                        }
                        else {
                            try {
                                var a = self._native.CSPVersion(providerName, providerType);
                                var Version = {
                                    MajorVersion: a.MajorVersion,
                                    MinorVersion: a.MinorVersion,
                                    BuildVersion: a.BuikdVersion,
                                    toStringDefault: a.MajorVersion + "." + a.MinorVersion + "." + a.BuildVersion
                                };
                                def.resolve(Version);
                            }
                            catch (ex) {
                                def.reject(ex);
                            }
                            ;
                        }
                        return def.promise;
                    };
                    /*Возвращает версию криптопровайдера в виде строки*/
                    CadespluginAbout.prototype.ProviderVersion = function (providerName, providerType) {
                        if (providerName === void 0) { providerName = ''; }
                        if (providerType === void 0) { providerType = 75; }
                        var self = this;
                        var def = self.plugin.GetQService().defer();
                        if (self.plugin.IsAsync) {
                            self._native.ProviderVersion(providerName, providerType).then(function (x) {
                                def.resolve(x);
                            }, def.reject);
                        }
                        else {
                            try {
                                var a = self._native.ProviderVersion(providerName, providerType);
                                def.resolve(a);
                            }
                            catch (ex) {
                                def.reject(ex);
                            }
                            ;
                        }
                        return def.promise;
                    };
                    CadespluginAbout.Create = function (plugin) {
                        var def = plugin.GetQService().defer();
                        if (plugin.IsAsync) {
                            plugin.CreateObject(CryptoPro.CadespluginObject.About).then(function (x) {
                                var cadesAbout = new CadespluginAbout(plugin, x);
                                var promises = [];
                                promises.push(x.BuildVersion.then(function (z) { return cadesAbout.BuildVersion = z; }));
                                promises.push(x.MajorVersion.then(function (z) { return cadesAbout.MajorVersion = z; }));
                                promises.push(x.MinorVersion.then(function (z) { return cadesAbout.MinorVersion = z; }));
                                promises.push(x.Version.then(function (z) { return cadesAbout.Version = z; }));
                                plugin.GetQService().all(promises)['finally'](function (z) {
                                    def.resolve(cadesAbout);
                                }, function (ex) {
                                    def.reject(ex);
                                });
                            }, function (ex) {
                                def.reject(ex);
                            });
                        }
                        else {
                            plugin.CreateObject(CryptoPro.CadespluginObject.About).then(function (x) {
                                var cadesAbout = new CadespluginAbout(plugin, x);
                                cadesAbout.BuildVersion = x.BuildVersion;
                                cadesAbout.MajorVersion = x.MajorVersion;
                                cadesAbout.MinorVersion = x.MinorVersion;
                                cadesAbout.PluginVersion = x.PluginVersion;
                                cadesAbout.Version = x.Version;
                                def.resolve(cadesAbout);
                            }, function (ex) {
                                def.reject(ex);
                            });
                        }
                        return def.promise;
                    };
                    return CadespluginAbout;
                })();
                CryptoPro.CadespluginAbout = CadespluginAbout;
            })(CryptoPro = Cryptography.CryptoPro || (Cryptography.CryptoPro = {}));
        })(Cryptography || (Cryptography = {}));





        var Cryptography;
        (function (Cryptography) {
            var CryptoPro;
            (function (CryptoPro) {
                var IOSnativeBridge = (function () {
                    function IOSnativeBridge() {
                        IOSnativeBridge.callbacksCount = 1;
                        IOSnativeBridge.callbacks = {};
                    }
                    ;
                    // Automatically called by native layer when a result is available
                    IOSnativeBridge.prototype.resultForCallback = function (callbackId, resultArray) {
                        var callback = IOSnativeBridge.callbacks[callbackId];
                        if (!callback)
                            return;
                        callback.apply(null, resultArray);
                    };
                    ;
                    // Use this in javascript to request native objective-c code
                    // functionName : string (I think the name is explicit :p)
                    // args : array of arguments
                    // callback : function with n-arguments that is going to be called when the native code returned
                    IOSnativeBridge.prototype.call = function (functionName, args, callback) {
                        var hasCallback = callback && typeof callback == "function";
                        var callbackId = hasCallback ? IOSnativeBridge.callbacksCount++ : 0;
                        if (hasCallback)
                            IOSnativeBridge.callbacks[callbackId] = callback;
                        var iframe = document.createElement("IFRAME");
                        var arrObjs = new Array("_CPNP_handle");
                        try {
                            iframe.setAttribute("src", "cpnp-js-call:" + functionName + ":" + callbackId + ":" + encodeURIComponent(JSON.stringify(args, arrObjs)));
                        }
                        catch (e) {
                            throw e;
                        }
                        document.documentElement.appendChild(iframe);
                        iframe.parentNode.removeChild(iframe);
                        iframe = null;
                    };
                    ;
                    return IOSnativeBridge;
                })();
                CryptoPro.IOSnativeBridge = IOSnativeBridge;
            })(CryptoPro = Cryptography.CryptoPro || (Cryptography.CryptoPro = {}));
        })(Cryptography || (Cryptography = {}));

        var Cryptography;
        (function (Cryptography) {
            var X509Certificate;
            (function (X509Certificate) {
                var X509CertificateStringSplitter = (function () {
                    function X509CertificateStringSplitter() {
                    }
                    X509CertificateStringSplitter.prototype.split = function (name) {
                        var self = this;
                        var res = {}, fields = name.split(','), rfields = [], rfc = -1, instr = false;
                        for (var i = 0; i < fields.length; i++) {
                            var position = -1;
                            var p = false;
                            do {
                                position = fields[i].indexOf('"', position + 1);
                                if (position >= 0)
                                    p = !p;
                                else
                                    break;
                            } while (true);
                            if (instr) {
                                rfields[rfc] += fields[i];
                                instr = !p;
                            }
                            else {
                                rfc++;
                                rfields[rfc] = fields[i];
                                instr = p;
                            }
                        }
                        var ff;
                        for (var f in rfields) {
                            if (rfields[f].split) {
                                ff = rfields[f].split('=');
                                if (ff.length < 2)
                                    continue;
                            }
                            //    res[trim(ff[ff.length - 2])] = trim(ff[ff.length - 1]);
                            res[self._trim(ff[0])] = ff[1];
                            for (var ind = 2; ind < ff.length; ind++) {
                                res[self._trim(ff[0])] += '=' + ff[ind];
                            }
                        }
                        if ((res['ИНН'] == null) && (res['OID.1.2.643.3.131.1.1'] != null)) {
                            res['ИНН'] = res['OID.1.2.643.3.131.1.1'];
                        }
                        if (res['OID.1.2.840.113549.1.9.2'] != null) {
                            ff = self._stripquotes(self._trim(res['OID.1.2.840.113549.1.9.2'])).split('=');
                            if (ff.length == 2)
                                res[self._trim(ff[0])] = self._trim(ff[1]);
                        }
                        return res;
                    };
                    ;
                    X509CertificateStringSplitter.prototype.splitAsArray = function (name) {
                        var start = 0;
                        var commaCount = 0;
                        var res = [];
                        for (var i = 0; i <= name.length - 1; i++) {
                            if (name[i] == '"')
                                commaCount++;
                            else if (name[i] == ',' && commaCount % 2 == 0) {
                                res.push(name.substr(start, i - start));
                                start = i + 1;
                                commaCount = 0;
                            }
                            else if (i == name.length - 1) {
                                res.push(name.substr(start));
                            }
                        }
                        var ss = [];
                        for (var i = 0; i <= res.length - 1; i++) {
                            var ff = null;
                            var j = res[i].indexOf('=');
                            if (j > 0) {
                                ff = new Array(2);
                                ff[0] = res[i].substr(0, j);
                                ff[1] = res[i].substr(j + 1);
                            }
                            if (ff) {
                                var key = this._trim(ff[0]);
                                var value = null;
                                switch (key) {
                                    case "1.2.643.3.131.1.1":
                                    case "1.2.643.100.1":
                                    case "1.2.643.100.3":
                                        value = "NUMERICSTRING:" + ff[1];
                                        break;
                                    default:
                                        value = this._stripquotes(ff[1]);
                                        break;
                                }
                                ss.push(key);
                                ss.push(value);
                            }
                        }
                        return ss;
                    };
                    ;
                    X509CertificateStringSplitter.prototype.splitAsObjectArray = function (subject) {
                        var start = 0;
                        var commaCount = 0;
                        var res = [];
                        for (var i = 0; i <= name.length - 1; i++) {
                            if (name[i] == '"')
                                commaCount++;
                            else if (name[i] == ',' && commaCount % 2 == 0) {
                                res.push(name.substr(start, i - start));
                                start = i + 1;
                                commaCount = 0;
                            }
                            else if (i == name.length - 1) {
                                res.push(name.substr(start));
                            }
                        }
                        var ss = [];
                        for (var i = 0; i <= res.length - 1; i++) {
                            var ff = null;
                            var j = res[i].indexOf('=');
                            if (j > 0) {
                                ff = new Array(2);
                                ff[0] = res[i].substr(0, j);
                                ff[1] = res[i].substr(j + 1);
                            }
                            if (ff) {
                                var key = this._trim(ff[0]);
                                var value = null;
                                value = this._stripquotes(ff[1]);
                                ss.push({ rdn: key, value: value });
                            }
                        }
                        return ss;
                    };
                    X509CertificateStringSplitter.prototype._trim = function (a) {
                        return a.replace(/(^\s+)|(\s+$)/g, "");
                    };
                    ;
                    X509CertificateStringSplitter.prototype._stripquotes = function (a) {
                        return a.replace(/(^")|("$)/g, "");
                    };
                    ;
                    return X509CertificateStringSplitter;
                })();
                X509Certificate.X509CertificateStringSplitter = X509CertificateStringSplitter;
            })(X509Certificate = Cryptography.X509Certificate || (Cryptography.X509Certificate = {}));
        })(Cryptography || (Cryptography = {}));

        /// <reference path="x509certificatestringsplitter.ts" />
        var Cryptography;
        (function (Cryptography) {
            var X509Certificate;
            (function (X509Certificate_1) {
                (function (CertificateFindValue) {
                    CertificateFindValue[CertificateFindValue["Thumbprint"] = 1] = "Thumbprint";
                    CertificateFindValue[CertificateFindValue["Serial"] = 2] = "Serial";
                })(X509Certificate_1.CertificateFindValue || (X509Certificate_1.CertificateFindValue = {}));
                var CertificateFindValue = X509Certificate_1.CertificateFindValue;
                ;
                var X509CertificateSubject = (function () {
                    function X509CertificateSubject() {
                    }
                    return X509CertificateSubject;
                })();
                X509Certificate_1.X509CertificateSubject = X509CertificateSubject;
                ;
                var ExtendetKey = (function () {
                    function ExtendetKey() {
                    }
                    return ExtendetKey;
                })();
                X509Certificate_1.ExtendetKey = ExtendetKey;
                ;
                var X509Certificate = (function () {
                    function X509Certificate(plugin, certificate) {
                        this.plugin = plugin;
                        var self = this;
                        if (!certificate)
                            throw { message: 'Argument "certificate" is empty' };
                        this._native = certificate;
                    }
                    ;
                    X509Certificate.prototype.BasicConstraints = function () {
                    };
                    ;
                    X509Certificate.prototype.Display = function () {
                        if (this._native)
                            this._native.Display();
                    };
                    ;
                    X509Certificate.prototype.Export = function () { };
                    ;
                    X509Certificate.prototype.ExtendedKeyUsage = function () { };
                    ;
                    X509Certificate.prototype.ExtendedProperties = function () { };
                    ;
                    X509Certificate.prototype.Extensions = function () { };
                    ;
                    X509Certificate.prototype.GetInfo = function () { };
                    ;
                    X509Certificate.prototype.HasPrivateKey = function () {
                        var def = this.plugin.GetQService().defer();
                        if (this._native) {
                            if (this.plugin.IsAsync) {
                                this._native.HasPrivateKey()
                                    .then(def.resolve, def.reject);
                            }
                            else {
                                try {
                                    var a = this._native.HasPrivateKey();
                                    def.resolve(a);
                                }
                                catch (ex) {
                                    def.reject(ex);
                                }
                            }
                        }
                        else
                            def.reject({ message: 'Don\'t have native object' });
                        return def.promise;
                    };
                    ;
                    X509Certificate.prototype.Import = function () { };
                    ;
                    X509Certificate.prototype.IsValid = function () { };
                    ;
                    X509Certificate.prototype.KeyUsage = function () { };
                    ;
                    X509Certificate.prototype.Save = function (filename, password, saveAs, includeOptions) {
                        if (this._native)
                            this._native.Save(filename, password, saveAs, includeOptions);
                    };
                    ;
                    X509Certificate.prototype.Template = function () {
                        if (this._native)
                            this._native.Template();
                    };
                    ;
                    X509Certificate.prototype.GetNative = function () {
                        return this._native;
                    };
                    X509Certificate.prototype.GetSubject = function () {
                        var a = new X509Certificate_1.X509CertificateStringSplitter();
                        return a.split(this.SubjectName);
                    };
                    ;
                    X509Certificate.prototype.GetIssuer = function () {
                        var a = new X509Certificate_1.X509CertificateStringSplitter();
                        return a.split(this.IssuerName);
                    };
                    ;
                    X509Certificate.Create = function (plugin, certificate) {
                        var def = plugin.GetQService().defer();
                        var promises = [];
                        if (plugin.IsAsync) {
                            certificate.then(function (oCert) {
                                //создаем объект -хранилище информации о сертификате
                                var cert = new Cryptography.X509Certificate.X509Certificate(plugin, oCert);
                                //собираем информацию для его сбора
                                promises.push(oCert.IssuerName.then(function (x) { cert.IssuerName = x; }));
                                promises.push(oCert.SerialNumber.then(function (x) { cert.SerialNumber = x; }));
                                promises.push(oCert.Thumbprint.then(function (x) { cert.Thumbprint = x; }));
                                promises.push(oCert.SubjectName.then(function (x) { cert.SubjectName = x; }));
                                promises.push(oCert.ValidFromDate.then(function (x) { cert.ValidFromDate = x; }));
                                promises.push(oCert.ValidToDate.then(function (x) { cert.ValidToDate = x; }));
                                promises.push(oCert.Version.then(function (x) { cert.Version = x; }));
                                //ждем когда все промисы разрешатся
                                plugin.GetQService().all(promises)['finally'](function (x) {
                                    def.resolve(cert);
                                });
                            });
                        }
                        else {
                            var cert = new Cryptography.X509Certificate.X509Certificate(plugin, certificate);
                            cert.IssuerName = certificate.IssuerName;
                            cert.SerialNumber = certificate.SerialNumber;
                            cert.Thumbprint = certificate.Thumbprint;
                            cert.SubjectName = certificate.SubjectName;
                            cert.ValidFromDate = certificate.ValidFromDate;
                            cert.ValidToDate = certificate.ValidToDate;
                            cert.Version = certificate.Version;
                            def.resolve(cert);
                        }
                        ;
                        return def.promise;
                    };
                    return X509Certificate;
                })();
                X509Certificate_1.X509Certificate = X509Certificate;
            })(X509Certificate = Cryptography.X509Certificate || (Cryptography.X509Certificate = {}));
        })(Cryptography || (Cryptography = {}));

        /// <reference path="../icryptoplugin.ts" />
        /// <reference path="../x509certificate/x509certificate.ts" />
        var Cryptography;
        (function (Cryptography) {
            var Pkcs;
            (function (Pkcs) {
                (function (CmsSignerOptions) {
                    CmsSignerOptions[CmsSignerOptions["CERTIFICATE_INCLUDE_CHAIN_EXCEPT_ROOT"] = 0] = "CERTIFICATE_INCLUDE_CHAIN_EXCEPT_ROOT";
                    CmsSignerOptions[CmsSignerOptions["CERTIFICATE_INCLUDE_WHOLE_CHAIN"] = 1] = "CERTIFICATE_INCLUDE_WHOLE_CHAIN";
                    CmsSignerOptions[CmsSignerOptions["CERTIFICATE_INCLUDE_END_ENTITY_ONLY"] = 2] = "CERTIFICATE_INCLUDE_END_ENTITY_ONLY";
                })(Pkcs.CmsSignerOptions || (Pkcs.CmsSignerOptions = {}));
                var CmsSignerOptions = Pkcs.CmsSignerOptions;
                ;
                var CmsSigner = (function () {
                    function CmsSigner(plugin, nativeSigner) {
                        this.plugin = plugin;
                        this._native = nativeSigner;
                    }
                    CmsSigner.prototype.ComputeSignature = function (data, certificate, detached) {
                        if (detached === void 0) { detached = false; }
                        var self = this;
                        var def = self.plugin.GetQService().defer();
                        if (self.plugin.IsAsync) {
                            self._native.propset_Certificate(certificate.GetNative());
                            self.plugin.CreateObject('CAdESCOM.CadesSignedData').then(function (signedData) {
                                signedData.propset_ContentEncoding(1);
                                signedData.propset_Content(data);
                                signedData.SignCades(self._native, 1, detached, 0).then(function (signature) {
                                    def.resolve(signature);
                                }, function (ex) {
                                    def.reject(ex);
                                });
                            }, function (ex) { def.reject(ex); });
                        }
                        else {
                            self._native.Certificate = certificate.GetNative();
                            self.plugin.CreateObject('CAdESCOM.CadesSignedData').then(function (signedData) {
                                try {
                                    signedData.ContentEncoding = 1;
                                    signedData.Content = data;
                                    var signature = signedData.SignCades(self._native, 1, detached, 0);
                                    def.resolve(signature);
                                }
                                catch (ex) {
                                    def.reject(ex);
                                }
                            }, function (ex) { def.reject(ex); });
                        }
                        return def.promise;
                    };
                    CmsSigner.prototype.ComputeTextSignature = function (data, certificate, detached) {
                        if (detached === void 0) { detached = false; }
                        var self = this;
                        var def = self.plugin.GetQService().defer();
                        if (self.plugin.IsAsync) {
                            self._native.propset_Certificate(certificate.GetNative());
                            self.plugin.CreateObject('CAdESCOM.CadesSignedData').then(function (signedData) {
                                signedData.propset_Content(data);
                                signedData.SignCades(self._native, 1, detached, 0).then(function (signature) {
                                    def.resolve(signature);
                                }, function (ex) {
                                    def.reject(ex);
                                });
                            }, function (ex) { def.reject(ex); });
                        }
                        else {
                            self._native.Certificate = certificate.GetNative();
                            self.plugin.CreateObject('CAdESCOM.CadesSignedData').then(function (signedData) {
                                try {
                                    signedData.Content = data;
                                    var signature = signedData.SignCades(self._native, 1, detached, 0);
                                    def.resolve(signature);
                                }
                                catch (ex) {
                                    def.reject(ex);
                                }
                            }, function (ex) { def.reject(ex); });
                        }
                        return def.promise;
                    };
                    CmsSigner.Create = function (plugin, option) {
                        var self = this;
                        var def = plugin.GetQService().defer();
                        var cmsSigner = null;
                        try {
                            plugin.CreateObject('CAdESCOM.CPSigner')
                                .then(function (signer) {
                                    cmsSigner = new Cryptography.Pkcs.CmsSigner(plugin, signer);
                                    if (option) {
                                        if (plugin.IsAsync)
                                            signer.propset_Options(option);
                                        else
                                            signer.Options = option;
                                    }
                                    def.resolve(cmsSigner);
                                });
                        }
                        catch (ex) {
                            def.reject(ex);
                        }
                        ;
                        return def.promise;
                    };
                    return CmsSigner;
                })();
                Pkcs.CmsSigner = CmsSigner;
            })(Pkcs = Cryptography.Pkcs || (Cryptography.Pkcs = {}));
        })(Cryptography || (Cryptography = {}));

        var Cryptography;
        (function (Cryptography) {
            var Pkcs;
            (function (Pkcs) {
                var EnvelopedCms = (function () {
                    function EnvelopedCms(plugin, nativeCms) {
                        this.plugin = plugin;
                        this._native = nativeCms;
                    }
                    EnvelopedCms.prototype.Decrypt = function (data) {
                        var _this = this;
                        var def = this.plugin.GetQService().defer();
                        if (this.plugin.IsAsync) {
                            this._native.propset_ContentEncoding(1).then(function () {
                                _this._native.Decrypt(data)
                                    .then(function (x) {
                                        _this._native.Content.then(function (x) {
                                            def.resolve(x);
                                        }).catch(function (ex) {
                                            def.reject(ex);
                                        });
                                    }).catch(function (ex) {
                                        def.reject(ex);
                                    });
                            }).catch(function (ex) {
                                def.reject(ex);
                            });
                        }
                        else {
                            try {
                                this._native.ContentEncoding = 1;
                                this._native.Decrypt(data);
                                var result = this._native.Content;
                                def.resolve(result);
                            }
                            catch (ex) {
                                def.reject(ex);
                            }
                        }
                        return def.promise;
                    };
                    EnvelopedCms.Create = function (plugin) {
                        var def = plugin.GetQService().defer();
                        try {
                            plugin.CreateObject('CAdESCOM.CPEnvelopedData').then(function (envelopedData) {
                                var result = new Cryptography.Pkcs.EnvelopedCms(plugin, envelopedData);
                                def.resolve(result);
                            }, function (ex) {
                                def.reject(ex);
                            });
                        }
                        catch (ex) {
                            def.reject(ex);
                        }
                        return def.promise;
                    };
                    return EnvelopedCms;
                })();
                Pkcs.EnvelopedCms = EnvelopedCms;
            })(Pkcs = Cryptography.Pkcs || (Cryptography.Pkcs = {}));
        })(Cryptography || (Cryptography = {}));

        var System;
        (function (System) {
            var SemVer = (function () {
                function SemVer(major, minor, patch, metadata) {
                    if (typeof (major) === 'string') {
                        var ver = major.split('.');
                        this.Major = parseInt(ver[0], 10);
                        this.Minor = ver[1] ? parseInt(ver[1], 10) : 0;
                        this.Patch = ver[2] ? parseInt(ver[2], 10) : 0;
                        this.Metadata = ver[3] ? ver[3] : undefined;
                    }
                    else {
                        this.Major = window.parseInt(major, 10);
                        this.Minor = minor ? window.parseInt(minor, 10) : 0;
                        this.Patch = patch ? window.parseInt(patch, 10) : 0;
                        this.Metadata = metadata;
                    }
                }
                ;
                SemVer.prototype.toString = function () {
                    var s = '';
                    if (this.Major)
                        s += this.Major;
                    else
                        s += '.';
                    if (this.Minor)
                        s += "." + this.Minor;
                    else
                        s += '.';
                    if (this.Patch)
                        s += "." + this.Patch;
                    if (this.Metadata && (s !== '.' || s != '..' || s != '...'))
                        s += '-' + this.Metadata;
                    else
                        s = null;
                    if (s === '' || s === '.' || s === '..' || s === '...')
                        s = null;
                    return s;
                };
                /**
                 * Сравнение двух версий
                 * @param ver1 первая версия
                 * @param ver2 вторая версия
                 */
                SemVer.compare = function (ver1, ver2) {
                    var compareComponent = function (a, b) {
                        if (a == b)
                            return 0;
                        if (a > b)
                            return 1;
                        if (a < b)
                            return -1;
                    };
                    if (!ver1 && !ver2)
                        return 0;
                    else if (ver1 && !ver2)
                        return 1;
                    else if (ver2 && !ver1)
                        return -1;
                    else {
                        var comp1 = compareComponent(ver1.Major, ver2.Major);
                        if (comp1 != 0)
                            return comp1;
                        var comp2 = compareComponent(ver1.Minor, ver2.Minor);
                        if (comp2 != 0)
                            return comp2;
                        var comp3 = compareComponent(ver1.Patch, ver2.Patch);
                        if (comp3 != 0)
                            return comp3;
                    }
                    return 0;
                };
                return SemVer;
            })();
            System.SemVer = SemVer;
        })(System || (System = {}));

        var Cryptography;
        (function (Cryptography) {
            var X509Certificate;
            (function (X509Certificate) {
                (function (X509CertificateFind) {
                    X509CertificateFind[X509CertificateFind["IssuerName"] = 2] = "IssuerName";
                    X509CertificateFind[X509CertificateFind["Thumbprint"] = 0] = "Thumbprint";
                    X509CertificateFind[X509CertificateFind["TimeValid"] = 9] = "TimeValid";
                    X509CertificateFind[X509CertificateFind["Policy"] = 8] = "Policy";
                    X509CertificateFind[X509CertificateFind["Serial"] = -1] = "Serial";
                })(X509Certificate.X509CertificateFind || (X509Certificate.X509CertificateFind = {}));
                var X509CertificateFind = X509Certificate.X509CertificateFind;
                var X509CertificateCollection = (function () {
                    function X509CertificateCollection(plugin, certificatesCollection) {
                        this.plugin = plugin;
                        this._native = certificatesCollection;
                        this.Items = [];
                    }
                    ;
                    X509CertificateCollection.prototype.NativeItem = function (index) {
                        var self = this;
                        var def = this.plugin.GetQService().defer();
                        if (this.plugin.IsAsync) {
                            try {
                                this._native.Item(index).then(function (certificate) {
                                    Cryptography.X509Certificate.X509Certificate.Create(self.plugin, certificate).then(function (oCert) {
                                        def.resolve(oCert);
                                    });
                                });
                            }
                            catch (ex) {
                                def.reject(ex);
                            }
                            ;
                        }
                        else {
                            try {
                                var certificate = this._native.Item(index);
                                Cryptography.X509Certificate.X509Certificate.Create(self.plugin, certificate).then(function (oCert) {
                                    def.resolve(oCert);
                                });
                            }
                            catch (ex) {
                                def.reject(ex);
                            }
                            ;
                        }
                        ;
                        return def.promise;
                    };
                    /*Возвращает сертификат из списка Items*/
                    X509CertificateCollection.prototype.Item = function (index) {
                        return this.Items;
                    };
                    /*Возвращает количество элементов в массиве Items*/
                    X509CertificateCollection.prototype.Count = function () {
                        return this.Items.length;
                    };
                    /*Осуществляет поиск сертификата в списке Items*/
                    X509CertificateCollection.prototype.Find = function (findValue, findObject) {
                        var result = [];
                        if (this.Items) {
                            var theSearch = null;
                            switch (findObject) {
                                case X509CertificateFind.IssuerName:
                                    theSearch = 'IssuerName';
                                    break;
                                case X509CertificateFind.Thumbprint:
                                    if (typeof (findValue) === 'string')
                                        findValue = findValue.replace(' ', '').toUpperCase();
                                    theSearch = 'Thumbprint';
                                    break;
                                case X509CertificateFind.Serial:
                                    if (typeof (findValue) === 'string')
                                        findValue = findValue.replace(' ', '').toUpperCase();
                                    theSearch = 'SerialNumber';
                                    break;
                            }
                            ;
                            for (var i = 0; i < this.Items.length; i++) {
                                var item = this.Items[i];
                                if (item[theSearch]) {
                                    var b = false;
                                    if (typeof (findValue) === 'string')
                                        b = item[theSearch].toString() == findValue;
                                    else if (findValue instanceof RegExp)
                                        b = item[theSearch].toString().match(findValue);
                                    if (b)
                                        result.push(item);
                                }
                                ;
                            }
                            ;
                        }
                        ;
                        return result;
                    };
                    ;
                    /*Создает массив сертификатов из нативной коллекции - служит для облегчения доступа к сертификатам*/
                    X509CertificateCollection.Create = function (plugin, certificateCollection) {
                        var def = plugin.GetQService().defer();
                        var collection = new Cryptography.X509Certificate.X509CertificateCollection(plugin, certificateCollection);
                        if (plugin.IsAsync) {
                            certificateCollection.Count.then(function (x) {
                                var count = x;
                                var promises = [];
                                for (var i = 1; i <= count; i++) {
                                    promises.push(Cryptography.X509Certificate.X509CertificateCollection.AddIntoCollection(plugin, collection.Items, certificateCollection.Item(i)));
                                }
                                ;
                                plugin.GetQService().all(promises)['finally'](function (x) {
                                    def.resolve(collection);
                                });
                            });
                        }
                        else {
                            var promises = [];
                            for (var i = 1; i <= certificateCollection.Count; i++) {
                                promises.push(Cryptography.X509Certificate.X509CertificateCollection.AddIntoCollection(plugin, collection.Items, certificateCollection.Item(i)));
                            }
                            ;
                            plugin.GetQService().all(promises)['finally'](function (x) {
                                def.resolve(collection);
                            });
                        }
                        return def.promise;
                    };
                    ;
                    /*Добавляет сертификат в коллекцию*/
                    X509CertificateCollection.AddIntoCollection = function (plugin, collection, certificate) {
                        var def = plugin.GetQService().defer();
                        Cryptography.X509Certificate.X509Certificate.Create(plugin, certificate).then(function (x) {
                            collection.push(x);
                            def.resolve(x);
                        });
                        return def.promise;
                    };
                    ;
                    return X509CertificateCollection;
                })();
                X509Certificate.X509CertificateCollection = X509CertificateCollection;
            })(X509Certificate = Cryptography.X509Certificate || (Cryptography.X509Certificate = {}));
        })(Cryptography || (Cryptography = {}));

        var System;
        (function (System) {
            var Binary;
            (function (Binary) {
                var Converter = (function () {
                    function Converter() {
                    }
                    Converter.stringToBytes = function (str) {
                        for (var bytes = [], i = 0; i < str.length; i++)
                            bytes.push(str.charCodeAt(i) & 0xFF);
                        return bytes;
                    };
                    ;
                    Converter.bytesToString = function (bytes) {
                        for (var str = [], i = 0; i < bytes.length; i++)
                            str.push(String.fromCharCode(bytes[i]));
                        return str.join("");
                    };
                    ;
                    Converter.rotateToLeft = function (n, b) {
                        return (n << b) | (n >>> (32 - b));
                    };
                    ;
                    Converter.rotateToRight = function (n, b) {
                        return (n << (32 - b)) | (n >>> b);
                    };
                    ;
                    Converter.endian = function (n) {
                        // If number given, swap endian
                        if (n.constructor == Number) {
                            return System.Binary.Converter.rotateToLeft(n, 8) & 0x00FF00FF |
                                System.Binary.Converter.rotateToLeft(n, 24) & 0xFF00FF00;
                        }
                        ;
                        var arr = n;
                        for (var i = 0; i < arr.length; i++)
                            arr[i] = System.Binary.Converter.endian(n[i]);
                        return arr;
                    };
                    ;
                    Converter.randomBytes = function (n) {
                        if (!n)
                            n = 1;
                        for (var bytes = []; n > 0; n--)
                            bytes.push(Math.floor(Math.random() * 256));
                        return bytes;
                    };
                    // Convert a byte array to big-endian 32-bit words
                    Converter.bytesToWords = function (bytes) {
                        for (var words = [], i = 0, b = 0; i < bytes.length; i++ , b += 8)
                            words[b >>> 5] |= bytes[i] << (24 - b % 32);
                        return words;
                    };
                    ;
                    // Convert big-endian 32-bit words to a byte array
                    Converter.wordsToBytes = function (words) {
                        for (var bytes = [], b = 0; b < words.length * 32; b += 8)
                            bytes.push((words[b >>> 5] >>> (24 - b % 32)) & 0xFF);
                        return bytes;
                    };
                    ;
                    Converter.bytesToHex = function (bytes) {
                        for (var hex = [], i = 0; i < bytes.length; i++) {
                            hex.push((bytes[i] >>> 4).toString(16));
                            hex.push((bytes[i] & 0xF).toString(16));
                        }
                        ;
                        return hex.join("");
                    };
                    ;
                    Converter.hexToBytes = function (hex) {
                        for (var bytes = [], c = 0; c < hex.length; c += 2)
                            bytes.push(parseInt(hex.substr(c, 2), 16));
                        return bytes;
                    };
                    ;
                    Converter.bytesToBase64 = function (bytes) {
                        // Use browser-native function if it exists
                        if (typeof btoa == "function")
                            return btoa(System.Binary.Converter.bytesToString(bytes));
                        for (var base64 = [], i = 0; i < bytes.length; i += 3) {
                            var triplet = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
                            for (var j = 0; j < 4; j++) {
                                if (i * 8 + j * 6 <= bytes.length * 8)
                                    base64.push(System.Binary.Converter.base64map.charAt((triplet >>> 6 * (3 - j)) & 0x3F));
                                else
                                    base64.push("=");
                            }
                            ;
                        }
                        ;
                        return base64.join("");
                    };
                    Converter.base64ToBytes = function (base64) {
                        // Use browser-native function if it exists
                        base64 = base64.replace(/\s/g, '');
                        if (typeof atob == "function")
                            return System.Binary.Converter.stringToBytes(atob(base64));
                        // Remove non-base-64 characters
                        base64 = base64.replace(/[^A-Z0-9+\/]/ig, "");
                        for (var bytes = [], i = 0, imod4 = 0; i < base64.length; imod4 = ++i % 4) {
                            if (imod4 == 0)
                                continue;
                            bytes.push(((System.Binary.Converter.base64map.indexOf(base64.charAt(i - 1)) & (Math.pow(2, -2 * imod4 + 8) - 1)) << (imod4 * 2)) |
                                (System.Binary.Converter.base64map.indexOf(base64.charAt(i)) >>> (6 - imod4 * 2)));
                        }
                        ;
                        return bytes;
                    };
                    Converter.UTF8StringToBytes = function (str) {
                        return System.Binary.Converter.stringToBytes(window.unescape(encodeURIComponent(str)));
                    };
                    Converter.UTF8BytesToString = function (bytes) {
                        return decodeURIComponent(window.escape(System.Binary.Converter.bytesToString(bytes)));
                    };
                    Converter.prototype.stringToBase64String = function (str) {
                        var a = System.Binary.Converter.UTF8StringToBytes(str);
                        return System.Binary.Converter.bytesToBase64(a);
                    };
                    Converter.base64map = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
                    return Converter;
                })();
                Binary.Converter = Converter;
            })(Binary = System.Binary || (System.Binary = {}));
        })(System || (System = {}));

        /// <reference path="../system/ipromise.ts" />
        /// <reference path="../system/binary/converter.ts" />
        /// <reference path="../icryptoplugin.ts" />
        var Cryptography;
        (function (Cryptography) {
            var X509Certificate;
            (function (X509Certificate) {
                (function (X509EnrollmentEncodingType) {
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_BASE64HEADER"] = 0] = "XCN_CRYPT_STRING_BASE64HEADER";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_BASE64"] = 1] = "XCN_CRYPT_STRING_BASE64";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_BINARY"] = 2] = "XCN_CRYPT_STRING_BINARY";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_BASE64REQUESTHEADER"] = 3] = "XCN_CRYPT_STRING_BASE64REQUESTHEADER";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_HEX"] = 4] = "XCN_CRYPT_STRING_HEX";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_HEXASCII"] = 5] = "XCN_CRYPT_STRING_HEXASCII";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_BASE64_ANY"] = 6] = "XCN_CRYPT_STRING_BASE64_ANY";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_ANY"] = 7] = "XCN_CRYPT_STRING_ANY";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_HEX_ANY"] = 8] = "XCN_CRYPT_STRING_HEX_ANY";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_BASE64X509CRLHEADER"] = 9] = "XCN_CRYPT_STRING_BASE64X509CRLHEADER";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_HEXADDR"] = 10] = "XCN_CRYPT_STRING_HEXADDR";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_HEXASCIIADDR"] = 11] = "XCN_CRYPT_STRING_HEXASCIIADDR";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_HEXRAW"] = 12] = "XCN_CRYPT_STRING_HEXRAW";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_NOCRLF"] = 1073741824] = "XCN_CRYPT_STRING_NOCRLF";
                    X509EnrollmentEncodingType[X509EnrollmentEncodingType["XCN_CRYPT_STRING_NOCR"] = 2147483648] = "XCN_CRYPT_STRING_NOCR";
                })(X509Certificate.X509EnrollmentEncodingType || (X509Certificate.X509EnrollmentEncodingType = {}));
                var X509EnrollmentEncodingType = X509Certificate.X509EnrollmentEncodingType;
                (function (X509EnrollmentContext) {
                    X509EnrollmentContext[X509EnrollmentContext["User"] = 1] = "User";
                    X509EnrollmentContext[X509EnrollmentContext["Machine"] = 2] = "Machine";
                })(X509Certificate.X509EnrollmentContext || (X509Certificate.X509EnrollmentContext = {}));
                var X509EnrollmentContext = X509Certificate.X509EnrollmentContext;
                var X509Enrollment = (function () {
                    function X509Enrollment(plugin) {
                        this.plugin = plugin;
                    }
                    ;
                    /*Создание закрытого ключа и запроса на сертификат*/
                    X509Enrollment.prototype.Create = function (createInfo) {
                        var self = this;
                        if (this.plugin.IsAsync) {
                            //асинхронное создание
                            return self.InitPrivateKey(createInfo).then(function (privateKey) {
                                return self.plugin.CreateObject('X509Enrollment.CX509CertificateRequestPkcs10').then(function (pkcs10) {
                                    return pkcs10.InitializeFromPrivateKey(createInfo.MachineContext ? 0x2 : 0x1, privateKey, "").then(function (x) {
                                        return self.LimitExchangeKeyToEncipherment(pkcs10, false, createInfo.KeySpec).then(function (x) {
                                            return self.AddDistinguishedName(pkcs10, createInfo.Subject).then(function (x) {
                                                return self.AddEKUToRequest(pkcs10, createInfo.EKUS).then(function (x) {
                                                    return self.SetHashAlgorithm(pkcs10, createInfo.HashAlgoritmOID).then(function (x) {
                                                        return self.InitializeFromRequest(pkcs10).then(function (enroll) {
                                                            return self.AddTemplateExtension(pkcs10, createInfo.Template).then(function () {
                                                                if (createInfo.TemplateData) {
                                                                    return self.AddCertType(pkcs10, createInfo.TemplateData).then(function (x) {
                                                                        return self.CreateRequest(enroll);
                                                                    });
                                                                }
                                                                else {
                                                                    return self.CreateRequest(enroll);
                                                                }
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        }
                        else {
                            //синхронное создание
                            return self.InitPrivateKey(createInfo).then(function (privateKey) {
                                return self.plugin.CreateObject('X509Enrollment.CX509CertificateRequestPkcs10').then(function (pkcs10) {
                                    pkcs10.InitializeFromPrivateKey(createInfo.MachineContext ? 0x2 : 0x1, privateKey, "");
                                    return self.LimitExchangeKeyToEncipherment(pkcs10, false, createInfo.KeySpec).then(function (x) {
                                        return self.AddDistinguishedName(pkcs10, createInfo.Subject).then(function (x) {
                                            return self.AddEKUToRequest(pkcs10, createInfo.EKUS).then(function (x) {
                                                return self.SetHashAlgorithm(pkcs10, createInfo.HashAlgoritmOID).then(function (x) {
                                                    return self.AddTemplateExtension(pkcs10, createInfo.Template).then(function () {
                                                        return self.InitializeFromRequest(pkcs10).then(function (enroll) {
                                                            if (createInfo.TemplateData) {
                                                                return self.AddCertType(pkcs10, createInfo.TemplateData).then(function (x) {
                                                                    return self.CreateRequest(enroll);
                                                                });
                                                            }
                                                            else {
                                                                return self.CreateRequest(enroll);
                                                            }
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        }
                        ;
                    };
                    ;
                    /*Установка сертификат в контейнер*/
                    X509Enrollment.prototype.Install = function (pkcs7, context, password) {
                        if (password === void 0) { password = ''; }
                        var self = this;
                        var def = self.plugin.GetQService().defer();
                        if (self.plugin.IsAsync) {
                            return self.plugin.CreateObject('X509Enrollment.CX509Enrollment').then(function (enroll) {
                                enroll.Initialize(context).then(function (x) {
                                    enroll.InstallResponse(0, pkcs7, X509EnrollmentEncodingType.XCN_CRYPT_STRING_ANY, password).then(function () {
                                        def.resolve(true);
                                    }, function (ex) { def.reject(ex); });
                                }, function (ex) { def.reject(ex); });
                            });
                        }
                        else {
                            self.plugin.CreateObject('X509Enrollment.CX509Enrollment').then(function (enroll) {
                                try {
                                    enroll.Initialize(context);
                                    enroll.InstallResponse(0, pkcs7, X509EnrollmentEncodingType.XCN_CRYPT_STRING_ANY, password);
                                    def.resolve(true);
                                }
                                catch (ex) {
                                    def.reject(ex);
                                }
                            }, function (ex) { def.reject(ex); });
                        }
                        ;
                        return def.promise;
                    };
                    ;
                    /*Проверка созданного запроса на сертификат*/
                    X509Enrollment.prototype.VerifyPKCS10 = function (pkcs10) {
                        var result = true;
                        var tmpKey = pkcs10;
                        tmpKey = tmpKey.replace('-----BEGIN NEW CERTIFICATE REQUEST-----\r\n', '');
                        tmpKey = tmpKey.replace('-----END NEW CERTIFICATE REQUEST-----\r\n', '');
                        tmpKey = tmpKey.replace('\r\n', '');
                        var buffer = System.Binary.Converter.base64ToBytes(tmpKey);
                        //var buffer = window.Crypto.util.base64ToBytes(tmpKey);
                        if (buffer.length > 0) {
                            var hex = System.Binary.Converter.bytesToHex(buffer);
                            if (hex != null && hex.length > 0) {
                                var i1 = hex.indexOf('06052a85036403');
                                var i2 = hex.indexOf('06052a85036401');
                                var i3 = hex.indexOf('06052a85036405');
                                if (i1 > (-1))
                                    if (hex.substr(i1 + 14, 2) !== '12')
                                        result = false;
                                if (i2 > (-1))
                                    if (hex.substr(i2 + 14, 2) !== '12')
                                        result = false;
                                if (i3 > (-1))
                                    if (hex.substr(i3 + 14, 2) !== '12')
                                        result = false;
                            }
                            else
                                result = false;
                        }
                        else
                            result = false;
                        return result;
                    };
                    /*Инициализация закрытого ключа*/
                    X509Enrollment.prototype.InitPrivateKey = function (createInfo) {
                        var CRYPT_USER_PROTECTED = 2, CRYPT_EXPORTABLE = 1, XCN_NCRYPT_UI_PROTECT_KEY_FLAG = 1, XCN_NCRYPT_UI_NO_PROTECTION_FLAG = 0, XCN_NCRYPT_ALLOW_EXPORT_FLAG = 1;
                        if (this.plugin.IsAsync) {
                            return this.plugin.CreateObject('X509Enrollment.CX509PrivateKey').then(function (privateKey) {
                                privateKey.propset_ProviderName(createInfo.ProviderName != null ? createInfo.ProviderName : '');
                                privateKey.propset_ProviderType(createInfo.ProviderType);
                                privateKey.propset_KeySpec(createInfo.KeySpec);
                                if (createInfo.ContainerName && createInfo.ContainerName.length > 0)
                                    privateKey.propset_ContainerName(createInfo.ContainerName);
                                privateKey.propset_KeyProtection(CRYPT_USER_PROTECTED & (createInfo.KeyGenFlags != 0 ? XCN_NCRYPT_UI_PROTECT_KEY_FLAG : XCN_NCRYPT_UI_NO_PROTECTION_FLAG));
                                privateKey.propset_ExportPolicy(CRYPT_EXPORTABLE & (createInfo.KeyGenFlags != 0 ? XCN_NCRYPT_ALLOW_EXPORT_FLAG : 0));
                                privateKey.propset_Length(createInfo.KeyGenFlags >> 16);
                                privateKey.propset_MachineContext(createInfo.MachineContext ? createInfo.MachineContext : false);
                                return privateKey;
                            });
                        }
                        else {
                            return this.plugin.CreateObject('X509Enrollment.CX509PrivateKey').then(function (privateKey) {
                                privateKey.ProviderName = createInfo.ProviderName != null ? createInfo.ProviderName : '';
                                privateKey.ProviderType = createInfo.ProviderType;
                                privateKey.KeySpec = createInfo.KeySpec;
                                if (createInfo.ContainerName && createInfo.ContainerName.length > 0)
                                    privateKey.ContainerName = createInfo.ContainerName;
                                privateKey.KeyProtection = (CRYPT_USER_PROTECTED & (createInfo.KeyGenFlags != 0 ? XCN_NCRYPT_UI_PROTECT_KEY_FLAG : XCN_NCRYPT_UI_NO_PROTECTION_FLAG));
                                privateKey.ExportPolicy = (CRYPT_EXPORTABLE & (createInfo.KeyGenFlags != 0 ? XCN_NCRYPT_ALLOW_EXPORT_FLAG : 0));
                                privateKey.Length = (createInfo.KeyGenFlags >> 16);
                                privateKey.MachineContext = (createInfo.MachineContext ? createInfo.MachineContext : false);
                                return privateKey;
                            });
                        }
                    };
                    X509Enrollment.prototype.LimitExchangeKeyToEncipherment = function (pkcs10, limit, keySpec) {
                        var self = this;
                        var CERT_DIGITAL_SIGNATURE_KEY_USAGE = 0x80, CERT_NON_REPUDIATION_KEY_USAGE = 0x40, CERT_KEY_ENCIPHERMENT_KEY_USAGE = 0x20, CERT_DATA_ENCIPHERMENT_KEY_USAGE = 0x10;
                        if (this.plugin.IsAsync) {
                            return self.plugin.CreateObject('X509Enrollment.CX509ExtensionKeyUsage').then(function (keyUsage) {
                                if (keySpec == 2) {
                                    return keyUsage.InitializeEncode(CERT_DIGITAL_SIGNATURE_KEY_USAGE | CERT_NON_REPUDIATION_KEY_USAGE).then(function (x) {
                                        pkcs10.X509Extensions.then(function (extensions) {
                                            return extensions.Add(keyUsage);
                                        });
                                    });
                                }
                                else {
                                    if (limit) {
                                        return keyUsage.InitializeEncode(CERT_KEY_ENCIPHERMENT_KEY_USAGE | CERT_DATA_ENCIPHERMENT_KEY_USAGE).then(function (x) {
                                            pkcs10.X509Extensions.then(function (extensions) {
                                                return extensions.Add(keyUsage);
                                            });
                                        });
                                    }
                                    else {
                                        return keyUsage.InitializeEncode(CERT_KEY_ENCIPHERMENT_KEY_USAGE | CERT_DATA_ENCIPHERMENT_KEY_USAGE | CERT_DIGITAL_SIGNATURE_KEY_USAGE | CERT_NON_REPUDIATION_KEY_USAGE).then(function (x) {
                                            pkcs10.X509Extensions.then(function (extensions) {
                                                return extensions.Add(keyUsage);
                                            });
                                        });
                                    }
                                }
                            });
                        }
                        else {
                            return self.plugin.CreateObject('X509Enrollment.CX509ExtensionKeyUsage').then(function (keyUsage) {
                                if (keySpec == 2) {
                                    keyUsage.InitializeEncode(CERT_DIGITAL_SIGNATURE_KEY_USAGE | CERT_NON_REPUDIATION_KEY_USAGE);
                                }
                                else {
                                    if (limit) {
                                        keyUsage.InitializeEncode(CERT_KEY_ENCIPHERMENT_KEY_USAGE | CERT_DATA_ENCIPHERMENT_KEY_USAGE);
                                    }
                                    else {
                                        keyUsage.InitializeEncode(CERT_KEY_ENCIPHERMENT_KEY_USAGE | CERT_DATA_ENCIPHERMENT_KEY_USAGE | CERT_DIGITAL_SIGNATURE_KEY_USAGE | CERT_NON_REPUDIATION_KEY_USAGE);
                                    }
                                }
                                ;
                                return pkcs10.X509Extensions.Add(keyUsage);
                            });
                        }
                    };
                    X509Enrollment.prototype.AddDistinguishedName = function (pkcs10, subject) {
                        var self = this;
                        var XCN_CERT_NAME_STR_NONE = 0, XCN_CERT_SIMPLE_NAME_STR = 1, XCN_CERT_OID_NAME_STR = 2, XCN_CERT_X500_NAME_STR = 3, XCN_CERT_XML_NAME_STR = 4, XCN_CERT_NAME_STR_SEMICOLON_FLAG = 0x40000000, XCN_CERT_NAME_STR_NO_PLUS_FLAG = 0x20000000, XCN_CERT_NAME_STR_NO_QUOTING_FLAG = 0x10000000, XCN_CERT_NAME_STR_CRLF_FLAG = 0x8000000, XCN_CERT_NAME_STR_COMMA_FLAG = 0x4000000, XCN_CERT_NAME_STR_REVERSE_FLAG = 0x2000000, XCN_CERT_NAME_STR_FORWARD_FLAG = 0x1000000, XCN_CERT_NAME_STR_DISABLE_IE4_UTF8_FLAG = 0x10000, XCN_CERT_NAME_STR_ENABLE_T61_UNICODE_FLAG = 0x20000, XCN_CERT_NAME_STR_ENABLE_UTF8_UNICODE_FLAG = 0x40000, XCN_CERT_NAME_STR_FORCE_UTF8_DIR_STR_FLAG = 0x80000, XCN_CERT_NAME_STR_DISABLE_UTF8_DIR_STR_FLAG = 0x100000;
                        if (self.plugin.IsAsync) {
                            return self.plugin.CreateObject('X509Enrollment.CX500DistinguishedName').then(function (distinguishedName) {
                                return distinguishedName.Encode(subject, XCN_CERT_NAME_STR_FORWARD_FLAG).then(function (x) {
                                    return pkcs10.propset_Subject(distinguishedName);
                                });
                            });
                        }
                        else {
                            return self.plugin.CreateObject('X509Enrollment.CX500DistinguishedName').then(function (distinguishedName) {
                                distinguishedName.Encode(subject, XCN_CERT_NAME_STR_FORWARD_FLAG);
                                pkcs10.Subject = (distinguishedName);
                            });
                        }
                        ;
                    };
                    X509Enrollment.prototype.AddEKUToRequest = function (pkcs10, ekus) {
                        var self = this;
                        var localEkus = null;
                        if (typeof (ekus) === 'string') {
                            localEkus = ekus.split(',');
                        }
                        else
                            localEkus = ekus;
                        var bAddedCodeSignEKU = false, bMustAddCodeSignEKU = false, nLen, ObjectId, ObjectIds, X509ExtensionEnhancedKeyUsage, SPC_INDIVIDUAL_SP_KEY_PURPOSE_OBJID = "1.3.6.1.4.1.311.2.1.21", SPC_COMMERCIAL_SP_KEY_PURPOSE_OBJID = "1.3.6.1.4.1.311.2.1.22", szOID_PKIX_KP_CODE_SIGNING = "1.3.6.1.5.5.7.3.3";
                        if (self.plugin.IsAsync) {
                            return self.plugin.CreateObject('X509Enrollment.CObjectIds').then(function (oids) {
                                var promises = [];
                                if (localEkus) {
                                    for (var i = 0; i < localEkus.length; i++) {
                                        var sEKU = localEkus[i];
                                        if (sEKU === SPC_INDIVIDUAL_SP_KEY_PURPOSE_OBJID || sEKU === SPC_COMMERCIAL_SP_KEY_PURPOSE_OBJID)
                                            bMustAddCodeSignEKU = true;
                                        if (sEKU === szOID_PKIX_KP_CODE_SIGNING)
                                            bAddedCodeSignEKU = true;
                                        var promise = self.AddEku(oids, sEKU);
                                        promises.push(promise);
                                    }
                                    ; //конец цикла
                                }
                                ;
                                return self.plugin.GetQService().all(promises)['finally'](function (x) {
                                    if (bMustAddCodeSignEKU && !bAddedCodeSignEKU) {
                                        return self.plugin.CreateObject("X509Enrollment.CObjectId").then(function (oid) {
                                            return oid.InitializeFromValue(szOID_PKIX_KP_CODE_SIGNING).then(function (x) {
                                                return oids.Add(oid).then(function (x) {
                                                    return self.plugin.CreateObject("X509Enrollment.CX509ExtensionEnhancedKeyUsage").then(function (objEKU) {
                                                        return objEKU.InitializeEncode(oids).then(function (x) {
                                                            return pkcs10.X509Extensions.then(function (x) {
                                                                return x.Add(objEKU);
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    }
                                    else {
                                        return self.plugin.CreateObject("X509Enrollment.CX509ExtensionEnhancedKeyUsage").then(function (objEKU) {
                                            return objEKU.InitializeEncode(oids).then(function (x) {
                                                return pkcs10.X509Extensions.then(function (x) {
                                                    return x.Add(objEKU);
                                                });
                                            });
                                        });
                                    }
                                    ;
                                });
                            });
                        }
                        else {
                            //синхронный код
                            return self.plugin.CreateObject('X509Enrollment.CObjectIds').then(function (oids) {
                                if (localEkus) {
                                    var promises = [];
                                    for (var i = 0; i < localEkus.length; i++) {
                                        var sEKU = localEkus[i];
                                        if (sEKU === SPC_INDIVIDUAL_SP_KEY_PURPOSE_OBJID || sEKU === SPC_COMMERCIAL_SP_KEY_PURPOSE_OBJID)
                                            bMustAddCodeSignEKU = true;
                                        if (sEKU === szOID_PKIX_KP_CODE_SIGNING)
                                            bAddedCodeSignEKU = true;
                                        var promise = self.AddEku(oids, sEKU);
                                        promises.push(promise);
                                    }
                                    ; //конец цикла
                                    return self.plugin.GetQService().all(promises)['finally'](function (x) {
                                        if (bMustAddCodeSignEKU && !bAddedCodeSignEKU) {
                                            return self.plugin.CreateObject("X509Enrollment.CObjectId").then(function (oid) {
                                                oid.InitializeFromValue(szOID_PKIX_KP_CODE_SIGNING);
                                                oids.Add(oid);
                                                return self.plugin.CreateObject("X509Enrollment.CX509ExtensionEnhancedKeyUsage").then(function (objEKU) {
                                                    objEKU.InitializeEncode(oids);
                                                    pkcs10.X509Extensions.Add(objEKU);
                                                });
                                            });
                                        }
                                        else {
                                            return self.plugin.CreateObject("X509Enrollment.CX509ExtensionEnhancedKeyUsage").then(function (objEKU) {
                                                objEKU.InitializeEncode(oids);
                                                pkcs10.X509Extensions.Add(objEKU);
                                            });
                                        }
                                        ;
                                    });
                                }
                                ;
                            });
                        }
                        ;
                    };
                    ;
                    X509Enrollment.prototype.AddEku = function (oids, eku) {
                        var self = this;
                        var def = self.plugin.GetQService().defer();
                        if (self.plugin.IsAsync) {
                            self.plugin.CreateObject("X509Enrollment.CObjectId").then(function (oid) {
                                oid.InitializeFromValue(eku).then(function (x) {
                                    oids.Add(oid);
                                    def.resolve(oid);
                                }, def.reject);
                            }, def.reject);
                        }
                        else {
                            self.plugin.CreateObject("X509Enrollment.CObjectId").then(function (oid) {
                                try {
                                    oid.InitializeFromValue(eku);
                                    oids.Add(oid);
                                    def.resolve(oid);
                                }
                                catch (ex) {
                                    def.reject(ex);
                                }
                            }, def.reject);
                        }
                        return def.promise;
                    };
                    /*Установка алгоритма хэша*/
                    X509Enrollment.prototype.SetHashAlgorithm = function (pkcs10, hashAlgorithmOID) {
                        var self = this;
                        if (self.plugin.IsAsync) {
                            return self.plugin.CreateObject('X509Enrollment.CObjectId').then(function (oid) {
                                return oid.InitializeFromValue(hashAlgorithmOID).then(function (x) {
                                    pkcs10.propset_HashAlgorithm(oid);
                                });
                            });
                        }
                        else {
                            return self.plugin.CreateObject('X509Enrollment.CObjectId').then(function (oid) {
                                oid.InitializeFromValue(hashAlgorithmOID);
                                pkcs10.HashAlgorithm = (oid);
                            });
                        }
                    };
                    X509Enrollment.prototype.InitializeFromRequest = function (pkcs10) {
                        var self = this;
                        if (self.plugin.IsAsync) {
                            return self.plugin.CreateObject('X509Enrollment.CX509Enrollment').then(function (enroll) {
                                return enroll.InitializeFromRequest(pkcs10).then(function (x) { return enroll; });
                            });
                        }
                        else {
                            return self.plugin.CreateObject('X509Enrollment.CX509Enrollment').then(function (enroll) {
                                enroll.InitializeFromRequest(pkcs10);
                                return enroll;
                            });
                        }
                        ;
                    };
                    ;
                    X509Enrollment.prototype.AddCertType = function (pkcs10, templateData) {
                        var self = this;
                        if (self.plugin.IsAsync) {
                            return self.plugin.CreateObject("X509Enrollment.CX509ExtensionTemplateName").then(function (template) {
                                return template.InitializeEncode(templateData).then(function (x) {
                                    return pkcs10.X509Extensions.then(function (x) {
                                        return x.Add(template);
                                    });
                                });
                            });
                        }
                        else {
                            return self.plugin.CreateObject("X509Enrollment.CX509ExtensionTemplateName").then(function (template) {
                                template.InitializeEncode(templateData);
                                pkcs10.X509Extensions.Add(template);
                            });
                        }
                        ;
                    };
                    ;
                    X509Enrollment.prototype.AddTemplateExtension = function (pkcs10, template) {
                        var self = this;
                        if (template) {
                            if (self.plugin.IsAsync) {
                                return self.plugin.CreateObject("X509Enrollment.CObjectId").then(function (oid) {
                                    return oid.InitializeFromValue(template.Oid).then(function (x) {
                                        return self.plugin.CreateObject("X509Enrollment.CX509ExtensionTemplate").then(function (extTemplate) {
                                            return extTemplate.InitializeEncode(oid, template.MajorVersion || 1, template.MinorVersion || 0).then(function (x) {
                                                return pkcs10.X509Extensions.then(function (x) {
                                                    return x.Add(extTemplate);
                                                });
                                            });
                                        });
                                    });
                                });
                            }
                            else {
                                var def = self.plugin.GetQService().defer();
                                self.plugin.CreateObject("X509Enrollment.CObjectId").then(function (oid) {
                                    try {
                                        oid.InitializeFromValue(template.Oid);
                                        self.plugin.CreateObject("X509Enrollment.CX509ExtensionTemplate").then(function (extTemplate) {
                                            extTemplate.InitializeEncode(oid, template.MajorVersion || 1, template.MinorVersion || 0);
                                            pkcs10.X509Extensions.Add(extTemplate);
                                            def.resolve();
                                        });
                                    }
                                    catch (ex) {
                                        def.reject(ex);
                                    }
                                }, def.reject);
                                return def.promise;
                            }
                        }
                        else {
                            var def = self.plugin.GetQService().defer();
                            def.resolve();
                            return def.promise;
                        }
                        ;
                    };
                    ;
                    X509Enrollment.prototype.CreateRequest = function (enroll, flags) {
                        var self = this;
                        var def = self.plugin.GetQService().defer();
                        if (!flags)
                            flags = X509EnrollmentEncodingType.XCN_CRYPT_STRING_BASE64 | X509EnrollmentEncodingType.XCN_CRYPT_STRING_NOCRLF;
                        if (self.plugin.IsAsync) {
                            enroll.CreateRequest(flags).then(function (x) {
                                def.resolve(x);
                            }, function (ex) { def.reject(ex); });
                        }
                        else {
                            try {
                                var a = enroll.CreateRequest(flags);
                                def.resolve(a);
                            }
                            catch (ex) {
                                def.reject(ex);
                            }
                            ;
                        }
                        ;
                        return def.promise;
                    };
                    ;
                    return X509Enrollment;
                })();
                X509Certificate.X509Enrollment = X509Enrollment;
            })(X509Certificate = Cryptography.X509Certificate || (Cryptography.X509Certificate = {}));
        })(Cryptography || (Cryptography = {}));

        var Cryptography;
        (function (Cryptography) {
            var X509Certificate;
            (function (X509Certificate) {
                var PrivateKey = (function () {
                    function PrivateKey(plugin, privateKey) {
                        this.plugin = plugin;
                        if (!privateKey)
                            throw { message: 'Argumant privateKey is empty' };
                        this._native = privateKey;
                    }
                    ;
                    return PrivateKey;
                })();
                X509Certificate.PrivateKey = PrivateKey;
            })(X509Certificate = Cryptography.X509Certificate || (Cryptography.X509Certificate = {}));
        })(Cryptography || (Cryptography = {}));

        var Cryptography;
        (function (Cryptography) {
            var X509Certificate;
            (function (X509Certificate) {
                (function (X509StoreLocation) {
                    X509StoreLocation[X509StoreLocation["CurrentUser"] = 2] = "CurrentUser";
                    X509StoreLocation[X509StoreLocation["LocalMachine"] = 1] = "LocalMachine";
                })(X509Certificate.X509StoreLocation || (X509Certificate.X509StoreLocation = {}));
                var X509StoreLocation = X509Certificate.X509StoreLocation;
                ;
                (function (X509StoreName) {
                    X509StoreName[X509StoreName["AddressBook"] = 'AddressBook'] = "AddressBook";
                    X509StoreName[X509StoreName["AuthRoot"] = 'AuthRoot'] = "AuthRoot";
                    X509StoreName[X509StoreName["CertificateAuthority"] = 'CertificateAuthority'] = "CertificateAuthority";
                    X509StoreName[X509StoreName["Disallowed"] = 'Disallowed'] = "Disallowed";
                    X509StoreName[X509StoreName["My"] = 'My'] = "My";
                    X509StoreName[X509StoreName["Root"] = 'Root'] = "Root";
                    X509StoreName[X509StoreName["TrustedPeople"] = 'TrustedPeople'] = "TrustedPeople";
                    X509StoreName[X509StoreName["TrustedPublisher"] = 'TrustedPublisher'] = "TrustedPublisher";
                })(X509Certificate.X509StoreName || (X509Certificate.X509StoreName = {}));
                var X509StoreName = X509Certificate.X509StoreName;
                ;
                (function (X509StoreOpenFlags) {
                    X509StoreOpenFlags[X509StoreOpenFlags["IncludeArchived"] = 256] = "IncludeArchived";
                    X509StoreOpenFlags[X509StoreOpenFlags["MaxAllowed"] = 2] = "MaxAllowed";
                    X509StoreOpenFlags[X509StoreOpenFlags["OpenExistingOnly"] = 128] = "OpenExistingOnly";
                    X509StoreOpenFlags[X509StoreOpenFlags["ReadOnly"] = 0] = "ReadOnly";
                    X509StoreOpenFlags[X509StoreOpenFlags["ReadWrite"] = 1] = "ReadWrite";
                })(X509Certificate.X509StoreOpenFlags || (X509Certificate.X509StoreOpenFlags = {}));
                var X509StoreOpenFlags = X509Certificate.X509StoreOpenFlags;
                var X509Store = (function () {
                    function X509Store(plugin, nativeStore, storeName, storeLocation) {
                        if (storeLocation === void 0) { storeLocation = X509StoreLocation.CurrentUser; }
                        this.plugin = plugin;
                        this.storeName = storeName;
                        this.storeLocation = storeLocation;
                        if (!plugin)
                            throw { message: 'Empty argument: plugin' };
                        this._native = nativeStore;
                    }
                    ;
                    X509Store.prototype.GetNative = function () {
                        var def = this.plugin.GetQService().defer();
                        var self = this;
                        this.plugin.CreateObject('CAdESCOM.store').then(function (x) {
                            self._native = x;
                            def.resolve(self);
                        }, function (x) {
                            self._native = null;
                            def.reject(x);
                        });
                        return def.promise;
                    };
                    ;
                    X509Store.prototype.Open = function (openFlags) {
                        if (openFlags === void 0) { openFlags = X509StoreOpenFlags.ReadOnly; }
                        var def = this.plugin.GetQService().defer();
                        var self = this;
                        if (this._native) {
                            if (this.plugin.IsAsync) {
                                this._native.Open(this.storeLocation, this.storeName, openFlags).then(function (x) {
                                    def.resolve(self);
                                }, function (x) { return def.reject(x); });
                            }
                            else {
                                try {
                                    self._native.Open(this.storeLocation, this.storeName, openFlags);
                                    def.resolve(self);
                                }
                                catch (e) {
                                    def.reject(e);
                                }
                            }
                        }
                        else
                            def.reject({ message: 'Native store is empty' });
                        return def.promise;
                    };
                    ;
                    X509Store.prototype.Close = function () {
                        if (this._native) {
                            this._native.Close();
                        }
                        ;
                    };
                    ;
                    /*Получает расположение хранилища сертификатов X.509.*/
                    X509Store.prototype.GetLocation = function () { return this.storeLocation; };
                    ;
                    /*Возвращает имя хранилища сертификатов X.509.*/
                    X509Store.prototype.GetName = function () { return this.storeName; };
                    ;
                    X509Store.prototype.Add = function (certificate) { };
                    X509Store.prototype.Remove = function (certificate) { };
                    /*получение списка сертификатов*/
                    X509Store.prototype.Certificates = function () {
                        var self = this;
                        var def = this.plugin.GetQService().defer();
                        if (this._native) {
                            if (this.plugin.IsAsync) {
                                this._native.Certificates.then(function (x) {
                                    Cryptography.X509Certificate.X509CertificateCollection.Create(self.plugin, x).then(function (x) {
                                        def.resolve(x);
                                    });
                                });
                            }
                            else {
                                Cryptography.X509Certificate.X509CertificateCollection.Create(self.plugin, this._native.Certificates).then(function (x) {
                                    def.resolve(x);
                                });
                            }
                        }
                        else
                            def.reject({ message: 'Native store is empty' });
                        return def.promise;
                    };
                    /*Осуществляет поиск сертификата*/
                    X509Store.prototype.FindCertificate = function (findValue, findObject) {
                        var self = this;
                        if (self.plugin.IsAsync) {
                            return self._native.Find(findValue, findObject).then(function (collection) {
                                return X509Certificate.X509CertificateCollection.Create(self.plugin, collection);
                            });
                        }
                        else {
                            var collection = self._native.Find(findValue, findObject);
                            return X509Certificate.X509CertificateCollection.Create(self.plugin, collection);
                        }
                        ;
                    };
                    ;
                    X509Store.Create = function (plugin, storeName, storeLocation) {
                        if (storeName === void 0) { storeName = 'My'; }
                        if (storeLocation === void 0) { storeLocation = X509StoreLocation.CurrentUser; }
                        return plugin.CreateObject('CAdESCOM.store').then(function (x) {
                            var x509Store = new X509Store(plugin, x, storeName, storeLocation);
                            return x509Store;
                        });
                    };
                    X509Store.Open = function (plugin, storeName, storeLocation, openFlags) {
                        if (storeName === void 0) { storeName = 'My'; }
                        if (storeLocation === void 0) { storeLocation = X509StoreLocation.CurrentUser; }
                        if (openFlags === void 0) { openFlags = X509StoreOpenFlags.ReadOnly; }
                        if (plugin instanceof X509Store) {
                            return plugin.Open(openFlags);
                        }
                        else {
                            return X509Store.Create(plugin, storeName, storeLocation).then(function (store) {
                                return store.Open(openFlags);
                            });
                        }
                    };
                    return X509Store;
                })();
                X509Certificate.X509Store = X509Store;
            })(X509Certificate = Cryptography.X509Certificate || (Cryptography.X509Certificate = {}));
        })(Cryptography || (Cryptography = {}));

        var Cryptography;
        (function (Cryptography) {
            var Xml;
            (function (Xml) {
                var XmlSigner = (function () {
                    function XmlSigner(plugin, nativeSigner) {
                        this.plugin = plugin;
                        this._native = nativeSigner;
                    }
                    XmlSigner.prototype.ComputeSignature = function (xml, certificate, template) {
                        var CADESCOM_XML_SIGNATURE_TYPE_ENVELOPED = 0, CADESCOM_XML_SIGNATURE_TYPE_ENVELOPING = 1, CADESCOM_XML_SIGNATURE_TYPE_TEMPLATE = 2;
                        var XmlDsigGost3410UrlObsolete = "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411", XmlDsigGost3411UrlObsolete = "http://www.w3.org/2001/04/xmldsig-more#gostr3411", XmlDsigGost3410Url = "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34102001-gostr3411", XmlDsigGost3411Url = "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr3411";
                        var self = this;
                        if (self.plugin.IsAsync) {
                            return self.plugin.CreateObject('CAdESCOM.CPSigner').then(function (signer) {
                                signer.propset_Certificate(certificate.GetNative());
                                self._native.propset_Content(xml);
                                // Указываем тип подписи - в данном случае вложенная
                                self._native.propset_SignatureType(2); //CADESCOM_XML_SIGNATURE_TYPE_ENVELOPED;
                                // Указываем алгоритм подписи
                                self._native.propset_SignatureMethod(XmlDsigGost3410Url);
                                // Указываем алгоритм хэширования
                                self._native.propset_DigestMethod(XmlDsigGost3411Url);
                                return self._native.Sign(signer).then(function (x) { return x; });
                            });
                        }
                        else {
                            return self.plugin.CreateObject('CAdESCOM.CPSigner').then(function (signer) {
                                signer.Certificate = (certificate.GetNative());
                                self._native.Content = (xml);
                                // Указываем тип подписи - в данном случае вложенная
                                self._native.SignatureType = 2; //CADESCOM_XML_SIGNATURE_TYPE_ENVELOPED;
                                // Указываем алгоритм подписи
                                self._native.SignatureMethod = XmlDsigGost3410Url;
                                // Указываем алгоритм хэширования
                                self._native.DigestMethod = XmlDsigGost3411Url;
                                return self._native.Sign(signer);
                            });
                        }
                    };
                    XmlSigner.Create = function (plugin) {
                        return plugin.CreateObject('CAdESCOM.SignedXML').then(function (nativeSigner) {
                            var signer = new Cryptography.Xml.XmlSigner(plugin, nativeSigner);
                            return signer;
                        });
                    };
                    return XmlSigner;
                })();
                Xml.XmlSigner = XmlSigner;
            })(Xml = Cryptography.Xml || (Cryptography.Xml = {}));
        })(Cryptography || (Cryptography = {}));

        var System;
        (function (System) {
            var ASN1;
            (function (ASN1) {
                var Asn1 = (function () {
                    function Asn1() {
                        var ID = [];
                        ID['BOOLEAN'] = 0x01;
                        ID['INTEGER'] = 0x02;
                        ID['BITSTRING'] = 0x03;
                        ID['OCTETSTRING'] = 0x04;
                        ID['NULL'] = 0x05;
                        ID['OBJECTIDENTIFIER'] = 0x06;
                        ID['ObjectDescripter'] = 0x07;
                        ID['UTF8String'] = 0x0c;
                        ID['SEQUENCE'] = 0x10;
                        ID['SET'] = 0x11;
                        ID['NumericString'] = 0x12;
                        ID['PrintableString'] = 0x13;
                        ID['TeletexString'] = 0x14;
                        ID['IA5String'] = 0x16;
                        ID['UTCTime'] = 0x17;
                        ID['GeneralizedTime'] = 0x18;
                        ID['Undefined'] = 0x1e;
                        this.NAME = [];
                        for (var i in ID) {
                            this.NAME[ID[i]] = i;
                        }
                        this.OID = [];
                        this.OID['0.2.262.1.10.0'] = 'extension';
                        this.OID['0.2.262.1.10.1.1'] = 'signature';
                        this.OID['1.2.840.113549.1.1'] = 'pkcs - 1';
                        this.OID['1.2.840.113549.1.1.1'] = 'rsaEncryption';
                        this.OID['1.2.840.113549.1.1.4'] = 'md5withRSAEncryption';
                        this.OID['1.2.840.113549.1.1.5'] = 'sha1withRSAEncryption';
                        this.OID['1.2.840.113549.1.1.6'] = 'rsaOAEPEncryptionSET';
                        this.OID['1.2.840.113549.1.7'] = 'pkcs - 7';
                        this.OID['1.2.840.113549.1.7.1'] = 'data';
                        this.OID['1.2.840.113549.1.7.2'] = 'signedData';
                        this.OID['1.2.840.113549.1.7.3'] = 'envelopedData';
                        this.OID['1.2.840.113549.1.7.4'] = 'signedAndEnvelopedData';
                        this.OID['1.2.840.113549.1.7.5'] = 'digestedData';
                        this.OID['1.2.840.113549.1.7.6'] = ' encryptedData';
                        this.OID['1.2.840.113549.1.7.7'] = ' dataWithAttributes';
                        this.OID['1.2.840.113549.1.7.8'] = ' encryptedPrivateKeyInfo';
                        this.OID['1.2.840.113549.1.9.22.1'] = ' x509Certificate(for.PKCS.#12)';
                        this.OID['1.2.840.113549.1.9.23.1'] = ' x509Crl(for.PKCS.#12)';
                        this.OID['1.2.840.113549.1.9.3'] = ' contentType';
                        this.OID['1.2.840.113549.1.9.4'] = ' messageDigest';
                        this.OID['1.2.840.113549.1.9.5'] = ' signingTime';
                        this.OID['2.16.840.1.113730.1'] = ' cert - extension';
                        this.OID['2.16.840.1.113730.1.1'] = ' netscape - cert - type';
                        this.OID['2.16.840.1.113730.1.12'] = ' netscape - ssl - server - name';
                        this.OID['2.16.840.1.113730.1.13'] = ' netscape - comment';
                        this.OID['2.16.840.1.113730.1.2'] = ' netscape - base - url';
                        this.OID['2.16.840.1.113730.1.3'] = ' netscape - revocation - url';
                        this.OID['2.16.840.1.113730.1.4'] = ' netscape - ca - revocation - url';
                        this.OID['2.16.840.1.113730.1.7'] = ' netscape - cert - renewal - url';
                        this.OID['2.16.840.1.113730.1.8'] = ' netscape - ca - policy - url';
                        this.OID['2.23.42.0'] = ' contentType';
                        this.OID['2.23.42.1'] = ' msgExt';
                        this.OID['2.23.42.10'] = ' national';
                        this.OID['2.23.42.2'] = ' field';
                        this.OID['2.23.42.2.0'] = ' fullName';
                        this.OID['2.23.42.2.1'] = ' givenName';
                        this.OID['2.23.42.2.10'] = ' amount';
                        this.OID['2.23.42.2.2'] = ' familyName';
                        this.OID['2.23.42.2.3'] = ' birthFamilyName';
                        this.OID['2.23.42.2.4'] = ' placeName';
                        this.OID['2.23.42.2.5'] = ' identificationNumber';
                        this.OID['2.23.42.2.6'] = ' month';
                        this.OID['2.23.42.2.7'] = ' date';
                        this.OID['2.23.42.2.7.11'] = ' accountNumber';
                        this.OID['2.23.42.2.7.12'] = ' passPhrase';
                        this.OID['2.23.42.2.8'] = ' address';
                        this.OID['2.23.42.3'] = ' attribute';
                        this.OID['2.23.42.3.0'] = ' cert';
                        this.OID['2.23.42.3.0.0'] = ' rootKeyThumb';
                        this.OID['2.23.42.3.0.1'] = ' additionalPolicy';
                        this.OID['2.23.42.4'] = ' algorithm';
                        this.OID['2.23.42.5'] = ' policy';
                        this.OID['2.23.42.5.0'] = ' root';
                        this.OID['2.23.42.6'] = ' module';
                        this.OID['2.23.42.7'] = ' certExt';
                        this.OID['2.23.42.7.0'] = ' hashedRootKey';
                        this.OID['2.23.42.7.1'] = ' certificateType';
                        this.OID['2.23.42.7.2'] = ' merchantData';
                        this.OID['2.23.42.7.3'] = ' cardCertRequired';
                        this.OID['2.23.42.7.5'] = ' setExtensions';
                        this.OID['2.23.42.7.6'] = ' setQualifier';
                        this.OID['2.23.42.8'] = ' brand';
                        this.OID['2.23.42.9'] = ' vendor';
                        this.OID['2.23.42.9.22'] = ' eLab';
                        this.OID['2.23.42.9.31'] = ' espace - net';
                        this.OID['2.23.42.9.37'] = ' e - COMM';
                        this.OID['2.5.29.1'] = ' authorityKeyIdentifier';
                        this.OID['2.5.29.10'] = ' basicConstraints';
                        this.OID['2.5.29.11'] = ' nameConstraints';
                        this.OID['2.5.29.12'] = ' policyConstraints';
                        this.OID['2.5.29.13'] = ' basicConstraints';
                        this.OID['2.5.29.14'] = ' subjectKeyIdentifier';
                        this.OID['2.5.29.15'] = ' keyUsage';
                        this.OID['2.5.29.16'] = ' privateKeyUsagePeriod';
                        this.OID['2.5.29.17'] = ' subjectAltName';
                        this.OID['2.5.29.18'] = ' issuerAltName';
                        this.OID['2.5.29.19'] = ' basicConstraints';
                        this.OID['2.5.29.2'] = ' keyAttributes';
                        this.OID['2.5.29.20'] = ' cRLNumber';
                        this.OID['2.5.29.21'] = ' cRLReason';
                        this.OID['2.5.29.22'] = ' expirationDate';
                        this.OID['2.5.29.23'] = ' instructionCode';
                        this.OID['2.5.29.24'] = ' invalidityDate';
                        this.OID['2.5.29.25'] = ' cRLDistributionPoints';
                        this.OID['2.5.29.26'] = ' issuingDistributionPoint';
                        this.OID['2.5.29.27'] = ' deltaCRLIndicator';
                        this.OID['2.5.29.28'] = ' issuingDistributionPoint';
                        this.OID['2.5.29.29'] = ' certificateIssuer';
                        this.OID['2.5.29.3'] = ' certificatePolicies';
                        this.OID['2.5.29.30'] = ' nameConstraints';
                        this.OID['2.5.29.31'] = ' cRLDistributionPoints';
                        this.OID['2.5.29.32'] = ' certificatePolicies';
                        this.OID['2.5.29.33'] = ' policyMappings';
                        this.OID['2.5.29.34'] = ' policyConstraints';
                        this.OID['2.5.29.35'] = ' authorityKeyIdentifier';
                        this.OID['2.5.29.36'] = ' policyConstraints';
                        this.OID['2.5.29.37'] = ' extKeyUsage';
                        this.OID['2.5.29.4'] = ' keyUsageRestriction';
                        this.OID['2.5.29.5'] = ' policyMapping';
                        this.OID['2.5.29.6'] = ' subtreesConstraint';
                        this.OID['2.5.29.7'] = ' subjectAltName';
                        this.OID['2.5.29.8'] = ' issuerAltName';
                        this.OID['2.5.29.9'] = ' subjectDirectoryAttributes';
                        this.OID['2.5.4.0'] = ' objectClass';
                        this.OID['2.5.4.1'] = ' aliasedEntryName';
                        this.OID['2.5.4.10'] = ' organizationName';
                        this.OID['2.5.4.10.1'] = ' collectiveOrganizationName';
                        this.OID['2.5.4.11'] = ' organizationalUnitName';
                        this.OID['2.5.4.11.1'] = ' collectiveOrganizationalUnitName';
                        this.OID['2.5.4.12'] = ' title';
                        this.OID['2.5.4.13'] = ' description';
                        this.OID['2.5.4.14'] = ' searchGuide';
                        this.OID['2.5.4.15'] = ' businessCategory';
                        this.OID['2.5.4.16'] = ' postalAddress';
                        this.OID['2.5.4.16.1'] = ' collectivePostalAddress';
                        this.OID['2.5.4.17'] = ' postalCode';
                        this.OID['2.5.4.17.1'] = ' collectivePostalCode';
                        this.OID['2.5.4.18'] = ' postOfficeBox';
                        this.OID['2.5.4.18.1'] = ' collectivePostOfficeBox';
                        this.OID['2.5.4.19'] = ' physicalDeliveryOfficeName';
                        this.OID['2.5.4.19.1'] = ' collectivePhysicalDeliveryOfficeName';
                        this.OID['2.5.4.2'] = ' knowledgeInformation';
                        this.OID['2.5.4.20'] = ' telephoneNumber';
                        this.OID['2.5.4.20.1'] = ' collectiveTelephoneNumber';
                        this.OID['2.5.4.21'] = ' telexNumber';
                        this.OID['2.5.4.21.1'] = ' collectiveTelexNumber';
                        this.OID['2.5.4.22.1'] = ' collectiveTeletexTerminalIdentifier';
                        this.OID['2.5.4.23'] = ' facsimileTelephoneNumber';
                        this.OID['2.5.4.23.1'] = ' collectiveFacsimileTelephoneNumber';
                        this.OID['2.5.4.25'] = ' internationalISDNNumber';
                        this.OID['2.5.4.25.1'] = ' collectiveInternationalISDNNumber';
                        this.OID['2.5.4.26'] = ' registeredAddress';
                        this.OID['2.5.4.27'] = ' destinationIndicator';
                        this.OID['2.5.4.28'] = ' preferredDeliveryMehtod';
                        this.OID['2.5.4.29'] = ' presentationAddress';
                        this.OID['2.5.4.3'] = ' commonName';
                        this.OID['2.5.4.31'] = ' member';
                        this.OID['2.5.4.32'] = ' owner';
                        this.OID['2.5.4.33'] = ' roleOccupant';
                        this.OID['2.5.4.34'] = ' seeAlso';
                        this.OID['2.5.4.35'] = ' userPassword';
                        this.OID['2.5.4.36'] = ' userCertificate';
                        this.OID['2.5.4.37'] = ' caCertificate';
                        this.OID['2.5.4.38'] = ' authorityRevocationList';
                        this.OID['2.5.4.39'] = ' certificateRevocationList';
                        this.OID['2.5.4.4'] = ' surname';
                        this.OID['2.5.4.40'] = ' crossCertificatePair';
                        this.OID['2.5.4.41'] = ' name';
                        this.OID['2.5.4.42'] = ' givenName';
                        this.OID['2.5.4.43'] = ' initials';
                        this.OID['2.5.4.44'] = ' generationQualifier';
                        this.OID['2.5.4.45'] = ' uniqueIdentifier';
                        this.OID['2.5.4.46'] = ' dnQualifier';
                        this.OID['2.5.4.47'] = ' enhancedSearchGuide';
                        this.OID['2.5.4.48'] = ' protocolInformation';
                        this.OID['2.5.4.49'] = ' distinguishedName';
                        this.OID['2.5.4.5'] = ' serialNumber';
                        this.OID['2.5.4.50'] = ' uniqueMember';
                        this.OID['2.5.4.51'] = ' houseIdentifier';
                        this.OID['2.5.4.52'] = ' supportedAlgorithms';
                        this.OID['2.5.4.53'] = ' deltaRevocationList';
                        this.OID['2.5.4.55'] = ' clearance';
                        this.OID['2.5.4.58'] = ' crossCertificatePair';
                        this.OID['2.5.4.6'] = ' countryName';
                        this.OID['2.5.4.7'] = ' localityName';
                        this.OID['2.5.4.7.1'] = ' collectiveLocalityName';
                        this.OID['2.5.4.8'] = ' stateOrProvinceName';
                        this.OID['2.5.4.8.1'] = ' collectiveStateOrProvinceName';
                        this.OID['2.5.4.9'] = ' streetAddress';
                        this.OID['2.5.4.9.1'] = ' collectiveStreetAddress';
                        this.OID['2.5.6.0'] = ' top';
                        this.OID['2.5.6.1'] = ' alias';
                        this.OID['2.5.6.10'] = ' residentialPerson';
                        this.OID['2.5.6.11'] = ' applicationProcess';
                        this.OID['2.5.6.12'] = ' applicationEntity';
                        this.OID['2.5.6.13'] = ' dSA';
                        this.OID['2.5.6.14'] = ' device';
                        this.OID['2.5.6.15'] = ' strongAuthenticationUser';
                        this.OID['2.5.6.16'] = ' certificateAuthority';
                        this.OID['2.5.6.17'] = ' groupOfUniqueNames';
                        this.OID['2.5.6.2'] = ' country';
                        this.OID['2.5.6.21'] = ' pkiUser';
                        this.OID['2.5.6.22'] = ' pkiCA';
                        this.OID['2.5.6.3'] = ' locality';
                        this.OID['2.5.6.4'] = ' organization';
                        this.OID['2.5.6.5'] = ' organizationalUnit';
                        this.OID['2.5.6.6'] = ' person';
                        this.OID['2.5.6.7'] = ' organizationalPerson';
                        this.OID['2.5.6.8'] = ' organizationalRole';
                        this.OID['2.5.6.9'] = ' groupOfNames';
                        this.OID['2.5.8'] = ' X.500- Algorithms';
                        this.OID['2.5.8.1'] = ' X.500- Alg - Encryption';
                        this.OID['2.5.8.1.1'] = ' rsa';
                        this.OID['2.54.1775.2'] = ' hashedRootKey';
                        this.OID['2.54.1775.3'] = ' certificateType';
                        this.OID['2.54.1775.4'] = ' merchantData';
                        this.OID['2.54.1775.5'] = ' cardCertRequired';
                        this.OID['2.54.1775.7'] = ' setQualifier';
                        this.OID['2.54.1775.99'] = ' set - data';
                        this.OID['1.3.6.1.5.5.7.3.1'] = 'Аутентификация сервера';
                        this.OID['1.3.6.1.5.5.7.3.2'] = 'Аутентификация клиента';
                        this.OID['1.3.6.1.5.5.7.3.3'] = 'Подписывание кода';
                        this.OID['1.3.6.1.5.5.7.3.4'] = 'Защищенная электронная почта';
                        this.OID['1.3.6.1.5.5.7.3.8'] = 'Простановка штампов времени';
                        this.OID['1.3.6.1.4.1.311.10.5.1'] = 'Цифровые права';
                        this.OID['1.3.6.1.4.1.311.10.3.12'] = 'Подписывание документа';
                    }
                    Asn1.prototype.decode = function (data) {
                        this.TAB = "                              ";
                        this.TAB_num = -1;
                        return this.readASN1(data);
                    };
                    Asn1.prototype.readASN1 = function (data) {
                        var point = 0;
                        var ret = "";
                        this.TAB_num++;
                        while (point < data.length) {
                            // Detecting TAG field (Max 1 octet)
                            var tag10 = parseInt("0x" + data.substr(point, 2));
                            var isSeq = tag10 & 32;
                            var isContext = tag10 & 128;
                            var tag = tag10 & 31;
                            var tagName = isContext ? "[" + tag + "]" : this.NAME[tag];
                            if (tagName == undefined) {
                                tagName = "Unsupported_TAG";
                            }
                            point += 2;
                            // Detecting LENGTH field (Max 2 octets)
                            var len = 0;
                            if (tag != 0x5) {
                                if (parseInt("0x" + data.substr(point, 2)) & 128) {
                                    var lenLength = parseInt("0x" + data.substr(point, 2)) & 127;
                                    if (lenLength > 2) {
                                        var error_message = "LENGTH field is too long.(at " + point
                                            + ")\nThis program accepts up to 2 octets of Length field.";
                                        alert(error_message);
                                        return error_message;
                                    }
                                    len = parseInt("0x" + data.substr(point + 2, lenLength * 2));
                                    point += lenLength * 2 + 2; // Special thanks to Mr.(or Ms.) T (Mon, 25 Nov 2002 23:49:29)
                                }
                                else if (lenLength != 0) {
                                    len = parseInt("0x" + data.substr(point, 2));
                                    point += 2;
                                }
                                if (len > data.length - point) {
                                    var error_message = "LENGTH is longer than the rest.\n";
                                    +"(LENGTH: " + len + ", rest: " + data.length + ")";
                                    alert(error_message);
                                    return error_message;
                                }
                            }
                            else {
                                point += 2;
                            }
                            // Detecting VALUE
                            var val = "";
                            var tab = this.TAB.substr(0, this.TAB_num * 3);
                            if (len) {
                                val = data.substr(point, len * 2);
                                point += len * 2;
                            }
                            ret += tab + tagName + " ";
                            ret += (isSeq) ? "{\n" + this.readASN1(val) + tab + "}" : this.getValue(isContext ? 4 : tag, val);
                            ret += "\n";
                        }
                        ;
                        this.TAB_num--;
                        return ret;
                    };
                    Asn1.prototype.getValue = function (tag, data) {
                        var ret = "";
                        if (tag == 1) {
                            ret = data ? 'TRUE' : 'FALSE';
                        }
                        else if (tag == 2) {
                            ret = (data.length < 3) ? parseInt("0x" + data) : data + ' : Too long Integer. Printing in HEX.';
                        }
                        else if (tag == 3) {
                            var unUse = parseInt("0x" + data.substr(0, 2));
                            var bits = data.substr(2);
                            if (bits.length > 4) {
                                ret = "0x" + bits;
                            }
                            else {
                                ret = parseInt("0x" + bits).toString(2);
                            }
                            ret += " : " + unUse + " unused bit(s)";
                        }
                        else if (tag == 5) {
                            ret = "";
                        }
                        else if (tag == 6) {
                            var res = new Array();
                            var d0 = parseInt("0x" + data.substr(0, 2));
                            res[0] = Math.floor(d0 / 40);
                            res[1] = d0 - res[0] * 40;
                            var stack = new Array();
                            var powNum = 0;
                            var i;
                            for (i = 1; i < data.length - 2; i = i + 2) {
                                var token = parseInt("0x" + data.substr(i + 1, 2));
                                stack.push(token & 127);
                                if (token & 128) {
                                    powNum++;
                                }
                                else {
                                    var j;
                                    var sum = 0;
                                    for (j in stack) {
                                        sum += stack[j] * Math.pow(128, powNum--);
                                    }
                                    res.push(sum);
                                    powNum = 0;
                                    stack = new Array();
                                }
                            }
                            ret = res.join(".");
                            if (this.OID[ret]) {
                                ret += " (" + this.OID[ret] + ")";
                            }
                        }
                        else if (this.NAME[tag].match(/(Time|String)$/)) {
                            var k = 0;
                            ret += "'";
                            while (k < data.length) {
                                ret += String.fromCharCode(parseInt("0x" + data.substr(k, 2), 16));
                                k += 2;
                            }
                            ret += "'";
                        }
                        else {
                            ret = data;
                        }
                        return ret;
                    };
                    return Asn1;
                })();
                ASN1.Asn1 = Asn1;
            })(ASN1 = System.ASN1 || (System.ASN1 = {}));
        })(System || (System = {}));

        var System;
        (function (System) {
            var Browser;
            (function (Browser) {
                var Utils;
                (function (Utils) {
                    var Navigator = (function () {
                        function Navigator(navigator) {
                            this.Version = null;
                            var self = this;
                            var name = null, version = null;
                            if (navigator) {
                                if (navigator.userAgent.indexOf('WOW64') > -1 || navigator.platform == 'Win64')
                                    this.Digit = 64;
                                else
                                    this.Digit = 32;
                                var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident|yabrowser(?=\/))\/?\s*(\d+)/i) || [];
                                if (/trident/i.test(M[1])) {
                                    tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                                    name = 'MSIE';
                                    version = new System.SemVer(tem[1], 0, 0);
                                }
                                else if (M[1] === 'Chrome') {
                                    //проверка на оперу/ежика/яндекс
                                    tem = ua.match(/\b(OPR|Edge|YaBrowser|Opera)\/(\d+)/);
                                    if (tem != null) {
                                        name = tem[1].replace('OPR', 'Opera');
                                        version = new System.SemVer(tem[2], 0, 0);
                                    }
                                    else {
                                        //чистый хром
                                        name = M[1];
                                        version = new System.SemVer(M[2], 0, 0);
                                    }
                                }
                                else {
                                    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
                                    if ((tem = ua.match(/version\/(\d+)/i)) != null)
                                        M.splice(1, 1, tem[1]);
                                    name = M[0];
                                    version = new System.SemVer(M[1], 0, 0);
                                }
                            }
                            this.Name = name;
                            this.Version = version;
                        }
                        return Navigator;
                    })();
                    Utils.Navigator = Navigator;
                })(Utils = Browser.Utils || (Browser.Utils = {}));
            })(Browser = System.Browser || (System.Browser = {}));
        })(System || (System = {}));
    </script>

    <script>
        (function (q) {
            function CryptographyGost(q) {
                var idleTime = 3000;
                var cadesplugin = new Cryptography.CryptoPro.Cadesplugin(q);



                /**получение списка сертификатов*/
                function _getCertificates() {
                    let self = this;
                    var certificates = [];
                    return cadesplugin.Load(idleTime)
                        .then(x => Cryptography.X509Certificate.X509Store.Open(cadesplugin))
                        .then(store => store.Certificates())
                        .then(collection => {
                            if (collection && collection.Items) {
                                for (var i = 0; i < collection.Items.length; i++) {
                                    certificates.push({
                                        thumbprint: collection.Items[i].Thumbprint,
                                        serial: collection.Items[i].SerialNumber,
                                        subject: collection.Items[i].SubjectName
                                    });
                                }
                                ;
                            }
                            return certificates;
                        });
                }

                /**Подписание*/
                function _sign(thumbprint, content, detached) {
                    var certificate;
                    return _findCertificateByThumbprint(thumbprint)
                        .then(x => {
                            certificate = x[0];
                            return Cryptography.Pkcs.CmsSigner.Create(cadesplugin);
                        })
                        .then(signer => {
                            return signer.ComputeSignature(content, certificate, detached);
                        })
                        .then(signature => {
                            //вот здесь получили подпись, теперь с ней идем на сервер и пусть сервер проверяет подпись
                            console.log({ signature: signature });
                            return signature;
                        })
                        .catch(ex => {
                            console.error(ex);
                            alert(ex.message || ex);
                        });

                }

                /**Поиск сертификата в хранидище по его отпечатку*/
                function _findCertificateByThumbprint(thumbprint) {
                    let self = this;
                    var certificates;
                    return cadesplugin.Load(this.idleTime)
                        .then(x => Cryptography.X509Certificate.X509Store.Open(cadesplugin))
                        .then(store => store.Certificates())
                        .then(collection => {
                            certificates = collection.Find(thumbprint, Cryptography.X509Certificate.X509CertificateFind.Thumbprint);
                            return certificates;
                        });
                }

                return {
                    getCertificates: _getCertificates,
                    sign: _sign,
                    findCertificateByThumbprint: _findCertificateByThumbprint
                }
            }

            window.cryptography = new CryptographyGost(q);


        })(Q)
    </script>

    <script>
        $(document).ready(function () {
            var certificateList = window.document.querySelector('#certList');

            window.cryptography.getCertificates()
                .then(function (certs) {
                    if (certs) {
                        for (var i = 0; i < certs.length; i++) {
                            var option = document.createElement('option');
                            option.value = certs[i].thumbprint;
                            option.textContent = '(' + certs[i].serial + ')' + certs[i].subject;
                            option.setAttribute('thumbprint', certs[i].thumbprint);
                            certificateList.appendChild(option);
                        }
                    }

                }).catch(function (ex) {
                    //произошла ошибка при получении списка сертификатов
                    console.error(ex);
                    alert(ex);
                })
            function sign() {
			    var sel = document.getElementById("certList"); // Получаем наш список
                var str_name = sel.options[sel.selectedIndex].text;
                //alert(str_name);
				
                var content = document.getElementById('packetForSign').value;
                var certList = document.getElementById('certList');
                var thumbprint = certList.options[certList.selectedIndex].getAttribute('thumbprint');
                if (thumbprint && thumbprint.length > 0) {
                    window.cryptography.sign(thumbprint, "<?=$base64_file?>", false)
                        .then(function (signature) {
                            if (signature) {
                                $.ajax({
                                    url: "/ajax/signature.php",
                                    dataType: "text",
                                    type: "POST",
                                    data: {signature: signature, inn: <?=$_GET["inn"]?>, id: <?=$_GET["id"]?>, str_name: str_name},
                                    success: function (response) {
                                        $('.info').html(response);
                                    }
                                });
                            }

                        })
                }


            }



            $(".signature").click(function () {
                sign();

            });
			//$("#certList").change(function () {
                
            //});

        });
    </script>
    <style type="text/css">
        .table input,
        .uneditable-input {
            width: 206px;
            max-width: 120px;
        }
        
        .table {
            width: 100%
        }
        
        .table .currency {
            margin: 0!important;
        }
        
        .btn-sm,
        .btn-group-sm>.btn {
            padding: 5px 10px;
            font-size: 12px;
            line-height: 1.5;
            border-radius: 3px;
        }
        
        .btn-primary {
            color: #ffffff;
            background-color: #428bca;
            border-color: #357ebd;
            text-decoration: none;
        }
        
     
		
		.name {width: 200px; display:inline-block}
		input {float: right; width:450px !important; margin-top: -10px !important}
    </style>
<?
CModule::IncludeModule("iblock");
$el = GetIBlockElement($_GET["id"]);?>	
    <div class="wrapper">
        <div  class="info" style="float: left; width: 600px;">	
		    <h3>Подпишите заявку для передачи на рассмотрения в банк</h3>
		    <h1>Заявка № <?=$_GET["id"]?></h1>
			<div class="element_anketa">
					 <span class="name">Принципал:</span> <input type="text" class="princ" name='L' placeholder='Наименование принципала'  value="<?=$el["PROPERTIES"]['L']["VALUE"]?>" disabled>												
			</div>
			<div class="element_anketa">
					<span class="name">ИНН:</span> <input type="text" placeholder='ИНН принципала' value="<?=$el["PROPERTIES"]['M']["VALUE"]?>" disabled>
			</div>	
			<div class="element_anketa">
					<span class="name">ОГРН:</span> <input type="text" placeholder='ОГРН принципала' value="<?=$el["PROPERTIES"]['OGRN_PRINC']["VALUE"]?>" disabled>
			</div>
			<div class="element_anketa notice_box">
															<span class="name">Номер извещения:</span> <input type="text" id="notice" class="notice" name='A' placeholder='Номер извещения/закупки'  value="<?=$el["PROPERTIES"]['A']["VALUE"]?>" disabled>
													</div>
			<div class="element_anketa notice_box">
														   <span class="name">Сумма гарантии:</span> <input type="text" id="notice" class="notice" name='B' value="<?=$el["PROPERTIES"]['B']["VALUE"]?>"  placeholder='Сумма БГ' disabled>
													</div>	
			</br>										
			<div class="element_anketa">
															<span class="name">Срок действия:</span> 
															<input placeholder='по' disabled id="date_to" type="text" name='D2' value="<?=$el["PROPERTIES"]['D2']["VALUE"]?>" placeholder='' class="date_time datepicker"  autocomplete="false" style="margin-right: 230px"> <span class="error text">
															<input id="date_from" type="text" name="D1" placeholder='с' value="<?=$el["PROPERTIES"]['D1']["VALUE"]?>" disabled placeholder='' class="date_time datepicker" autocomplete="false" style="margin-left: -20px !important">
															</span>
													</div>										
		
			</br>
			<input type="hidden" id="packetForSign" value="1234567890" />
			<span class="name">Выберите подписанта:</span> <select id="certList" style="width:425px"></select>
			</br> 
			<!--Баланс - <a href="<?$_SERVER['DOCUMENT_ROOT']?>/archives/<?=$_GET["inn"]?>/fin.xlsm" download>Скачать</a>-->
			</br>
			<a href="<?$_SERVER['DOCUMENT_ROOT']?>/archives/<?=$_GET["inn"]?>/<?=$_GET["id"]?>.zip" download>Скачать подписываемый файл</a></br>
			</br>
			</br>
			<a class="btn btn-primary btn-sm signature" id="<?=$_GET["id"]?>" href="#">Подписать и передать в банк</a></br>
			</br>
			
		</div>			
    </div>


    <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>