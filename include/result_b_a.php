<?global $arResult;?>
<div class="horizont_line"></div>
<div class="element_anketa">
        ОСВ в разрезе контрагентов по счетам: 01, 58, 60.1, 60.2, 62.1, 62.2, 66, 67, 76, 68, 69 за период 12 месяцев, предшествующих дате подачи документов в Банк
        <div class="file_button">
                <input type="file" name='DA'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['DA']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Справка об открытых расчетных (текущих) счетах в кредитных организациях, выданная налоговым органом (оригинал или нотариально заверенная копия) со сроком выдачи не более 1 месяца до даты обращения в Банк        
        <div class="file_button">
                <input type="file" name='DB'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['DB']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
	Справка из ИФНС об отсутствии задолженности перед бюджетом всех уровней (оригинал или нотариально заверенная копия) со сроком выдачи не более 1 месяца до даты обращения в Банк
        <div class="file_button">
                <input type="file" name='DC'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['DC']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Справка выданная кредитными организациями (со сроком выдачи не более 1 месяца до даты обращения в Банк), в которых открыты расчетные счета клиента об оборотах денежных средств на счетах за последние 6 месяцев с разбивкой по месяцам
        <div class="file_button">
                <input type="file" name='DD'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['DD']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Справка выданная кредитными организациями (со сроком выдачи не более 1 месяца до даты обращения в Банк), в которых открыты расчетные счета клиента о картотеке №2 неоплаченных расчетных документов и ее длительности на текущую дату        
        <div class="file_button">
                <input type="file" name='DE'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['DE']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Справка выданная кредитными организациями (со сроком выдачи не более 1 месяца до даты обращения в Банк), в которых открыты расчетные счета клиента о наличии/отсутствии ссудной задолженности или о кредитной истории за предыдущие 12 месяцев с указанием условий
        <div class="file_button">
                <input type="file" name='DF'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['DF']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                
</div>
<div class="horizont_line"></div>
        