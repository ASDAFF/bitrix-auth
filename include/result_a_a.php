
<?global $arResult;?>
<div class="horizont_line"></div>
<div class="element_anketa">
        Бухгалтерская отчетность за 2014 год (баланс, отчет о прибылях и убытках) с отметкой налогового органа о принятии или копией электронного документа заверенного ЭЦП налогового органа или почтовой квитанции
        <div class="file_button">
                <input type="file" name='AA'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['AA']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Управленческая бухгалтерская отчетность на отчетные даты 2015 года (баланс и отчет о прибылях и убытках)
        
        <div class="file_button">
                <input type="file" name='AB'>
        </div>
        
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['AB']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Налоговая декларация по налогу на прибыль за 2014 год с отметкой налогового органа об их принятии или копией электронного документа заверенного ЭЦП налогового органа или почтовой квитанции
        <div class="file_button">
                <input type="file" name='AC'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['AC']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        
        

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Налоговая декларация по налогу на прибыль за отчетные периоды 2015 года с отметкой налогового органа об их принятии или копией электронного документа заверенного ЭЦП налогового органа или почтовой квитанции
        <div class="file_button">
                <input type="file" name='AD'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['AD']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Оригинал Заявления (<a href='#' target='_blank'>скачать образец</a>) на получение банковской гарантии
        <div class="file_button">
                <input type="file" name='AE'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['AE']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Конкурсная документация с описанием конкурса, с указанием дат проведения конкурса и его условий
        <div class="file_button">
                <input type="file" name='AF'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['AF']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        
        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Протокол подведения итогов конкурса
        <div class="file_button">
                <input type="file" name='AG'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['AG']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        
        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Проект государственного/муниципального контракта
        <div class="file_button">
                <input type="file" name='AH'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['AH']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        
        

</div>
<div class="horizont_line"></div>