<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json; charset=UTF-8');
global $USER;
$rsUser = CUser::GetByID($_SESSION['ID_NEW_USER']);
$arUser = $rsUser->Fetch();
//preprint($arUser);
if($arUser['PERSONAL_PAGER'] == $_POST['KOD']){
        $result['type'] = 'true';
        $USER->Authorize($arUser['ID'],false,true);
        $user = new CUser;

        $arFields = Array(
                "ACTIVE"                => "Y",
                'PERSONAL_PAGER'        => $kod
        );
        $user->Update($arUser['ID'],$arFields);
}else{
        $result['type'] = 'false';
        $result['text'] = 'Код подтверждения указан не верно';
}
echo json_encode($result);