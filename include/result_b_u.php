<?global $arResult;?>
<div class="element_title">
        <span class="title_table">Юридические документы</span>
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Свидетельство о государственной регистрации юридического лица
        <div class="file_button">
                <input type="file" name='FA'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FA']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Свидетельство о постановке на учет в налоговом органе        
        <div class="file_button">
                <input type="file" name='FB'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FB']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Решение/протокол о назначении на должность единоличного исполнительного органа (генерального директора, директора, президента, или исполнительного директора согласно Уставу) и продлении срока его полномочий на новый срок, если срок продлевался
        <div class="file_button">
                <input type="file" name='FC'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FC']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Приказ о назначении главного бухгалтера или о возложении его обязанностей на руководителя организации        
        <div class="file_button">
                <input type="file" name='FD'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FD']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Разрешение (лицензия) на занятие отдельными видами деятельности (в необходимых случаях)
        <div class="file_button">
                <input type="file" name='FE'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FE']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Протокол/выписка из протокола об одобрении сделки на заключение контракта/на заключение договора о предоставлении БГ (в случае, если сделка считается крупной)
        <div class="file_button">
                <input type="file" name='FF'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FF']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Согласие налогоплательщика налоговому органу на разглашение сведений, составляющих налоговую тайну
        <div class="file_button">
                <input type="file" name='FG'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FG']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Свидетельство о регистрации права собственности на здание/помещение или договор аренды, субаренды или иного договора, подтверждающего передачу помещения/здания во временное владение и пользование
        <div class="file_button">
                <input type="file" name='FH'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FH']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Информационное письмо Федеральной службы государственной статистики (Росстата) об учете в ЕГРПО, заверенное оттиском печати и подписью первого лица (руководителя) Клиента (“копия верна”)       
        <div class="file_button">
                <input type="file" name='FI'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['FI']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                        
</div>
<div class="horizont_line"></div>
        