<?global $arResult;?>
<div class="element_title">
        <span class="title_table">Юридические документы</span>
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Устав организации, дополнения и изменения к нему, утвержденные и зарегистрированные в установленном законодательством РФ порядке 
        <div class="file_button">
                <input type="file" name='CA'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['CA']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        
        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Паспорт руководителя (заполненные страницы)
        <div class="file_button">
                <input type="file" name='CB'>
        </div>
                <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['CB']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        


</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Паспорт главного бухгалтера (заполненные страницы)        
        <div class="file_button">
                <input type="file" name='CC'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['CC']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        
        

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Список лиц, имеющих право на участие в общем собрании акционеров/участников (выписка из реестра акционеров/выписка из состава участников), составленный на дату предоставления Заявления на получение БГ        
        <div class="file_button">
                <input type="file" name='CD'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['CD']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        
        

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Оригинал или нотариальная копия выписки из ЕГРЮЛ
        <div class="file_button">
                <input type="file" name='CE'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['CE']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        
        

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Карточка с образцами подписей и оттиском печати (копия, заверенная банком)
        <div class="file_button">
                <input type="file" name='CF'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['CF']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>        
        
</div>
<div class="horizont_line"></div>
