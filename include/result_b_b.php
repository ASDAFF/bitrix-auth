<?global $arResult;?>
<div class="horizont_line"></div>
<div class="element_anketa">
        Основные средства (строка 1130)
        <div class="file_button">
                <input type="file" name='EA'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['EA']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                
        
</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Дебиторской и кредиторской задолженности по установленной <a href='#' target='_blank'>форме</a> (в скачиваемом файле: с указанием контрагентов, суммы, даты образования и погашения)        
        <div class="file_button">
                <input type="file" name='EB'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['EB']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Займы и кредиты (строки 1510, 1410 баланса) с указанием кредиторов, суммы, сроков предоставления и погашения        
        <div class="file_button">
                <input type="file" name='EC'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['EC']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                

</div>
<div class="horizont_line"></div>
<div class="element_anketa">
        Финансовые вложения (строка 1150) по установленной <a href='#' target='_blank'>форме</a> (в скачиваемом файле: с указанием контрагентов, характера задолженности (участие в других организациях, предоставленные займы или иное), суммы задолженности, даты возникновения и погашения.)        
        <div class="file_button">
                <input type="file" name='ED'>
        </div>
        <?if(!empty($arResult)){
                $ID = $arResult['PROPERTIES']['ED']['VALUE'];
                if(!empty($ID)){
                        $rsfile = CFile::GetByID($ID);
                        $arfile = $rsfile->Fetch();?>
                        <div class="file_url">
                                <a href="/include/upload.php?ID=<?=$ID?>" target="_blank"><?=$arfile['ORIGINAL_NAME']?></a>
                        </div>
                <?}
        }?>                

</div>
<div class="horizont_line"></div>        