<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json; charset=UTF-8');
$group_id [] = $_POST['RegistrationType'];
$user = new CUser;


$kod = rand(1000, 9999);
$arFields = Array(
        "NAME"                  => $_POST['NAME'],
		"LAST_NAME"				=> $_POST['SURNAME'],
		"SECOND_NAME"			=> $_POST['BATKO'],
        "EMAIL"                 => $_POST['EMAIL'],
        "LOGIN"                 => $_POST['EMAIL'],
        "ACTIVE"                => "N",
        "PASSWORD"              => $_POST['PASSWORD'],
        "CONFIRM_PASSWORD"      => $_POST['CONFIRM_PASSWORD'],
        'PERSONAL_MOBILE'       => $_POST['PERSONAL_MOBILE'],
		'GROUP_ID'				=> $group_id,
        'PERSONAL_PAGER'        => $kod,
		"UF_COMPANY"			=> $_POST['UF_COMPANY'],
		"UF_INN"				=> $_POST['UF_INN'],
		"UF_KPP"				=> $_POST['UF_KPP'],
		"UF_OGN"				=> $_POST['UF_OGN'],
		"UF_OGRNIP"				=> $_POST['UF_OGRNIP'],
);
// preprint($arFields);
if($_POST['RegistrationType'] == 8){
	if(!empty($_POST['UF_COMPANY'])&&!empty($_POST['UF_INN'])&&!empty($_POST['UF_KPP'])&&!empty($_POST['UF_OGN'])){
		$ID = $user->Add($arFields);
	}
}elseif($_POST['RegistrationType'] == 9){
	if(!empty($_POST['UF_COMPANY'])&&!empty($_POST['UF_INN'])&&!empty($_POST['UF_OGRNIP'])){
		$ID = $user->Add($arFields);
	}
}elseif($_POST['RegistrationType'] == 10){
	if(!empty($_POST['UF_INN'])){
		$ID = $user->Add($arFields);
	}
}

if (intval($ID) > 0){
        $result['type'] = 'true';
        $arEventFields = array('EMAIL'=> $_POST['EMAIL'], 'KOD'=>$kod);
        $result['text'] = CEvent::SendImmediate("EMAIL_true", 's1', $arEventFields);
        $_SESSION['ID_NEW_USER'] = $ID;
}else{

        $rsUser = CUser::GetByLogin($_POST['EMAIL']);
        if($arUser = $rsUser->Fetch()){
                if($arUser['ACTIVE'] == 'N'){
                        $result['type'] = 'true';
                        $arEventFields = array('EMAIL'=> $_POST['EMAIL'], 'KOD'=>$arUser['PERSONAL_PAGER']);
                        $result['text'] = CEvent::SendImmediate("EMAIL_true", 's1', $arEventFields);
                        $_SESSION['ID_NEW_USER'] = $arUser['ID'];
                }else{
                        $result['type'] = 'false';
                        $result['text'] = str_replace('E-Mail', 'Логин', $user->LAST_ERROR);
                }
        }else{
				if($_POST['RegistrationType'] == 10){
					$result['type'] = 'false';
					$result['text'] = "Не указан ИНН";
				}elseif($_POST['RegistrationType'] == 9){
					if(empty($_POST['UF_COMPANY'])){
						$result['type'] = 'false';
						$result['text'] = "Не указан Найминование";
					}
					if(empty($_POST['UF_INN'])){
						$result['type'] = 'false';
						$result['text'] = "Не указан ИНН";
					}
					if(empty($_POST['UF_OGRNIP'])){
						$result['type'] = 'false';
						$result['text'] = "Не указан ОГРНИП";
					}
					
				}elseif($_POST['RegistrationType'] == 8){
					if(empty($_POST['UF_COMPANY'])){
						$result['type'] = 'false';
						$result['text'] = "Не указан Найминование";
					}
					if(empty($_POST['UF_INN'])){
						$result['type'] = 'false';
						$result['text'] = "Не указан ИНН";
					}
					if(empty($_POST['UF_OGN'])){
						$result['type'] = 'false';
						$result['text'] = "Не указан ОГРН";
					}
					if(empty($_POST['UF_KPP'])){
						$result['type'] = 'false';
						$result['text'] = "Не указан КПП";
					}
					
				}else{
					$result['type'] = 'false';
					$result['text'] = str_replace('Логин', 'E-Mail', $user->LAST_ERROR);
				}
        }
        
}
echo json_encode($result);