<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/json; charset=UTF-8');
global $USER;
/*echo $USER->GetID();
preprint($_POST);
die();*/
if((empty($_POST['NAME']))||(empty($_POST['PERSONAL_MOBILE']))||(empty($_POST['EMAIL']))){
        $result['type'] = 'false';
        $result['text'] = 'Поля не заполнены';
}

if($_POST['password']!=$_POST['new_password']){
        $result['type'] = 'false';
        $result['text'] = 'Пароли не совпадают';
}else{
        $user = new CUser;
        $arFields = Array(
                "NAME"                  => $_POST['NAME'],
                "EMAIL"                 => $_POST['EMAIL'],
                //"LOGIN"                 => $_POST['EMAIL'],
                'PERSONAL_MOBILE'       => $_POST['PERSONAL_MOBILE'],
                'PERSONAL_NOTES'        => $_POST['PERSONAL_NOTES']
        );
        if(!empty($_POST['PASSWORD'])){
                $arFields["PASSWORD"] = $_POST['PASSWORD'];
                $arFields["CONFIRM_PASSWORD"] = $_POST['CONFIRM_PASSWORD'];
        }
                
        if ($user->Update($USER->GetID(), $arFields)){
                $result['type'] = 'true';
                $result['text'] = 'Изменения сохранены';
        }else{
                $result['type'] = 'false';
                $result['text'] = $user->LAST_ERROR;
        }
        
}

echo json_encode($result);